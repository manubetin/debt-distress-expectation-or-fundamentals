
fun_BC_1=function(country){ 
  scale=1
  mydata_q_country= mydata_q %>% filter(ISO3_Code==country & year>=1980)
  mydata_q_country=mydata_q_country %>% mutate(Troughs=ifelse(Troughs==1,GDPV_XDC_cycle/scale,NA),
                                               Peaks=ifelse(Peaks==1,GDPV_XDC_cycle/scale,NA),
                                               Troughs_lab=ifelse(Troughs==1,0,NA),
                                               Peaks_lab=ifelse(Peaks==1,1,NA),
                                               Phase_A1_to_A2=ifelse(Phase_A1_to_A2==1,GDPV_XDC_cycle/scale,NA),
                                               Phase_B1_to_B2=ifelse(Phase_B1_to_B2==1,GDPV_XDC_cycle/scale,NA),
                                               GDPV_XDC_cycle_A1=ifelse(Phase_A1==1,GDPV_XDC_cycle/scale,NA),
                                               GDPV_XDC_cycle_B1=ifelse(Phase_B1==1,GDPV_XDC_cycle/scale,NA),
                                               GDPV_XDC_cycle_A2=ifelse(Phase_A2==1,GDPV_XDC_cycle/scale,NA),
                                               GDPV_XDC_cycle_B2=ifelse(Phase_B2==1,GDPV_XDC_cycle/scale,NA),
                                               trust_shock=ifelse(trust_shock==1,GDPV_XDC_cycle/scale,NA),
                                               distrust_shock=ifelse(distrust_shock==1,GDPV_XDC_cycle/scale,NA))
  ggplot() +
    # geom_point(data =mydata_q_country,aes(x=Period,y=Peaks),shape=24,fill="darkgreen",size=3)+
    # geom_point(data =mydata_q_country,aes(x=Period,y=Troughs),shape=25,fill="darkred",size=3)+
    # geom_point(data =mydata_q_country,aes(x=Period,y=Phase_A1_to_A2),shape=3,color="darkgreen",size=4)+
    # geom_point(data =mydata_q_country,aes(x=Period,y=Phase_B1_to_B2),shape=3,color="darkred",size=4)+
    # 
    geom_line(data=mydata_q_country, stat = "identity",aes(x = Period, y =GDPV_XDC_cycle/scale)) +
    # geom_line(data=mydata_q_country, stat = "identity",aes(x = Period, y =GDPV_XDC_cycle_A1,color="Phase A1")) +
    # geom_line(data =mydata_q_country,stat = "identity",aes(x = Period, y =GDPV_XDC_cycle_B1,color="Phase B1")) +
    # geom_line(data=mydata_q_country, stat = "identity",aes(x = Period, y =GDPV_XDC_cycle_A2,color="Phase A2")) +
    # geom_line(data =mydata_q_country,stat = "identity",aes(x = Period, y =GDPV_XDC_cycle_B2,color="Phase B2")) +
    # 
    scale_color_manual(values = c("lightgreen","darkgreen","red","darkred"),name="")+
    scale_x_date(date_breaks = "1 year",date_labels =  "%Y")+
    xlab("") +
    ylab("Cycle (% of trend)") +
    labs(title=NULL,subtitle =NULL ,caption=NULL)+
    theme_bw(base_size = 10) + 
    theme(axis.text.x=element_text(angle=90,hjust=1),legend.position = "bottom")
}

fun_BC_2=function(country){ 
  scale=1
  mydata_q_country= mydata_q %>% filter(ISO3_Code==country & year>=1980)
  mydata_q_country=mydata_q_country %>% mutate(Troughs=ifelse(Troughs==1,GDPV_XDC_cycle/scale,NA),
                                               Peaks=ifelse(Peaks==1,GDPV_XDC_cycle/scale,NA),
                                               Troughs_lab=ifelse(Troughs==1,0,NA),
                                               Peaks_lab=ifelse(Peaks==1,1,NA),
                                               Phase_A1_to_A2=ifelse(Phase_A1_to_A2==1,GDPV_XDC_cycle/scale,NA),
                                               Phase_B1_to_B2=ifelse(Phase_B1_to_B2==1,GDPV_XDC_cycle/scale,NA),
                                               GDPV_XDC_cycle_A1=ifelse(Phase_A1==1,GDPV_XDC_cycle/scale,NA),
                                               GDPV_XDC_cycle_B1=ifelse(Phase_B1==1,GDPV_XDC_cycle/scale,NA),
                                               GDPV_XDC_cycle_A2=ifelse(Phase_A2==1,GDPV_XDC_cycle/scale,NA),
                                               GDPV_XDC_cycle_B2=ifelse(Phase_B2==1,GDPV_XDC_cycle/scale,NA),
                                               trust_shock=ifelse(trust_shock==1,GDPV_XDC_cycle/scale,NA),
                                               distrust_shock=ifelse(distrust_shock==1,GDPV_XDC_cycle/scale,NA))
  ggplot() +
     geom_point(data =mydata_q_country,aes(x=Period,y=Peaks),shape=24,fill="darkgreen",size=3)+
     geom_point(data =mydata_q_country,aes(x=Period,y=Troughs),shape=25,fill="darkred",size=3)+
    # geom_point(data =mydata_q_country,aes(x=Period,y=Phase_A1_to_A2),shape=3,color="darkgreen",size=4)+
    # geom_point(data =mydata_q_country,aes(x=Period,y=Phase_B1_to_B2),shape=3,color="darkred",size=4)+
    # 
    geom_line(data=mydata_q_country, stat = "identity",aes(x = Period, y =GDPV_XDC_cycle/scale)) +
    # geom_line(data=mydata_q_country, stat = "identity",aes(x = Period, y =GDPV_XDC_cycle_A1,color="Phase A1")) +
    # geom_line(data =mydata_q_country,stat = "identity",aes(x = Period, y =GDPV_XDC_cycle_B1,color="Phase B1")) +
    # geom_line(data=mydata_q_country, stat = "identity",aes(x = Period, y =GDPV_XDC_cycle_A2,color="Phase A2")) +
    # geom_line(data =mydata_q_country,stat = "identity",aes(x = Period, y =GDPV_XDC_cycle_B2,color="Phase B2")) +
    # 
    scale_color_manual(values = c("lightgreen","darkgreen","red","darkred"),name="")+
    scale_x_date(date_breaks = "1 year",date_labels =  "%Y")+
    xlab("") +
    ylab("Cycle (% of trend)") +
    labs(title=NULL,subtitle =NULL ,caption=NULL)+
    theme_bw(base_size = 10) + 
    theme(axis.text.x=element_text(angle=90,hjust=1),legend.position = "bottom")
}

fun_BC_3=function(country){ 
  scale=1
  mydata_q_country= mydata_q %>% filter(ISO3_Code==country & year>=1980)
  mydata_q_country=mydata_q_country %>% mutate(Troughs=ifelse(Troughs==1,GDPV_XDC_cycle/scale,NA),
                                               Peaks=ifelse(Peaks==1,GDPV_XDC_cycle/scale,NA),
                                               Troughs_lab=ifelse(Troughs==1,0,NA),
                                               Peaks_lab=ifelse(Peaks==1,1,NA),
                                               Phase_A1_to_A2=ifelse(Phase_A1_to_A2==1,GDPV_XDC_cycle/scale,NA),
                                               Phase_B1_to_B2=ifelse(Phase_B1_to_B2==1,GDPV_XDC_cycle/scale,NA),
                                               GDPV_XDC_cycle_A1=ifelse(Phase_A1==1,GDPV_XDC_cycle/scale,NA),
                                               GDPV_XDC_cycle_B1=ifelse(Phase_B1==1,GDPV_XDC_cycle/scale,NA),
                                               GDPV_XDC_cycle_A2=ifelse(Phase_A2==1,GDPV_XDC_cycle/scale,NA),
                                               GDPV_XDC_cycle_B2=ifelse(Phase_B2==1,GDPV_XDC_cycle/scale,NA),
                                               trust_shock=ifelse(trust_shock==1,GDPV_XDC_cycle/scale,NA),
                                               distrust_shock=ifelse(distrust_shock==1,GDPV_XDC_cycle/scale,NA))
  ggplot() +
    geom_point(data =mydata_q_country,aes(x=Period,y=Peaks),shape=24,fill="darkgreen",size=3)+
    geom_point(data =mydata_q_country,aes(x=Period,y=Troughs),shape=25,fill="darkred",size=3)+
     geom_point(data =mydata_q_country,aes(x=Period,y=Phase_A1_to_A2),shape=3,color="darkgreen",size=4)+
     geom_point(data =mydata_q_country,aes(x=Period,y=Phase_B1_to_B2),shape=3,color="darkred",size=4)+
    # 
    geom_line(data=mydata_q_country, stat = "identity",aes(x = Period, y =GDPV_XDC_cycle/scale)) +
    # geom_line(data=mydata_q_country, stat = "identity",aes(x = Period, y =GDPV_XDC_cycle_A1,color="Phase A1")) +
    # geom_line(data =mydata_q_country,stat = "identity",aes(x = Period, y =GDPV_XDC_cycle_B1,color="Phase B1")) +
    # geom_line(data=mydata_q_country, stat = "identity",aes(x = Period, y =GDPV_XDC_cycle_A2,color="Phase A2")) +
    # geom_line(data =mydata_q_country,stat = "identity",aes(x = Period, y =GDPV_XDC_cycle_B2,color="Phase B2")) +
    # 
    scale_color_manual(values = c("lightgreen","darkgreen","red","darkred"),name="")+
    scale_x_date(date_breaks = "1 year",date_labels =  "%Y")+
    xlab("") +
    ylab("Cycle (% of trend)") +
    labs(title=NULL,subtitle =NULL ,caption=NULL)+
    theme_bw(base_size = 10) + 
    theme(axis.text.x=element_text(angle=90,hjust=1),legend.position = "bottom")
}

fun_BC_4=function(country){ 
  scale=1
  mydata_q_country= mydata_q %>% filter(ISO3_Code==country & year>=1980)
  mydata_q_country=mydata_q_country %>% mutate(Troughs=ifelse(Troughs==1,GDPV_XDC_cycle/scale,NA),
                                               Peaks=ifelse(Peaks==1,GDPV_XDC_cycle/scale,NA),
                                               Troughs_lab=ifelse(Troughs==1,0,NA),
                                               Peaks_lab=ifelse(Peaks==1,1,NA),
                                               Phase_A1_to_A2=ifelse(Phase_A1_to_A2==1,GDPV_XDC_cycle/scale,NA),
                                               Phase_B1_to_B2=ifelse(Phase_B1_to_B2==1,GDPV_XDC_cycle/scale,NA),
                                               GDPV_XDC_cycle_A1=ifelse(Phase_A1==1,GDPV_XDC_cycle/scale,NA),
                                               GDPV_XDC_cycle_B1=ifelse(Phase_B1==1,GDPV_XDC_cycle/scale,NA),
                                               GDPV_XDC_cycle_A2=ifelse(Phase_A2==1,GDPV_XDC_cycle/scale,NA),
                                               GDPV_XDC_cycle_B2=ifelse(Phase_B2==1,GDPV_XDC_cycle/scale,NA),
                                               trust_shock=ifelse(trust_shock==1,GDPV_XDC_cycle/scale,NA),
                                               distrust_shock=ifelse(distrust_shock==1,GDPV_XDC_cycle/scale,NA))
  ggplot() +
    geom_point(data =mydata_q_country,aes(x=Period,y=Peaks),shape=24,fill="darkgreen",size=3)+
    geom_point(data =mydata_q_country,aes(x=Period,y=Troughs),shape=25,fill="darkred",size=3)+
    geom_point(data =mydata_q_country,aes(x=Period,y=Phase_A1_to_A2),shape=3,color="darkgreen",size=4)+
    geom_point(data =mydata_q_country,aes(x=Period,y=Phase_B1_to_B2),shape=3,color="darkred",size=4)+
    # 
    geom_line(data=mydata_q_country, stat = "identity",aes(x = Period, y =GDPV_XDC_cycle/scale)) +
    geom_line(data=mydata_q_country, stat = "identity",aes(x = Period, y =GDPV_XDC_cycle_A1,color="Phase A1")) +
     geom_line(data =mydata_q_country,stat = "identity",aes(x = Period, y =GDPV_XDC_cycle_B1,color="Phase B1")) +
     geom_line(data=mydata_q_country, stat = "identity",aes(x = Period, y =GDPV_XDC_cycle_A2,color="Phase A2")) +
     geom_line(data =mydata_q_country,stat = "identity",aes(x = Period, y =GDPV_XDC_cycle_B2,color="Phase B2")) +
    # 
    scale_color_manual(values = c("lightgreen","darkgreen","red","darkred"),name="")+
    scale_x_date(date_breaks = "1 year",date_labels =  "%Y")+
    xlab("") +
    ylab("Cycle (% of trend)") +
    labs(title=NULL,subtitle =NULL ,caption=NULL)+
    theme_bw(base_size = 10) + 
    theme(axis.text.x=element_text(angle=90,hjust=1),legend.position = "bottom")
}

fun_BC_5=function(country,var){ 

  mydata_q_country= mydata_q %>% filter(ISO3_Code==country & year>=2000)
  mydata_q_country=mydata_q_country %>% mutate(Troughs=ifelse(Troughs==1,max(eval(parse(text=var)),na.rm=T),NA),
                                               Peaks=ifelse(Peaks==1,max(eval(parse(text=var)),na.rm=T),NA),
                                               Phase_A1_to_A2=ifelse(Phase_A1_to_A2==1,max(eval(parse(text=var)),na.rm=T),NA),
                                               Phase_B1_to_B2=ifelse(Phase_B1_to_B2==1,max(eval(parse(text=var)),na.rm=T),NA),
                                               GDPV_XDC_cycle_A=ifelse(Phase_A==1,max(eval(parse(text=var)),na.rm=T),NA),
                                               GDPV_XDC_cycle_B=ifelse(Phase_B==1,max(eval(parse(text=var)),na.rm=T),NA),
                                              # GDPV_XDC_cycle_A2=ifelse(Phase_A2==1,max(eval(parse(text=var)),na.rm=T),NA),
                                              # GDPV_XDC_cycle_B2=ifelse(Phase_B2==1,max(eval(parse(text=var)),na.rm=T),NA),
                                               trust_shock=ifelse(trust_shock==1,max(eval(parse(text=var)),na.rm=T),NA),
                                               distrust_shock=ifelse(distrust_shock==1,max(eval(parse(text=var)),na.rm=T),NA))
  ggplot() +
    #  geom_point(data =mydata_q_country,aes(x=Period,y=Peaks),shape=24,fill="darkgreen",size=3)+
    #  geom_point(data =mydata_q_country,aes(x=Period,y=Troughs),shape=25,fill="darkred",size=3)+
    #  geom_point(data =mydata_q_country,aes(x=Period,y=Phase_A1_to_A2),shape=3,color="darkgreen",size=4)+
    #  geom_point(data =mydata_q_country,aes(x=Period,y=Phase_B1_to_B2),shape=3,color="darkred",size=4)+
    # # # 
    geom_bar(data=mydata_q_country, stat = "identity",aes(x = Period, y =GDPV_XDC_cycle_A,fill="Phase A"),width=100,alpha=0.5) +
    geom_bar(data =mydata_q_country,stat = "identity",aes(x = Period, y =GDPV_XDC_cycle_B,fill="Phase B"),width=100,alpha=0.5) +
    # geom_bar(data=mydata_q_country, stat = "identity",aes(x = Period, y =GDPV_XDC_cycle_A2,fill="Phase A2"),width=100,alpha=0.5) +
    # geom_bar(data =mydata_q_country,stat = "identity",aes(x = Period, y =GDPV_XDC_cycle_B2,fill="Phase B2"),width=100,alpha=0.5) +
    geom_line(data=mydata_q_country, stat = "identity",aes(x = Period, y =eval(parse(text=var)))) +
    scale_fill_manual(values = c("lightgreen","darkred"),name="")+
    scale_x_date(date_breaks = "1 year",date_labels =  "%Y")+
    xlab("") +
    ylab(var) +
    labs(title=NULL,subtitle =NULL ,caption=NULL)+
    theme_bw(base_size = 10) + 
    theme(axis.text.x=element_text(angle=90,hjust=1),legend.position = "bottom")
}


fun_describe_BC=function(ctry,var){  
  mlist=list()
  mlist[["Cycle"]]=fun_BC_1(ctry)
  mlist[["Turning points"]]=fun_BC_2(ctry)
  mlist[["Phases 1"]]=fun_BC_3(ctry)
  mlist[["Phases 2 "]]=fun_BC_4(ctry)
  mlist[[paste(var," in Cycle",sep="")]]=fun_BC_5(ctry,var)
  return(mlist)
}
