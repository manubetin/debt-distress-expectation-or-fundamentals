

# ***  panel_hp_filter(mydata,var,lambda,smooth_cycle) ***
#function that decompose the cyclical component of a serie using hp filter and giving the option to smooth the cyclical
#component. 
#dependencies: dplyr, hpfilter
#parameters are:
#   mydata: a dataframe containing at least the following columns:
#               "ISO3_Code" as character that gives the country code
#               "Period" as Date format %Y-%m-%d at quarterly frequency
#               "var" the numeric variable specify in the function
#   var: the variable on which you want to perform the filter
#the algorithm will skip any country with less than 10 observations and with missing values
#output:
#   the original dataframe with the trend, cycle and smooth cycle of the serie specified as var

panel_hp_filter=function(mydata,var,lambda=1600,smooth_cycle=T){
  list_country=unique(mydata$ISO3_Code) #find list of countries available in the sample
  n_ctry=length(list_country) #find the number of countries
  min_quarters=10 #set the minimum number of quarter available to start the algorithm
  j=0
  for(i in 1:n_ctry){
    country=list_country[i]
    #Subsample the columns with only the variable that will be used
    mydata_country= mydata %>% filter(ISO3_Code==country) %>%
      mutate(year=year(Period),quarter(Period)) %>%
      dplyr::select(Period,ISO3_Code,year,quarter,var)
    
    #Remove Periods with missing values
    mydata_country_noNA= mydata_country %>% filter(!is.na(eval(parse(text=var)))) %>% dplyr::select(Period,ISO3_Code,year,quarter,var)
    
    #check whether there are missing quarters in dataset
    a=mydata_country_noNA %>% group_by(year,ISO3_Code) %>%summarize(obs=n()) %>% filter(obs!=4)
    completion_condition=(dim(data.frame(a))[1]==0 & dim(mydata_country_noNA)[1]>=min_quarters)
    
    if(completion_condition) { #apply the filter if the serie has no missing quarter and has more than 10 observations
      
      filter=hpfilter(mydata_country_noNA[,var],type="lambda",freq=lambda)
      filter_cycle=filter$cycle
      
      #find the start and end periods for which the information is available
      start=min(mydata_country_noNA %>% dplyr::select(year))
      start_quarter=min(mydata_country_noNA %>% filter(year==start) %>% dplyr::select(quarter))
      end=max(mydata_country_noNA %>% dplyr::select(year))
      end_quarter=max(mydata_country_noNA %>% filter(year==end) %>% dplyr::select(quarter))
      
      #transform serie into time series
      filter_cycle=ts(filter_cycle,start=c(start,start_quarter),end=c(end,end_quarter),frequency = 4)
      dates=as.Date(filter_cycle) # store the serie of dates
      
      if(smooth_cycle==T){
        #use algorithm to smooth the cyclical component (DS for Deseasonalized) using STL decomposition
        filter_cycle_trend=stl(filter_cycle,s.window = "periodic")$time.series[,"trend"]
      }else filter_cycle_trend=filter_cycle #use the cycle with no seasonal adjustment
      
      filter_cycle_trend=as.numeric(data.frame(unlist(filter_cycle_trend))[,1]) #reset the serie into proper format
      filter_cycle_trend=cbind(dates,data.frame(filter_cycle_trend)) #include the dates to the serie
      filter_cycle_trend$ISO3_Code=country
      colnames(filter_cycle_trend)=c("Period","Value_cycle_DS","ISO3_Code") #rename the columns of the dataset
      mydata_country=cbind(filter_cycle_trend, filter$x,filter$cycle,filter$trend)
      colnames(mydata_country)=c("Period","Value_cycle_DS","ISO3_Code","Value_filtered","Value_cycle","Value_trend")
      j=j+1
      if(j==1){ mydata_filtered=mydata_country} #in the first loop initialize the dataset with all countries
      mydata_filtered=rbind(mydata_filtered,mydata_country) 
      # print(paste(country," data has been succesfully detrended",sep=""))
    }else warning(paste(country," data has not been detrended",sep="")) #warning message for countries that did not pass the conditions to launch the algorithm
  }
  return(mydata_filtered)
}

Locate_Peaks_Troughs=function(mydata,var,mincycle=12,minphase=2){
  list_country=unique(mydata$ISO3_Code) #find list of countries available in the sample
  fun=function(ctry){
    country=ctry
    #Subsample the dataset with only the column of interest
    mydata_country= mydata %>% filter(ISO3_Code==country & !is.na(eval(parse(text=var)))) %>% 
      mutate(ISO3_Code=as.character(ISO3_Code), Period=as.Date(Period),year=year(Period),quarter=quarter(Period))
    mydata_country=data.frame(mydata_country)
    
    if(dim(mydata_country)[1]!=0){ #only start the algorithm if the dataset is not empty
      start=min(mydata_country %>% dplyr::select(year)) #find effective start date of the serie
      end=max(mydata_country %>% dplyr::select(year)) #find effective end date of the serie
      start_quarter=min(mydata_country %>% filter(year==start) %>% dplyr::select(quarter))
      end_quarter=max(mydata_country %>% filter(year==end) %>% dplyr::select(quarter))
      
      #transform data into time series 
      mydata_country=ts(mydata_country,start=c(start,start_quarter), end=c(end,end_quarter),frequency = 4)
      
      #use algorithm to generate the cycles and handle error when no data available
      t <- try(BBQ(mydata_country[,var],mincycle=mincycle,minphase =minphase, name="Dating Business Cycles"), silent=T)
      if("try-error" %in% class(t)) {
        warning(paste(country,": Error in BBQ algorithm",sep=""))
      }else {
        Peaks_troughs=show(t) #use show to convert the output of BBQ into a data.frame
        Peaks_troughs$ISO3_Code=country #include code countries for all rows
      return(Peaks_troughs)
      }
    }  
  }
  Peaks_troughs_data=lapply(list_country,fun)
  Peaks_troughs_data=do.call(rbind,Peaks_troughs_data)
  
  Peaks_troughs_data$Duration=as.numeric(Peaks_troughs_data$Duration)
  #transform periods in the proper quaterly format
  Peaks_troughs_data$Peaks=as.Date(as.yearqtr(Peaks_troughs_data$Peaks, format = "%YQ%q")) 
  Peaks_troughs_data$Troughs=as.Date(as.yearqtr(Peaks_troughs_data$Troughs, format = "%YQ%q"))
  
  #match the two dataset to have a columns of dummies for peaks and Troughs in the main dataset
  list_country=unique(as.character(mydata$ISO3_Code))
  n_ctry=length(list_country)
  mydata=data.frame(mydata)
  mydata<-mydata %>% mutate(Period=as.Date(Period))
  for(i in 1:n_ctry){
    country=list_country[i]
    #create a dummy variable for each peak in the data
    mydata[mydata$ISO3_Code==country,"Peaks"]=ifelse(mydata[mydata$ISO3_Code==country,"Period"] %in% Peaks_troughs_data[Peaks_troughs_data$ISO3_Code==country,"Peaks"],1,NA) 
    #create a dummy variable for each trough in the data
    mydata[mydata$ISO3_Code==country,"Troughs"]=ifelse(mydata[mydata$ISO3_Code==country,"Period"] %in% Peaks_troughs_data[Peaks_troughs_data$ISO3_Code==country,"Troughs"],1,NA)
  }
  mydata=mydata %>% group_by(ISO3_Code,Period) %>% distinct()#make sure there is no duplicate in the dataset
  return(list(data=mydata,Peaks_troughs=Peaks_troughs_data))
  #the function returns two elements:
  # mydata is the dataset in panel format with the dummies
  # Peaks_troughs_data is the summary of peaks and troughs dates
}

# ***  Expansions_Contractions_4=function(mydata,var,mincycle=12,minphase=2) ***

#function that apply the BBQ algorithm on a panel data and returns a dataframe with the turning points of the serie
#The index and duration of the cycles and dummy variables for expansion and recessions as well as for the first and second
#half of the expansion the recession
#dependencies: dplyr, BCDating, function Locate_Peaks_Troughs
#parameters are:
#   mydata: a dataframe containing at least the following columns:
#               "ISO3_Code" as character that gives the country code
#               "Period" as Date format %Y-%m-%d at quarterly frequency
#               "var" the numeric variable specify in the function
#   var: the variable on which you want to perform the analysis
#   "mincycle" the minimum length of a cycle
#   "minphase" the minimum length of each phase

#output:
#   the original dataframe, Peaks, Troughs, Phase A1, Phase A2, Phase B1, Phase B2, Phase A,Phase B,Index_cycle,
#   lenght of each phase,  Phase A1_t, Phase A2_t, Phase B1_t, Phase B2_t, Phase A_t,Phase B_t that are counters 
# for each additional quarter since the respective peak/Trough/A1_to_A2/A2_to_B1

Expansions_Contractions_4=function(mydata,var,mincycle=12,minphase=2){
  
  #compute the Peaks and troughs with BBQ algorithm using 4 subphase (A1,A2,B1,B2)
  mydata=data.frame(mydata)
  mydata=Locate_Peaks_Troughs(mydata,var,mincycle,minphase)$data %>% arrange(ISO3_Code,Period) #rearrange the rows in proper order 
  mydata=data.frame(mydata)
  list_country=unique(mydata$ISO3_Code)
    fun=function(ctry){
    count_Peaks=0
    count_Troughs=0
    country=ctry
    #initialize the new variables
    mydata_country=mydata %>% filter(ISO3_Code==country & !is.na(eval(parse(text=var)))) %>% mutate(Phase_A_t=NA, #time trend inside each phase of expansion
                                                                                                    Phase_B_t=NA, #time trend  in phase of recession
                                                                                                    Index_Phase_A=NA, #count the position of each phase A  
                                                                                                    Index_Phase_B=NA, #count the position of each phase B  
                                                                                                    Phase_A1_to_A2=0, #dummy for quarter in the middle of expansion cycle
                                                                                                    Phase_A2_t=0, #time trend in the second half phase of expansion
                                                                                                    Phase_B1_to_B2=0, #dummy for quarter in the middle of recession cycle
                                                                                                    Phase_B2_t=0) #time trend in the second half phase of recession
    
    #find the whether the first cycle is phase A or Phase B
    #keep in mind that there is a issue for the first cycle in phase_A1 and phase_A2 that are not perfectly computed
    z_peak=1
    while(is.na(mydata_country[1+z_peak,"Peaks"]) & z_peak<dim(mydata_country)[1]){z_peak=z_peak+1} 
    z_trough=1
    while(is.na(mydata_country[1+z_trough,"Troughs"]) & z_trough<dim(mydata_country)[1]){z_trough=z_trough+1}
    z=min(z_peak,z_trough)
    if(z==z_peak){
      count_Peaks=count_Peaks+1
      mydata_country[1:z,"Phase_A_t"]=1:z
      mydata_country[1:z,"Index_Phase_A"]=count_Peaks
    }else{
      count_Troughs=count_Troughs+1
      mydata_country[1:z,"Phase_B_t"]=1:z
      mydata_country[1:z,"Index_Phase_B"]=count_Troughs
    }
    #message(paste(country,z,dim(mydata_country)[1]) )
    if (z<dim(mydata_country)[1]) {
      for(i in z:dim(mydata_country)[1]){ #loop on all countries
        
        if(!is.na(mydata_country[i,"Troughs"])){ #build variables for Booms
          j=1
          count_Peaks=count_Peaks+1
          while(is.na(mydata_country[i+j,"Peaks"]) & j<40){ #j<40 is only used to make sure there is no infinite loop
            mydata_country[i+j,"Index_Phase_A"]=count_Peaks
            mydata_country[i+j,"Phase_A_t"]=j
            j=j+1
          }
          mydata_country[i+j,"Phase_A_t"]=j
          mydata_country[i+j,"Index_Phase_A"]=count_Peaks
          mydata_country[i+round(j/2,0),"Phase_A1_to_A2"]=1 #create dummy at half of the cycle
          v=j-round(j/2,0)
          #restart time trend for the second half of the cycle
          mydata_country[(i+round(j/2,0)+1):(i+round(j/2,0)+v),"Phase_A2_t"]=1:v
        }else if(!is.na(mydata_country[i,"Peaks"])){ #build variables for troughs
          j=1
          count_Troughs=count_Troughs+1
          while(is.na(mydata_country[i+j,"Troughs"]) & j<40){
            mydata_country[i+j,"Phase_B_t"]=j
            mydata_country[i+j,"Index_Phase_B"]=count_Troughs
            j=j+1
          }
          mydata_country[i+j,"Phase_B_t"]=j
          mydata_country[i+j,"Index_Phase_B"]=count_Troughs
          mydata_country[i+round(j/2,0),"Phase_B1_to_B2"]=1
          v=j-round(j/2,0)
          mydata_country[(i+round(j/2,0)+1):(i+round(j/2,0)+v),"Phase_B2_t"]=1:v
        }
      }
    }
   return(mydata_country) 
  }
    mydata=lapply(list_country,fun)
    mydata=do.call(rbind,mydata)
 
    mydata=mydata %>% mutate(Phase_A=ifelse(!is.na(Phase_B_t),1,0),
                           Phase_B=ifelse(!is.na(Phase_A_t),0,1),
                           Phase_A=ifelse(Phase_B==1,0,1),
                           Phase_A1_t=ifelse(Phase_A==1 & Phase_A2_t==0,Phase_A_t,0),
                           Phase_B1_t=ifelse(Phase_B==1 & Phase_B2_t==0,Phase_B_t,0),
                           Phase_A2=ifelse(Phase_A2_t!=0,1,0),
                           Phase_A1=ifelse(Phase_A1_t!=0,1,0),
                           Phase_B2=ifelse(Phase_B2_t!=0,1,0),
                           Phase_B1=ifelse(Phase_B1_t!=0,1,0),
                           Peaks=ifelse(is.na(Peaks),0,Peaks),
                           Troughs=ifelse(is.na(Troughs),0,Troughs),
                           Phase_A_t=ifelse(is.na(Phase_A_t),0,Phase_A_t),
                           Phase_B_t=ifelse(is.na(Phase_B_t),0,Phase_B_t),
                           year=year(Period),
                           quarter=quarter(Period))
  
  mydata=mydata %>% group_by(ISO3_Code,Index_Phase_A) %>% arrange(ISO3_Code,Period) %>% mutate(length.Phase_A=n(),
                                                                                               length.Phase_A=ifelse(Phase_A_t==0,0,length.Phase_A))
  mydata=mydata %>% group_by(ISO3_Code,Index_Phase_B) %>% arrange(ISO3_Code,Period) %>% mutate(length.Phase_B=n(),
                                                                                               length.Phase_B=ifelse(Phase_B_t==0,0,length.Phase_B))
  mydata=mydata %>% mutate(Phase_A_t_mirror=ifelse(Phase_A_t==0,Phase_B_t,-(length.Phase_A-Phase_A_t))) 
  
  #correct the indexes to ensure that the first cycle correspond to a complete cycle and not only due to arbitrary initial 
  
  mydata<-mydata %>% group_by(ISO3_Code) %>% 
    mutate(Index_Phase_B=Index_Phase_B-1) %>%
    mutate(Index_Phase_A=Index_Phase_A-ifelse(is.na(first(Index_Phase_A)),0,1))
  #creation of an index for the cycle (trough to trough)
  mydata<-mydata %>% mutate(Index_cycle=ifelse(is.na(Index_Phase_A),Index_Phase_B,Index_Phase_A),
                            Index_cycle=ifelse(is.na(Index_cycle),0,Index_cycle)) 
  
  mydata=mydata %>% filter(!is.na(ISO3_Code) & !is.na(Period)) %>%
    dplyr::select(ISO3_Code,
                  Period,
                  year,
                  quarter,
                  var,
                  Phase_A,
                  Phase_A_t,
                  Index_Phase_A,
                  length.Phase_A,
                  Phase_A1,
                  Phase_A1_t,
                  Phase_A2,
                  Phase_A2_t,
                  Phase_B,
                  Phase_B_t,
                  Index_Phase_B,
                  length.Phase_B,
                  Phase_B1,
                  Phase_B1_t,
                  Phase_B2,
                  Phase_B2_t,
                  Peaks,
                  Troughs,
                  Phase_A1_to_A2,
                  Phase_B1_to_B2,
                  Index_cycle)
  return(mydata)
}

# ***  Expansions_Contractions_2=function(mydata,var,mincycle=12,minphase=2) ***

#function that apply the BBQ algorithm on a panel data and returns a dataframe with the turning points of the serie
#The index and duration of the cycles and dummy variables for expansion and recessions
#dependencies: dplyr, BCDating, function Locate_Peaks_Troughs
#parameters are:
#   mydata: a dataframe containing at least the following columns:
#               "ISO3_Code" as character that gives the country code
#               "Period" as Date format %Y-%m-%d at quarterly frequency
#               "var" the numeric variable specify in the function
#   var: the variable on which you want to perform the analysis
#   "mincycle" the minimum length of a cycle
#   "minphase" the minimum length of each phase

#output:
#   the original dataframe, Peaks, Troughs, Phase A,Phase B,Index_cycle,
#   lenght of each phase, Phase A_t,Phase B_t that are counters 
# for each additional quarter since the respective peak/Trough

Expansions_Contractions_2=function(mydata,var,mincycle=12,minphase=2){
  
  #compute the Peaks and troughs with BBQ algorithm using 2 subphase (A,B)
  mydata=data.frame(mydata)
  mydata=Locate_Peaks_Troughs(mydata,var,mincycle,minphase)$data %>% arrange(ISO3_Code,Period) #rearrange the rows in proper order 
  mydata=data.frame(mydata)
  list_country=unique(mydata$ISO3_Code)
  n_ctry=length(list_country)
  
  
  fun=function(ctry){
    count_Peaks=0
    count_Troughs=0
    country=ctry
    #initialize the new variables
    mydata_country=mydata %>% filter(ISO3_Code==country & !is.na(eval(parse(text=var)))) %>% mutate(Phase_A_t=NA, #time trend inside each phase of expansion
                                                                                                    Phase_B_t=NA, #time trend  in phase of recession
                                                                                                    Index_Phase_A=NA, #count the position of each phase A  
                                                                                                    Index_Phase_B=NA, #count the position of each phase B  
                                                                                                    Phase_A1_to_A2=0, #dummy for quarter in the middle of expansion cycle
                                                                                                    Phase_A2_t=0, #time trend in the second half phase of expansion
                                                                                                    Phase_B1_to_B2=0, #dummy for quarter in the middle of recession cycle
                                                                                                    Phase_B2_t=0) #time trend in the second half phase of recession
    
    #find the whether the first cycle is phase A or Phase B
    #keep in mind that there is a issue for the first cycle in phase_A1 and phase_A2 that are not perfectly computed
    z_peak=1
    while(is.na(mydata_country[1+z_peak,"Peaks"]) & z_peak<dim(mydata_country)[1]){z_peak=z_peak+1} 
    z_trough=1
    while(is.na(mydata_country[1+z_trough,"Troughs"]) & z_trough<dim(mydata_country)[1]){z_trough=z_trough+1}
    z=min(z_peak,z_trough)
    if(z==z_peak){
      count_Peaks=count_Peaks+1
      mydata_country[1:z,"Phase_A_t"]=1:z
      mydata_country[1:z,"Index_Phase_A"]=count_Peaks
    }else{
      count_Troughs=count_Troughs+1
      mydata_country[1:z,"Phase_B_t"]=1:z
      mydata_country[1:z,"Index_Phase_B"]=count_Troughs
    }
    #message(paste(country,z,dim(mydata_country)[1]) )
    if (z<dim(mydata_country)[1]) {
      for(i in z:dim(mydata_country)[1]){ #loop on all countries
        
        if(!is.na(mydata_country[i,"Troughs"])){ #build variables for Booms
          j=1
          count_Peaks=count_Peaks+1
          while(is.na(mydata_country[i+j,"Peaks"]) & j<40){ #j<40 is only used to make sure there is no infinite loop
            mydata_country[i+j,"Index_Phase_A"]=count_Peaks
            mydata_country[i+j,"Phase_A_t"]=j
            j=j+1
          }
          mydata_country[i+j,"Phase_A_t"]=j
          mydata_country[i+j,"Index_Phase_A"]=count_Peaks
          mydata_country[i+round(j/2,0),"Phase_A1_to_A2"]=1 #create dummy at half of the cycle
          v=j-round(j/2,0)
          #restart time trend for the second half of the cycle
          mydata_country[(i+round(j/2,0)+1):(i+round(j/2,0)+v),"Phase_A2_t"]=1:v
        }else if(!is.na(mydata_country[i,"Peaks"])){ #build variables for troughs
          j=1
          count_Troughs=count_Troughs+1
          while(is.na(mydata_country[i+j,"Troughs"]) & j<40){
            mydata_country[i+j,"Phase_B_t"]=j
            mydata_country[i+j,"Index_Phase_B"]=count_Troughs
            j=j+1
          }
          mydata_country[i+j,"Phase_B_t"]=j
          mydata_country[i+j,"Index_Phase_B"]=count_Troughs
          mydata_country[i+round(j/2,0),"Phase_B1_to_B2"]=1
          v=j-round(j/2,0)
          mydata_country[(i+round(j/2,0)+1):(i+round(j/2,0)+v),"Phase_B2_t"]=1:v
        }
      }
    }
    return(mydata_country) 
  }
  mydata=lapply(list_country,fun)
  mydata=do.call(rbind,mydata)
  
  mydata=mydata %>% mutate(Phase_A=ifelse(!is.na(Phase_B_t),1,0),
                           Phase_B=ifelse(!is.na(Phase_A_t),0,1),
                           Phase_A=ifelse(Phase_B==1,0,1),
                           Phase_A1_t=ifelse(Phase_A==1 & Phase_A2_t==0,Phase_A_t,0),
                           Phase_B1_t=ifelse(Phase_B==1 & Phase_B2_t==0,Phase_B_t,0),
                           Phase_A2=ifelse(Phase_A2_t!=0,1,0),
                           Phase_A1=ifelse(Phase_A1_t!=0,1,0),
                           Phase_B2=ifelse(Phase_B2_t!=0,1,0),
                           Phase_B1=ifelse(Phase_B1_t!=0,1,0),
                           Peaks=ifelse(is.na(Peaks),0,Peaks),
                           Troughs=ifelse(is.na(Troughs),0,Troughs),
                           Phase_A_t=ifelse(is.na(Phase_A_t),0,Phase_A_t),
                           Phase_B_t=ifelse(is.na(Phase_B_t),0,Phase_B_t),
                           year=year(Period),
                           quarter=quarter(Period))
  
  mydata=mydata %>% group_by(ISO3_Code,Index_Phase_A) %>% arrange(ISO3_Code,Period) %>% mutate(length.Phase_A=n(),
                                                                                               length.Phase_A=ifelse(Phase_A_t==0,0,length.Phase_A))
  mydata=mydata %>% group_by(ISO3_Code,Index_Phase_B) %>% arrange(ISO3_Code,Period) %>% mutate(length.Phase_B=n(),
                                                                                               length.Phase_B=ifelse(Phase_B_t==0,0,length.Phase_B))
  mydata=mydata %>% mutate(Phase_A_t_mirror=ifelse(Phase_A_t==0,Phase_B_t,-(length.Phase_A-Phase_A_t))) 
  
  #correct the indexes to ensure that the first cycle correspond to a complete cycle and not only due to arbitrary initial 
  
  mydata<-mydata %>% group_by(ISO3_Code) %>% 
    mutate(Index_Phase_B=Index_Phase_B-1) %>%
    mutate(Index_Phase_A=Index_Phase_A-ifelse(is.na(first(Index_Phase_A)),0,1))
  #creation of an index for the cycle (trough to trough)
  mydata<-mydata %>% mutate(Index_cycle=ifelse(is.na(Index_Phase_A),Index_Phase_B,Index_Phase_A),
                            Index_cycle=ifelse(is.na(Index_cycle),0,Index_cycle)) 
  
  mydata=mydata %>% filter(!is.na(ISO3_Code) & !is.na(Period)) %>%
    dplyr::select(ISO3_Code,
                  Period,
                  year,
                  quarter,
                  var,
                  Phase_A,
                  Phase_A_t,
                  Index_Phase_A,
                  length.Phase_A,
                  Phase_B,
                  Phase_B_t,
                  Index_Phase_B,
                  length.Phase_B,
                  Peaks,
                  Troughs,
                  Index_cycle)
  
  return(mydata)
}

cycle_stats=function(mydata,var){
  
  # mydata<-mydata %>% dplyr::select(ISO3_Code,Period,var)
  # mydata<-Expansion_recession(mydata,var,mincycle = mincycle,minphase = minphase)
  
  #  list of variables that are need to use the function if no then disply error message
  vars= c("Phase_A","Phase_A_t","Index_Phase_A","length.Phase_A","Phase_B","Phase_B_t","Index_Phase_B","length.Phase_B", "Peaks","Troughs", "Index_cycle")
  
  if(all(vars %in% names(mydata))){
    mystats<-data.frame(mydata) %>% filter((Peaks==1 | Troughs==1)) %>% group_by(ISO3_Code) %>%
      mutate(Period_all=paste(dplyr::lag(Period),Period,dplyr::lead(Period),sep="/"),
             var_Trough_1=dplyr::lag(eval(parse(text=var))),
             var_Peaks=eval(parse(text=var)),
             var_Trough_2=dplyr::lead(eval(parse(text=var))),
             duration.B=dplyr::lead(length.Phase_B),
             duration.A=length.Phase_A,
             duration.C=duration.A+duration.B,
             amplitude.A=abs(var_Trough_1-var_Peaks),#/var_Trough_1,
             amplitude.B=-abs(var_Peaks-var_Trough_2),#/var_Peaks,
             amplitude.C=abs(var_Trough_1-var_Trough_2),#/var_Trough_1,
             quarter.amplitude.A=amplitude.A/duration.A,
             quarter.amplitude.B=amplitude.B/duration.B,
             quarter.amplitude.C=amplitude.C/duration.C) %>% filter(Peaks!=0) %>%
      dplyr::select(ISO3_Code,Index_cycle,Period,Period_all,var_Trough_1,var_Peaks,var_Trough_2,
                    duration.A,duration.B,duration.C,
                    amplitude.A,amplitude.B,amplitude.C,
                    quarter.amplitude.A,quarter.amplitude.B,quarter.amplitude.C) 
    return(mystats)
  }else{
    mystats=NULL
    print(warning("Cannot compute cycle stats, please first apply the function Expansion_recession)"))
    return(mystats)
    
  }
  
}

fun.Cycle=function(mydata,var,country){ 
  scale=1
  mydata_q_country= mydata %>% filter(ISO3_Code==country) #& year>=1980)
  mydata_q_country=mydata_q_country %>% mutate(Troughs=ifelse(Troughs==1,eval(parse(text=var))/scale,NA),
                                               Peaks=ifelse(Peaks==1,eval(parse(text=var))/scale,NA),
                                               Troughs_lab=ifelse(Troughs==1,0,NA),
                                               Peaks_lab=ifelse(Peaks==1,1,NA),
                                               Phase_A1_to_A2=ifelse(Phase_A1_to_A2==1,eval(parse(text=var))/scale,NA),
                                               Phase_B1_to_B2=ifelse(Phase_B1_to_B2==1,eval(parse(text=var))/scale,NA))
ggplot() +
    geom_line(data=mydata_q_country, stat = "identity",aes(x = Period, y =eval(parse(text=var))/scale)) +
    scale_color_manual(values = c("lightgreen","darkgreen","red","darkred"),name="")+
    scale_x_date(date_breaks = "1 year",date_labels =  "%Y")+
    xlab("") +
    ylab("Cycle (% of trend)") +
    labs(title=NULL,subtitle =NULL ,caption=NULL)+
    theme_bw(base_size = 10) + 
    theme(axis.text.x=element_text(angle=90,hjust=1),legend.position = "bottom",axis.text=element_text(size=12))
}

fun.Turning.Points=function(mydata,var,country){ 
  scale=1
  mydata_q_country= mydata %>% filter(ISO3_Code==country)# & year>=1980)
  mydata_q_country=mydata_q_country %>% mutate(Troughs=ifelse(Troughs==1,eval(parse(text=var))/scale,NA),
                                               Peaks=ifelse(Peaks==1,eval(parse(text=var))/scale,NA),
                                               Troughs_lab=ifelse(Troughs==1,0,NA),
                                               Peaks_lab=ifelse(Peaks==1,1,NA))
  ggplot() +
    geom_point(data =mydata_q_country,aes(x=Period,y=Peaks),shape=24,fill="darkgreen",size=3)+
    geom_point(data =mydata_q_country,aes(x=Period,y=Troughs),shape=25,fill="darkred",size=3)+
    geom_line(data=mydata_q_country, stat = "identity",aes(x = Period, y =eval(parse(text=var))/scale)) +
    scale_color_manual(values = c("lightgreen","darkgreen","red","darkred"),name="")+
    scale_x_date(date_breaks = "1 year",date_labels =  "%Y")+
    xlab("") +
    ylab("Cycle (% of trend)") +
    labs(title=NULL,subtitle =NULL ,caption=NULL)+
    theme_bw(base_size = 10) + 
    theme(axis.text.x=element_text(angle=90,hjust=1),legend.position = "bottom",axis.text=element_text(size=12))
}

fun.Phase.AB=function(mydata,var,country){ 
  scale=1
  mydata_q_country= mydata %>% filter(ISO3_Code==country)# & year>=1980)
  mydata_q_country=mydata_q_country %>% mutate(Troughs=ifelse(Troughs==1,eval(parse(text=var))/scale,NA),
                                               Peaks=ifelse(Peaks==1,eval(parse(text=var))/scale,NA),
                                               Troughs_lab=ifelse(Troughs==1,0,NA),
                                               Peaks_lab=ifelse(Peaks==1,1,NA),
                                               Phase_A1_to_A2=ifelse(Phase_A1_to_A2==1,eval(parse(text=var))/scale,NA),
                                               Phase_B1_to_B2=ifelse(Phase_B1_to_B2==1,eval(parse(text=var))/scale,NA))
  ggplot() +
    geom_point(data =mydata_q_country,aes(x=Period,y=Peaks),shape=24,fill="darkgreen",size=3)+
    geom_point(data =mydata_q_country,aes(x=Period,y=Troughs),shape=25,fill="darkred",size=3)+
    geom_point(data =mydata_q_country,aes(x=Period,y=Phase_A1_to_A2),shape=3,color="darkgreen",size=4)+
    geom_point(data =mydata_q_country,aes(x=Period,y=Phase_B1_to_B2),shape=3,color="darkred",size=4)+
    geom_line(data=mydata_q_country, stat = "identity",aes(x = Period, y =eval(parse(text=var))/scale)) +
    scale_color_manual(values = c("lightgreen","darkgreen","red","darkred"),name="")+
    scale_x_date(date_breaks = "1 year",date_labels =  "%Y")+
    xlab("") +
    ylab("Cycle (% of trend)") +
    labs(title=NULL,subtitle =NULL ,caption=NULL)+
    theme_bw(base_size = 10) + 
    theme(axis.text.x=element_text(angle=90,hjust=1),legend.position = "bottom",axis.text=element_text(size=12))
}

fun.Phase.A1.A2.B1.B2=function(mydata,var,country){ 
  scale=1
  mydata_q_country= mydata %>% filter(ISO3_Code==country)# & year>=1980)
  mydata_q_country=mydata_q_country %>% mutate(Troughs=ifelse(Troughs==1,eval(parse(text=var))/scale,NA),
                                               Peaks=ifelse(Peaks==1,eval(parse(text=var))/scale,NA),
                                               Troughs_lab=ifelse(Troughs==1,0,NA),
                                               Peaks_lab=ifelse(Peaks==1,1,NA),
                                               Phase_A1_to_A2=ifelse(Phase_A1_to_A2==1,eval(parse(text=var))/scale,NA),
                                               Phase_B1_to_B2=ifelse(Phase_B1_to_B2==1,eval(parse(text=var))/scale,NA),
                                               var_A1=ifelse(Phase_A1==1,eval(parse(text=var))/scale,NA),
                                               var_B1=ifelse(Phase_B1==1,eval(parse(text=var))/scale,NA),
                                               var_A2=ifelse(Phase_A2==1,eval(parse(text=var))/scale,NA),
                                               var_B2=ifelse(Phase_B2==1,eval(parse(text=var))/scale,NA))
  ggplot() +
    geom_point(data =mydata_q_country,aes(x=Period,y=Peaks),shape=24,fill="darkgreen",size=3)+
    geom_point(data =mydata_q_country,aes(x=Period,y=Troughs),shape=25,fill="darkred",size=3)+
    geom_point(data =mydata_q_country,aes(x=Period,y=Phase_A1_to_A2),shape=3,color="darkgreen",size=4)+
    geom_point(data =mydata_q_country,aes(x=Period,y=Phase_B1_to_B2),shape=3,color="darkred",size=4)+
    #
    geom_line(data=mydata_q_country, stat = "identity",aes(x = Period, y =eval(parse(text=var))/scale)) +
    geom_line(data=mydata_q_country, stat = "identity",aes(x = Period, y =var_A1,color="Phase A1")) +
    geom_line(data =mydata_q_country,stat = "identity",aes(x = Period, y =var_B1,color="Phase B1")) +
    geom_line(data=mydata_q_country, stat = "identity",aes(x = Period, y =var_A2,color="Phase A2")) +
    geom_line(data =mydata_q_country,stat = "identity",aes(x = Period, y =var_B2,color="Phase B2")) +
    # 
    scale_color_manual(values = c("lightgreen","darkgreen","red","darkred"),name="")+
    scale_x_date(date_breaks = "1 year",date_labels =  "%Y")+
    xlab("") +
    ylab("Cycle (% of trend)") +
    labs(title=NULL,subtitle =NULL ,caption=NULL)+
    theme_bw(base_size = 10) + 
    theme(axis.text.x=element_text(angle=90,hjust=1),legend.position = "bottom",axis.text=element_text(size=12))
}

fun.Phase.AB.Var=function(mydata,var,country,compare_var=NULL){ 
  
  if(!is.null(compare_var)){
  mydata_q_country= mydata %>% filter(ISO3_Code==country)# & year>=2000)
  mydata_q_country=mydata_q_country %>% mutate(Troughs=ifelse(Troughs==1,max(eval(parse(text=compare_var)),na.rm=T),NA),
                                               Peaks=ifelse(Peaks==1,max(eval(parse(text=compare_var)),na.rm=T),NA),
                                               var_A=ifelse(Phase_A==1,max(eval(parse(text=compare_var)),na.rm=T),NA),
                                               var_B=ifelse(Phase_B==1,max(eval(parse(text=compare_var)),na.rm=T),NA))
  ggplot() +
    geom_bar(data=mydata_q_country, stat = "identity",aes(x = Period, y =var_A,fill="Phase A"),width=100,alpha=0.5) +
    geom_bar(data =mydata_q_country,stat = "identity",aes(x = Period, y =var_B,fill="Phase B"),width=100,alpha=0.5) +
    geom_line(data=mydata_q_country, stat = "identity",aes(x = Period, y =eval(parse(text=compare_var)))) +
    scale_fill_manual(values = c("lightgreen","darkred"),name="")+
    scale_x_date(date_breaks = "1 year",date_labels =  "%Y")+
    xlab("") +
    ylab(compare_var) +
    labs(title=NULL,subtitle =NULL ,caption=NULL)+
    theme_bw(base_size = 10) + 
    theme(axis.text.x=element_text(angle=90,hjust=1),legend.position = "bottom",axis.text=element_text(size=12))
  }else{
    mydata_q_country= mydata %>% filter(ISO3_Code==country)# & year>=2000)
     mydata_q_country=mydata_q_country %>% mutate(var_A=ifelse(Phase_A==1,1,NA),
                                                  var_B=ifelse(Phase_B==1,1,NA))
    ggplot() +
      geom_bar(data=mydata_q_country, stat = "identity",aes(x = Period, y =var_A,fill="Phase A"),width=100,alpha=0.5) +
      geom_bar(data =mydata_q_country,stat = "identity",aes(x = Period, y =var_B,fill="Phase B"),width=100,alpha=0.5) +
      #geom_line(data=mydata_q_country, stat = "identity",aes(x = Period, y =eval(parse(text=compare_var)))) +
      scale_fill_manual(values = c("lightgreen","darkred"),name="")+
      scale_x_date(date_breaks = "1 year",date_labels =  "%Y")+
      xlab("") +
      ylab(compare_var) +
      labs(title=NULL,subtitle =NULL ,caption=NULL)+
      theme_bw(base_size = 10) + 
      theme(axis.text.x=element_text(angle=90,hjust=1),legend.position = "bottom",axis.text=element_text(size=12))
    
  }
    
}

plot.Cycles=function(mydata,var,ctry,compare_var){  
  mlist=list()
  mlist[["Cycle"]]=fun.Cycle(mydata,var,ctry)
  mlist[["Turning points"]]=fun.Turning.Points(mydata,var,ctry)
  mlist[["Phases A.B"]]=fun.Phase.AB(mydata,var,ctry)
  mlist[["Phases A1.A1.B1.B2"]]=fun.Phase.A1.A2.B1.B2(mydata,var,ctry)
  mlist[[paste(compare_var," in Cycle",sep="")]]=fun.Phase.AB.Var(mydata,var,ctry,compare_var)
  return(mlist)
}



cycles=function(mydata_q,var){
  var_cycles=mydata_q %>% dplyr::select(ISO3_Code,Period,var)
  var_cycles=Expansions_Contractions_4(data.frame(var_cycles),var)
  var_cycles_stats=cycle_stats(var_cycles,var)
  #Plot_var_cycles=plot.Cycles(var_cycles,var,"MEX",var)
  var_cycles=var_cycles %>% dplyr::select(ISO3_Code,
                                          Period,
                                          A=Phase_A,
                                          B=Phase_B,
                                          A_index=Index_Phase_A,
                                          B_index=Index_Phase_B,
                                          Peak=Peaks,
                                          Trough=Troughs)
  names(var_cycles)=c("ISO3_Code","Period",paste(var,"_",names(var_cycles)[3:length(names(var_cycles))],sep=""))
  return(list(cycles=var_cycles,cycles_stats=var_cycles_stats))#,cycles_plots=Plot_var_cycles))
}

CoIncidence_Index=function(list_var_cycles,var_1,var_2){
  ndata=merge(x=list_var_cycles[[var_1]]$cycles,y=list_var_cycles[[var_2]]$cycles,by=c("ISO3_Code","Period"),all=T)
  
  ndata=ndata %>% 
    mutate(coincidence=eval(parse(text=paste(var_1,'_A',sep="")))*eval(parse(text=paste(var_2,'_A',sep="")))+
             eval(parse(text=paste(var_1,'_B',sep="")))*eval(parse(text=paste(var_2,'_B',sep="")))) %>% 
    group_by(ISO3_Code) %>%
    summarize(coincidence=sum(coincidence,na.rm=T)/n())
  names(ndata)=c("ISO3_Code",paste("CoIndex.",var_1,".",var_2,sep=""))
  return(ndata)
}

CoIndex_barchart=function(var1,var2){
  list_var_cycles=c(var1,var2)#,"FXC","X5Y_cds","bd_spread")
  list_cycles=lapply(list_var_cycles,function(x){cycles(mydata_q,x)})
  names(list_cycles)=list_var_cycles
  CoIndex=CoIncidence_Index(list_cycles,var1,var2)
  CoIndex=CoIndex %>% filter(CoIndex[,2]!=0)
  CoIndex=merge(x=CoIndex,y=ctry_classif(),by=c("ISO3_Code"))
  
  plot=ggplot()+
    geom_bar(stat='identity',aes(x=reorder(CoIndex[,"ISO3_Code"],CoIndex[,2] ),y=CoIndex[,2],fill=CoIndex[,"Classification"]))+
    #geom_hline(yintercept=median_CI,aes(fill="Median"))+
    theme_bw(base_size = 10) + 
    xlab("")+
    ylab("Coincidence Index")+
    labs(title=paste(var1," & ",var2,sep=""),subtitle =NULL ,caption=NULL)+
    theme(axis.text.x=element_text(angle=90,hjust=1),legend.position = "bottom")
  return(list(`Coincidence index`=CoIndex,plot=plot))
}

CoIncidence_Index_A=function(list_var_cycles,var_1,var_2){
  ndata=merge(x=list_var_cycles[[var_1]]$cycles,y=list_var_cycles[[var_2]]$cycles,by=c("ISO3_Code","Period"),all=T)
  
  ndata=ndata %>% filter(eval(parse(text=paste(var_1,'_A',sep="")))==1) %>%
    mutate(coincidence=eval(parse(text=paste(var_1,'_A',sep="")))*eval(parse(text=paste(var_2,'_A',sep="")))) %>% 
    group_by(ISO3_Code) %>%
    summarize(coincidence=sum(coincidence,na.rm=T)/n())
  names(ndata)=c("ISO3_Code",paste("CoIndex.",var_1,".",var_2,sep=""))
  return(ndata)
}

CoIndex_barchart_A=function(var1,var2){
  list_var_cycles=c(var1,var2)#,"FXC","X5Y_cds","bd_spread")
  list_cycles=lapply(list_var_cycles,function(x){cycles(mydata_q,x)})
  names(list_cycles)=list_var_cycles
  CoIndex=CoIncidence_Index_A(list_cycles,var1,var2)
  CoIndex=CoIndex %>% filter(CoIndex[,2]!=0)
  CoIndex=merge(x=CoIndex,y=ctry_classif(),by=c("ISO3_Code"))
  
  plot=ggplot()+
    geom_bar(stat='identity',aes(x=reorder(CoIndex[,"ISO3_Code"],CoIndex[,2] ),y=CoIndex[,2],fill=CoIndex[,"Classification"]))+
    #geom_hline(yintercept=median_CI,aes(fill="Median"))+
    theme_bw(base_size = 10) + 
    xlab("")+
    ylab("Coincidence Index")+
    labs(title=paste(var1," & ",var2,sep=""),subtitle =NULL ,caption=NULL)+
    theme(axis.text.x=element_text(angle=90,hjust=1),legend.position = "bottom")
  return(list(`Coincidence index`=CoIndex,plot=plot))
}

CoIncidence_Index_B=function(list_var_cycles,var_1,var_2){
  ndata=merge(x=list_var_cycles[[var_1]]$cycles,y=list_var_cycles[[var_2]]$cycles,by=c("ISO3_Code","Period"),all=T)
  
  ndata=ndata %>% filter(eval(parse(text=paste(var_1,'_B',sep="")))==1) %>%
    mutate(coincidence=eval(parse(text=paste(var_1,'_B',sep="")))*eval(parse(text=paste(var_2,'_B',sep="")))) %>% 
    group_by(ISO3_Code) %>%
    summarize(coincidence=sum(coincidence,na.rm=T)/n())
  names(ndata)=c("ISO3_Code",paste("CoIndex.",var_1,".",var_2,sep=""))
  return(ndata)
}

CoIndex_barchart_B=function(var1,var2){
  list_var_cycles=c(var1,var2)#,"FXC","X5Y_cds","bd_spread")
  list_cycles=lapply(list_var_cycles,function(x){cycles(mydata_q,x)})
  names(list_cycles)=list_var_cycles
  CoIndex=CoIncidence_Index_B(list_cycles,var1,var2)
  CoIndex=CoIndex %>% filter(CoIndex[,2]!=0)
  CoIndex=merge(x=CoIndex,y=ctry_classif(),by=c("ISO3_Code"))
  
  plot=ggplot()+
    geom_bar(stat='identity',aes(x=reorder(CoIndex[,"ISO3_Code"],CoIndex[,2] ),y=CoIndex[,2],fill=CoIndex[,"Classification"]))+
    #geom_hline(yintercept=median_CI,aes(fill="Median"))+
    theme_bw(base_size = 10) + 
    xlab("")+
    ylab("Coincidence Index")+
    labs(title=paste(var1," & ",var2,sep=""),subtitle =NULL ,caption=NULL)+
    theme(axis.text.x=element_text(angle=90,hjust=1),legend.position = "bottom")
  return(list(`Coincidence index`=CoIndex,plot=plot))
}

