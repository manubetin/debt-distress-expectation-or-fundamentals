list_events_by_ctry=function(mydata_q,list_var,path=NA,title="title",type="latex",label="label.table"){
  First_years_defaults=aggregate_defaults(mydata_q,list_var)
  
  First_years_defaults=First_years_defaults %>% 
    mutate(country_name=countrycode(ISO3_Code,origin="iso3c",destination = "country.name")) %>%
    dplyr::select(ISO3_Code,country_name,everything()) %>% filter(ISO3_Code!="COG") %>%
    dplyr::select(-ISO3_Code)
  
  #  First_years_defaults=First_years_defaults %>% 
  #  filter(!is.na(list_var[i]) | !is.na(SD_E.RR_first) | !is.na(SD.CV) | !is.na(SD.CT)) 
  
  if(!is.null(path)){
    stargazer(First_years_defaults,
              title=title,
              type=type,
              font.size = "footnotesize",
              table.placement="H",
              summary=F,
              label=label,
              out=path)
  }else return(First_years_defaults)
  
}
