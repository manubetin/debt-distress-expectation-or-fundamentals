##### *********************************************************************************************#####

#                       New project                                                         ####

##### *********************************************************************************************#####


##### *********************************************************************************************#####
##### set up#####
##clean environment
rm(list = ls())

## set the working directory were the script is located
current_path = rstudioapi::getActiveDocumentContext()$path 
#setwd(dirname(current_path))

#download own functions
file.sources = list.files("Functions",  pattern="*.R$", full.names=TRUE, ignore.case=TRUE)
sapply(file.sources,source,.GlobalEnv) #download the functions necessary for the simulator.

##install common packages
packages <- c("dplyr","ggplot2","rio","reshape2","zoo","grDevices","foreign","car","countrycode","tidyquant","lubridate","readxl","XML","readr","knitr",
              "kableExtra","stargazer","survival","rmarkdown","IMFData","wbstats","factoextra","Quandl","BCDating")
#install.my.packages(packages)

## load common packages
load.my.packages(packages)

##### *********************************************************************************************#####
init=mydata_init(list_countries(),start=1960,end = 2018,frequency = "month") 
init=init %>% mutate(year=year(Period))

source_data=list()
#Data on crisis from Cohen et Valladier
source_data[["CV_data"]]=import("Data/Data on crisis/source_files/Cohen_valladier.csv")
CV_data=source_data[["CV_data"]] %>% mutate(ISO3_Code=countrycode(country,origin="country.name",destination="iso3c"),
                           Period=as.Date(paste(year,"01","01",sep="-"))) %>%
  dplyr::select(ISO3_Code,Period,year,SD.CV=default)

CV_metadt=list(variables=c(SD.CV="Dummy for years of sovereign if any of the following conditions are fullfilled: i) the sum of 
                           interest and principa arrears of a country is larger than \5% of total long term debt outstanding, ii) receive 
                           support from the Paris Club (in the form of reschedulling or debt reduction. iii) It receives balance of payment
                           support by the IMF in the form of standby Arrangements or Extended Fund Facilities."),
               Source="Cohen et Valladier (2012)",
               Description="Debt distress on external debt on all type of creditors from 1970-2012, yearly frequency")
CV_ctries=unique(CV_data$ISO3_Code)

CV_data=CV_data %>% mutate(CV_Frequency=ifelse(ISO3_Code %in% CV_ctries,"year",NA),
                           CV_Sample=ifelse(ISO3_Code %in% CV_ctries,1,0))

#Data on crisis from Reinhart and Rogoff
source_data[["RR_data"]]=import("Data/Data on crisis/source_files/Reinhart_Rogoff.xls") 
RR_data=source_data[["RR_data"]] %>% mutate(year=Period,
                                            Period=paste(year,"01-01",sep="-"),
                                            RR_Sample=1,
                                            RR_Frequency="year") %>% 
  dplyr::select(ISO3_Code,
                Period,
                year,
                IC.RR=Inflation_crisis,
                CC.RR=Currency_crisis,
                SMC.RR=Stock_market_crash,
                SD_D.RR=Sovereign_crisis_dom,
                SD_E.RR=Sovereign_crisis_ext,
                RR_Sample,
                RR_Frequency)

RR_metadt=list(variables=c(IC.RR="Dummy for inflation crisis",
                           CC.RR="Dummy for currency crisis",
                           SMC.RR="Dummy for stock market crash",
                           SD_D.RR="Dummy for domestic sovereign debt crisis defined as: failutre to meet a principal or interest payment on the due data 
                           (or within the specified grace period). The episodes also include instance where reschedule debt is ultimatly extinguished
                           in terms less favourable than the original obligations",
                           SD_E.RR="Dummy for external sovereign debt crisis as: same definition than external debt plus events involving freezing of 
                           bank deposits and/or forcible conversions of such deposits from dollars to local currency "),
               Source="Reinhart and Rogoff (2009)",
               Description="External and domestic episodes of debt distress on all types of creditors (from start to the end)
               on the period 1800-2010 at a yearly basis for 68 countries")

#Data on banking crisis from Laeven and valencia


BC.LV=import("Data/Data on crisis/source_files/Syst_Banking_Crises_2018.xlsx",sheet="BC.LV")
Period_BC.LV=import("Data/Data on crisis/source_files/Syst_Banking_Crises_2018.xlsx",sheet="Period_BC.LV")


Period_BC.LV=Period_BC.LV%>% mutate(
           Period=Period_BC.LV,
           Period_BC_sys.LV=as.Date(Period_BC_sys.LV),
           year=year(Period),
           ISO3_Code=countrycode(`Country name`,origin="country.name",destination="iso3c",warn = F),
           ISO3_Code=ifelse(`Country name`=="Central African Rep.","CAF",ISO3_Code))

BC.LV=BC.LV%>% mutate(year=year_BC.LV,
                      ISO3_Code=countrycode(Country,origin="country.name",destination="iso3c",warn = F),
                      ISO3_Code=ifelse(Country=="Central African Rep.","CAF",ISO3_Code))

BC.LV=merge(x=BC.LV,y=Period_BC.LV,by=c("ISO3_Code","year"),all=T)

BC.LV=BC.LV %>% dplyr::select(ISO3_Code,Period,year,Period_BC_sys.LV,
                              BC_desc.LV,BC_Yloss.LV,BC_FiscalCost_percGDP.LV,BC_FiscalCost_percGDP_net.LV,
                              BC_FiscalCost_percFiSecAssets.LV, BC_PeakLiquidity.LV,	BC_LiquiditySupport.LV,
                              BC_PeakNPL.LV,BC_deltadebt.LV)

BC.LV=BC.LV %>% mutate(BC_freq.LV=ifelse(!is.na(Period),"monthy","yearly"),
                       Period=ifelse(BC_freq.LV=="yearly",paste0(year,"-01-01"),Period),
                       BC.LV=1) %>% 
  dplyr::select(ISO3_Code,Period,Period_BC_sys.LV,BC.LV,BC_freq.LV,
                BC_desc.LV,BC_Yloss.LV,BC_FiscalCost_percGDP.LV,BC_FiscalCost_percGDP_net.LV,
                BC_FiscalCost_percFiSecAssets.LV, BC_PeakLiquidity.LV,	BC_LiquiditySupport.LV,
                BC_PeakNPL.LV,BC_deltadebt.LV)

vars=c("BC.LV","BC_freq.LV","BC_Yloss.LV","BC_FiscalCost_percGDP.LV",
       "BC_FiscalCost_percGDP_net.LV","BC_FiscalCost_percFiSecAssets.LV",
       "BC_PeakLiquidity.LV","BC_LiquiditySupport.LV","BC_PeakNPL.LV",
       "BC_deltadebt.LV")

BC.LV=BC.LV %>% mutate_at(vars,as.numeric)

source_data[["LV_data_Banking"]]=BC.LV

LV_data_Banking=source_data[["LV_data_Banking"]] 

temp=mydata_init(list_countries(),start=1970,end=2017,frequency = "month")
LV_data=merge(x=temp,y=LV_data_Banking,by=c("ISO3_Code","Period"),all.x=T)

LV_data=LV_data %>% mutate(BC.LV=ifelse(is.na(BC.LV),0,BC.LV),
                           year=year(Period)) %>% arrange(ISO3_Code,Period)

#Data on sovereign crisis from Laeven and valencia
source_data[["LV_data_Sovereign"]]=import("Data/Data on crisis/source_files/Laeven_Valencia_sovereign_debt_crises.xlsx")
LV_data_Sovereign=source_data[["LV_data_Sovereign"]] %>% mutate(SD.LV=1,
                                           Period=as.Date(paste(Year,"01","01",sep="-")),
                                           ISO3_Code=countrycode(Country,origin="country.name",destination="iso3c")) %>%
  dplyr::select(ISO3_Code,year=Year,Period,SD.LV)

LV_data=merge(x=LV_data,y=LV_data_Sovereign,by=c("ISO3_Code","Period","year"),all.x=T)
LV_data=LV_data %>% mutate(SD.LV=ifelse(is.na(SD.LV),0,SD.LV))

#Data on currency crisis from Laeven and valencia
source_data[["LV_data_currency"]]=read.csv("Data/Data on crisis/source_files/Laeven_Valencia_currency_crises.csv")
LV_data_currency=source_data[["LV_data_currency"]] %>% mutate(CC.LV=1,
                                           Period=as.Date(paste(Year,"01","01",sep="-")),
                                           ISO3_Code=countrycode(Country,origin="country.name",destination="iso3c")) %>%
  dplyr::select(ISO3_Code,Period,year=Year,CC.LV)

LV_data=merge(x=LV_data,y=LV_data_currency,by=c("ISO3_Code","Period","year"),all.x=T)
LV_data=LV_data %>% mutate(CC.LV=ifelse(is.na(CC.LV),0,CC.LV),
                           LV_Sample=1,
                           LV_Frequency="year") %>% dplyr::select(-year)

LV_metadt=list(variables=c(SD.LV="Dummy for Sovereign debt crisis",
                           BC_LV="Dummy for banking crisis",
                           CC_LV="Dummy for currency crisis"),
               Source="Laeven and Valencia",
               Description="...")

##Trebesch
#2014 update building on AEJ Macro 2013 paper "Sovereign Defaults: The Price of Haircuts", by Juan Cruces and Christoph Trebesch																			
		
source_data[["CT_data"]]=import("Data/Data on crisis/source_files/Cruces-Trebesch-Haircuts-2014-update.xlsx",sheet="2014 Version (new)")
CT_data=source_data[["CT_data"]] %>% mutate(ISO3_Code=Code,
                                            Period=as.Date(as.yearmon(paste(year,month,sep="-"),format="%Y-%m")),
                                            Debt.restruct.CT=as.numeric(Debt_Restructured),
                                            Pref.haircut.CT=as.numeric(Preferred_Haircut),
                                            Und.disc.rate.CT=as.numeric(Underlying_Discount_Rate),
                                            Market.haircut.CT=as.numeric(Market_Haircut),
                                            FV.reduct.CT=as.numeric(FV_Reduction),
                                            CT_Sample=1,
                                            CT_Frequency="month") %>%
  dplyr::select(ISO3_Code,
                Period,
                Debt.restruct.CT,
                Pref.haircut.CT,
                Und.disc.rate.CT,
                Market.haircut.CT,
                FV.reduct.CT,
                CT_Sample,
                CT_Frequency)

dt=mydata_init(countries = list_countries(),start="1970",end="2014",frequency = "month")
CT_data=merge(x=dt,y=CT_data,by=c("ISO3_Code","Period"),all=T)
 
CT_data2=CT_data[,3:dim(CT_data)[2]] %>% replace(.,is.na(.),0)

CT_data=cbind(CT_data[,1:2],CT_data2)


CT_metadt=list(variables=c(Debt.restruct.CT="The amount of debt restructured in millions of current US$ (including arrears, excluding holdouts)",
                           Pref.haircut.CT="SZ Haircut [HSZ as in equation (2), 'preferred']. This is our preferred haircut measure. It follows the estimation approach by Sturzenegger/Zettelmeyer and compares the present value of the old debt to the present value of the new debt (using the same discount rate).",
                           Und.disc.rate.CT="The discount rate used to value future cash flows",
                           Market.haircut.CT="Market Haircut [HM as in equation (1)]. This is the measure often used by market participants and the financial press. It takes the old debt at face value and compares this to the present value of the new debt.",
                           FV.reduct.CT="'Face Value Reduction' measure (in %), This measure shows nominal debt reduction only and does not take into account creditor losses due to maturity extensions or changes in interest rates."),
               Source="'Sovereign Defaults: The Price of Haircuts', by Juan Cruces and Christoph Trebesch",
               Description="Data provides a list of 187 distressed sovereign debt restructurings with external private creditors (banks and bondholders) occuring between 1970 and 2013. We also provide haircut estimates and further information on each deal. Data coverage: worldwide, 1970-2013.This update: August 20, 2014")


#ISDA defaults

source_data[["ISDA_data"]]=import("Data/Data on crisis/source_files/CDS.sovereign.defaults.xls")
ISDA_data=source_data[["ISDA_data"]] %>% mutate(Period=as.Date(Period),CDS.ISDA=as.numeric(CDS.ISDA))

#ISDA_data=source_data[["ISDA_data"]]


ISDA_metadt=list(variables=c(SD.ISDA.day="Day at which the CDS contract has been executed following the decision
                             of the Credit Derivatives Determination Commitees (CDDC)",
                           CDS.ISDA="Auction price of CDS after default",
                           SD.ISDA="Dummy equal to one when ISDA default has been triggered"),
                Source="International Swap Derivaties Association, https://www.isda.org/1970/01/01/otc-derivatives-data-sources/",
                Description="Default is at least one of the following event occure: Bankcrupcy,failure to pay, repudiation/moratorium
                , obligation acceleration, restructuring")

#Data on crisis from Beers  & Mavalwalla

source_data[["BM_data"]]=import("Data/Data on crisis/source_files/crag-database-update-30-06-17.xlsx")
BM_data=source_data[["BM_data"]] %>% mutate(ISO3_Code=countrycode(Country,origin="country.name",destination="iso3c"),
                                            Period=as.Date(paste(Year,"01","01",sep="-")),
                                            SD.debt.Tot.BM=Total,
                                            SD.debt.IMF.BM=IMF,
                                            SD.debt.OthersOffCred.BM=`Other official creditors`,
                                            SD.debt.PrivateCred.BM=`Private creditors`,
                                            SD.debt.FCBankloans.BM=`FC bank loans`,
                                            SD.debt.FCBonds.BM=`FC bonds`,
                                            SD.debt.LCdebt.BM=`LC debt`,
                                            SD.debt.IBRD.BM=`IBRD`,
                                            SD.debt.ParisClub.BM=`Paris Club`,
                                            SD.BM=ifelse(SD.debt.Tot.BM>0,1,0),
                                            BM_Sample=1,
                                            BM_Frequency="year") %>%
  dplyr::select(ISO3_Code,Period,year=Year,
                SD.BM,
                SD.debt.Tot.BM,
                SD.debt.IMF.BM,
                SD.debt.OthersOffCred.BM,
                SD.debt.PrivateCred.BM,
                SD.debt.FCBankloans.BM,
                SD.debt.FCBonds.BM,
                SD.debt.LCdebt.BM,
                SD.debt.IBRD.BM,
                SD.debt.ParisClub.BM,
                BM_Sample,
                BM_Frequency)

BM_metadt=list(variables=c(SD.debt.Tot.BM="Nominal value of total debt in default in dollars",
                           SD.debt.IMF.BM="Nominal value of debt due to the IMF in default in dollars, computed as cumulative interest arrears and charges and applied them to the principal amount
of loans and overdue quota amounts reported as being for at least six month",
                           SD.debt.OthersOffCred.BM="Nominal value of debt due to other official creditors in default in dollars, loan arrears by governments due to the MLIs and bilateral official creditors, including national export credit and development agencies,
data from World Databank that reports cumulative annual amounts of unpaid interest and principal as well as resturctured debt and write-offs of interest and principal",
                           SD.debt.PrivateCred.BM="Nominal value of debt due to private creditors in default in dollars, Foreign currency-denominated lending to governments by foreign commercial creditors, including bondholders, banks and suppliers (source World DataBank)
cumulative annual amounts of unpaid interest and principal for this category of creditors, as well as write offs and restructurings",
                           SD.debt.FCBankloans.BM="Nominal value of Foreign currency bank loans in default in dollars, foreign currency-denominated bilateral and syndicated loans to government by commericla banks.",
                           SD.debt.FCBonds.BM="Nominal value of Foreign currency bonds in default in dollars, foreign currency denominated bonds and other marketable secrutiies issued by governments. Where bond interest is due but unpaid,
#we estimate cumulative interest arrears for the years from the start to the end of the bond default based on reported bond coupons.",
                           SD.debt.LCdebt.BM="Nominal value of local currency debt in default in dollars, debt issued by a government in its own currency",
                           SD.debt.IBRD.BM="Nominal value of debt due to the IBRD in default in dollars",
                           SD.debt.ParisClub.BM="Nominal value of debt due to the Paris Club in default in dollars"),
               Source="Database of Sovereign defaults 2017, David Beers & Jamshid Mavalwalla, update of GRAG's sovereign database: http://www/bankofcanada.ca/wp-content/uploads/2017/07/crag-database-update-30-06-17.xlsx",
               Description="1960-2016, Default occure when debt service is not paid on the due date or within a specified grace period, when payments are not
               made within the time frame specified under a guarantee, or, absent an outright payment default, in any of the following circumstances where 
               creditors incur material payment default, in any of the following circumstances: aggreement between governments and creditors
               that reduces interest rates and/or extend maturities on outstanding debt, Government exchange offers to creditors where existing debt is 
               swapped for new debt on less-economic terms. government purchases of debt at substantial discounts to par. Government redenomination of foreign currency debt into
               new local currency obligations on less-economic term. Swaps of sovereign debt for equity (usually relating to privatization programs) on less-economic terms
               Retrospective taxes targeting sovereign debt service payments. Conversion of central bank notes into new currency of 
               less-than-equivalent face value")



#Data on crisis from Beers  & Mavalwalla

var=list("IMF SBA","IMF SBA 5","SBA AU","IMF EFF",
         "IMF EFF 5","IMF SAF","IMF SAF 5","IMF PRGF",
         "IMF PRGF 5","PRGF AU","IMF PLL","IMF PCL",
         "IMF PLL 5","IMF PCL 5","IMF ESF","IMF ESF 5","IMF FCL","IMF FCL 5")

IMF_intervention_data=lapply(var, function(x){
  source_data[[paste0("IMF_programs_",x)]]=import("Data/Data on crisis/source_files/IMF_WB_programs.xls",sheet=x)
  names(source_data[[paste0("IMF_programs_",x)]])
  
  BM_data=melt(source_data[[paste0("IMF_programs_",x)]],id=c("Country Code","Country Name"))
  BM_data=BM_data %>% mutate(year=variable) %>%
    rename(ISO3_Code=`Country Code`) %>% mutate(myvar=gsub(" ","",x),Period=year) %>% dplyr::select(ISO3_Code,Period,year,myvar,value)
  #names(BM_data)[3]=x
  BM_data=BM_data %>% arrange(ISO3_Code,year)
  return(BM_data)
})
names(IMF_intervention_data)=var
IMF_intervention_data=do.call(rbind,IMF_intervention_data)
IMF_intervention_data=IMF_intervention_data %>% mutate(value=as.numeric(value))
IMF_intervention_data=dcast(IMF_intervention_data,ISO3_Code+year+Period~myvar)

IMF_intervention_metadt=list(variables=c(IMF_SBA="IMF Standby Arrangement agreed, dummy. The SBA framework allows the fund to respond quickly to countries' external financing needs, and to support policies designd to help them emerge from crisis
               and restore sustainable growth. SBA are more often used by middle income member countries, the length of the SBA is flexible and typically covers a period of 12-24 months (no more than 36 months) consistent
                with addressing short-term balance of payment problems.",
                           `IMFSBA5`="IMF Standby Arrangement in effect for at least 5 months in a particular year, dummy.",
                           `SBAAU`="Augmentation of an IMF Standby Arrangement, dummy.",
                           `IMFEFF`="IMF Extended Fund Facility Arrangement agreed, dummy. IMF extend fund facility (EFF) provide assistance to countries (i) experiencing serious payment imbalances because of 
               structural impediments, or (ii) characterized by slow growth and inherently weak balance of payment positions. The EFF provides assistance in support of a comprehensive programs that include policies of
               the scope and character required to correct structural imbalances over an extended period.",
                           `IMFEFF5`="IMF Extended Fund Facility Arrangement in effect for at least 5 months in a particular year, dummy.",
                           `IMFSAF`="IMF Structural Adjustment Facility Arrangement agreed, dummy.",
                           `IMFSAF5`="IMF Structural Adjustment Facility Arrangement in effect for at least 5 months in a particular year, dummy.",
                           `IMFPRGF`="IMF Poverty Reduction and Growth Facility Arrangement agreed, dummy.",
                           `IMFPRGF5`="IMF Poverty Reduction and Growth Facility Arrangement in effect for at least 5 months in a particular year, dummy.",
                           `PRGFAU`="Augmentation of an IMF Poverty Reduction and Growth Facility Arrangement, dummy.",
                           `IMFPLL`="IMF Precautionary and Liquidity Line Arrangement agreed, dummy. provide financing to meet actual or potential balance of payment needs of countries with sound policies and is intended to serve as a backstrop of help
                           resolve crsis under wide-ranging situations",
                           `IMFPLL5`="IMF Precautionary and Liquidity Line Arrangement in effect for at least 5 months in a particular year, dummy.",
                           `IMFPCL`="IMF Precautionary Credit Line Arrangement agreed, dummy.",
                           `IMFPCL5`="IMF Precautionary Credit Line Arrangement in effect for at least 5 months in a particular year, dummy.",
                           `IMFESF`="IMF Exogenous Shock Facility Arrangement agreed, dummy.",
                           `IMFESF5`="IMF Exogenous Shock Facility Arrangement in effect for at least 5 months in a particular year, dummy.",
                           `IMFFCL`="IMF Flexible Credit Line Arrangement agreed, dummy. Instrument  for crisis prevention ad crisis mitigation lending for countries with very strong policy frameworks abd track records in economic performance ",
                           `IMFFCL5`="IMF Flexible Credit Line Arrangement in effect for at least 5 months in a particular year, dummy."),
               Source="Dreher, Axel, IMF and Economic Growth: The Effects of Programs, Loans, and Compliance with Conditionality, World Development 34, 5: 769-788 (2006).",
               Description="update July 2017: ")



#Data on IMF programs collected on IMF website

dt=import("Data/Data on crisis/source_files/IMF_programs_update_2.xlsx")

dt=dt%>% mutate(month_arrangement=substring(Date_arrangement,1,3),
                day_arrangement=substring(Date_arrangement,5,6),
                year_arrangement=substring(Date_arrangement,9,13),
                month_expir=substring(Expiration_date,1,3),
                day_expir=substring(Expiration_date,5,6),
                year_expir=substring(Expiration_date,9,13))

name_month=c('Apr','Aug','Dec','Feb','Jan','Jul','Jun','Mar','May','Nov','Oct','Sep')
num_month=c("04","08",'12','02',"01","07","06","03","05","11","10","09")
match_month=cbind(name_month,num_month)
colnames(match_month)=c("month","numeric_month_arrangement")

dt=merge(x=dt,y=match_month,by.x=c("month_arrangement"),by.y=c("month"),all.x=T)

colnames(match_month)=c("month","numeric_month_expir")
dt=merge(x=dt,y=match_month,by.x=c("month_expir"),by.y=c("month"),all.x=T)

dt=dt%>% mutate(Period=ifelse(!is.na(year_arrangement),paste(year_arrangement,numeric_month_arrangement,"01",sep="-"),NA))
dt=dt %>% arrange(ISO3_Code,Period) %>% dplyr::select(ISO3_Code,Period,everything())
dt=dt %>% dplyr::select(-c(month_arrangement,numeric_month_arrangement,year_arrangement))
dt=dt %>% mutate(Period=as.Date(Period))

dt=dt%>% mutate(Date_Expiration=ifelse(!is.na(year_expir),paste(year_expir,numeric_month_expir,day_expir,sep="-"),NA))
dt=dt %>% dplyr::select(-c(month_expir,day_expir,numeric_month_expir,year_expir,Expiration_date))

dt=dt %>% mutate(duration_IMFprogram=as.numeric(year(as.Date(Date_Expiration)))-as.numeric(year(as.Date(Period))),
                 Sup_reserves_facilities=ifelse(is.na(Sup_reserves_facilities),0,1))

rio::export(dt,"Data/Data on crisis/source_files/IMF_programs_clean.RData")

ctries=unique(dt$ISO3_Code)
dt2=mydata_init(ctries,start=1960,end=2018,frequency = "month")
dt2=dt2 %>% mutate(ISO3_Code=as.character(ISO3_Code))

dt=merge(x=dt2,y=dt,by=c("ISO3_Code","Period"),all=T)

dt=dt %>% mutate(Date_arrangement=ifelse(is.na(Date_arrangement),0,Date_arrangement),
              Amount_agreed=ifelse(is.na(Amount_agreed),0,Amount_agreed),
              Amount_drawn=ifelse(is.na(Amount_drawn),0,Amount_drawn),
              Amount_Outstanding=ifelse(is.na(Amount_Outstanding),0,Amount_Outstanding),
              Sup_reserves_facilities=ifelse(is.na(Sup_reserves_facilities),0,Sup_reserves_facilities),
              day_arrangement=ifelse(is.na(day_arrangement),0,day_arrangement),
              Date_Expiration=ifelse(is.na(Date_Expiration),0,Date_Expiration))

fun_Amount_IMF_programs=function(var){
  dt_figure= dt %>% mutate(year=year(Period),
                           variable=substr(variable,4,7)) %>% group_by(year,variable) %>% summarize(myvar=sum(eval(parse(text=var)),na.rm=T)) %>% filter(!is.na(variable))
  plot=ggplot(dt_figure)+
    geom_bar(stat = "identity",aes(x=year,y=myvar,fill=variable)) +
    theme_bw()+
    labs(fill=NULL,
         x=NULL,
         y=var,
         caption="In thousands of SDRs")+
    #ylim(c(0,250000))+
    theme(legend.position="bottom")
  plot
}

# names(dt)
# fun_Amount_IMF_programs("Amount_agreed")
# ggsave("Output/Graphs/Business cycles/IMF_programs/IMFprograms_Amount_aggreed.png")
# fun_Amount_IMF_programs("Amount_drawn")
# ggsave("Output/Graphs/Business cycles/IMF_programs/IMFprograms_Amount_drawn.png")
# fun_Amount_IMF_programs("Amount_Outstanding")
# ggsave("Output/Graphs/Business cycles/IMF_programs/IMFprograms_Amount_Oustanding.png")

dt=dt %>% mutate(IMF_EFF=ifelse(variable=="IMFEFF",1,0),
                 IMF_SBA=ifelse(variable=="IMFSBA",1,0),
                 IMF_SAF=ifelse(variable=="IMFSAF",1,0),
                 IMF_SCF=ifelse(variable=="IMFSCF",1,0),
                 IMF_ECF=ifelse(variable=="IMFECF",1,0),
                 IMF_ESF=ifelse(variable=="IMFESF",1,0),
                 IMF_PLL=ifelse(variable=="IMFPLL",1,0),
                 IMF_FCL=ifelse(variable=="IMFFCL",1,0))

dt=dt %>% mutate(IMFEFF_Amount_agreed=ifelse(IMF_EFF==1,Amount_agreed,0),
                 IMFSBA_Amount_agreed=ifelse(IMF_SBA==1,Amount_agreed,0),
                 IMFSAF_Amount_agreed=ifelse(IMF_SAF==1,Amount_agreed,0),
                 IMFSCF_Amount_agreed=ifelse(IMF_SCF==1,Amount_agreed,0),
                 IMFECF_Amount_agreed=ifelse(IMF_ECF==1,Amount_agreed,0),
                 IMFESF_Amount_agreed=ifelse(IMF_ESF==1,Amount_agreed,0),
                 IMFPLL_Amount_agreed=ifelse(IMF_PLL==1,Amount_agreed,0),
                 IMFFCL_Amount_agreed=ifelse(IMF_FCL==1,Amount_agreed,0))

dt=dt %>% mutate(IMFEFF_Amount_drawn=ifelse(IMF_EFF==1,Amount_drawn,0),
                 IMFSBA_Amount_drawn=ifelse(IMF_SBA==1,Amount_drawn,0),
                 IMFSAF_Amount_drawn=ifelse(IMF_SAF==1,Amount_drawn,0),
                 IMFSCF_Amount_drawn=ifelse(IMF_SCF==1,Amount_drawn,0),
                 IMFECF_Amount_drawn=ifelse(IMF_ECF==1,Amount_drawn,0),
                 IMFESF_Amount_drawn=ifelse(IMF_ESF==1,Amount_drawn,0),
                 IMFPLL_Amount_drawn=ifelse(IMF_PLL==1,Amount_drawn,0),
                 IMFFCL_Amount_drawn=ifelse(IMF_FCL==1,Amount_drawn,0))

dt=dt %>% mutate(IMFEFF_Amount_Outstanding=ifelse(IMF_EFF==1,Amount_Outstanding,0),
                 IMFSBA_Amount_Outstanding=ifelse(IMF_SBA==1,Amount_Outstanding,0),
                 IMFSAF_Amount_Outstanding=ifelse(IMF_SAF==1,Amount_Outstanding,0),
                 IMFSCF_Amount_Outstanding=ifelse(IMF_SCF==1,Amount_Outstanding,0),
                 IMFECF_Amount_Outstanding=ifelse(IMF_ECF==1,Amount_Outstanding,0),
                 IMFESF_Amount_Outstanding=ifelse(IMF_ESF==1,Amount_Outstanding,0),
                 IMFPLL_Amount_Outstanding=ifelse(IMF_PLL==1,Amount_Outstanding,0),
                 IMFFCL_Amount_Outstanding=ifelse(IMF_FCL==1,Amount_Outstanding,0))

dt=dt %>% mutate(IMFEFF_Date_expir=ifelse(IMF_EFF==1,Date_Expiration,0),
                 IMFSBA_Date_expir=ifelse(IMF_SBA==1,Date_Expiration,0),
                 IMFSAF_Date_expir=ifelse(IMF_SAF==1,Date_Expiration,0),
                 IMFSCF_Date_expir=ifelse(IMF_SCF==1,Date_Expiration,0),
                 IMFECF_Date_expir=ifelse(IMF_ECF==1,Date_Expiration,0),
                 IMFESF_Date_expir=ifelse(IMF_ESF==1,Date_Expiration,0),
                 IMFPLL_Date_expir=ifelse(IMF_PLL==1,Date_Expiration,0),
                 IMFFCL_Date_expir=ifelse(IMF_FCL==1,Date_Expiration,0))

dt=dt %>% mutate(IMFEFF_SupResFacilities=ifelse(IMF_EFF==1 & !is.na(Sup_reserves_facilities),1,0),
                 IMFSBA_SupResFacilities=ifelse(IMF_SBA==1 & !is.na(Sup_reserves_facilities),1,0),
                 IMFSAF_SupResFacilities=ifelse(IMF_SAF==1 & !is.na(Sup_reserves_facilities),1,0),
                 IMFSCF_SupResFacilities=ifelse(IMF_SCF==1 & !is.na(Sup_reserves_facilities),1,0),
                 IMFECF_SupResFacilities=ifelse(IMF_ECF==1 & !is.na(Sup_reserves_facilities),1,0),
                 IMFESF_SupResFacilities=ifelse(IMF_ESF==1 & !is.na(Sup_reserves_facilities),1,0),
                 IMFPLL_SupResFacilities=ifelse(IMF_PLL==1 & !is.na(Sup_reserves_facilities),1,0),
                 IMFFCL_SupResFacilities=ifelse(IMF_FCL==1 & !is.na(Sup_reserves_facilities),1,0))

dt=dt %>% dplyr::select(-c(variable,Amount_agreed,Amount_drawn,Amount_Outstanding,Sup_reserves_facilities,Date_Expiration))

dt2=dt[,4:dim(dt)[2]] #%>% replace(.,is.na(.),0)

dt=cbind(dt[,1:3],dt2)

IMFprograms_dt=dt

IMFprograms_dt %>% filter(ISO3_Code=="AFG" & !is.na(IMFECF_Amount_drawn))

IMF_programs_metadt=list(variables=c(IMF_SBA="IMF Standby Arrangement agreed, dummy. The SBA framework allows the fund to respond quickly to countries' external financing needs, and to support policies designd to help them emerge from crisis
               and restore sustainable growth. SBA are more often used by middle income member countries, the length of the SBA is flexible and typically covers a period of 12-24 months (no more than 36 months) consistent
                                         with addressing short-term balance of payment problems.",
                                         `IMF_EFF`="IMF Extended Fund Facility Arrangement agreed, dummy. IMF extend fund facility (EFF) provide assistance to countries (i) experiencing serious payment imbalances because of 
                                         structural impediments, or (ii) characterized by slow growth and inherently weak balance of payment positions. The EFF provides assistance in support of a comprehensive programs that include policies of
                                         the scope and character required to correct structural imbalances over an extended period.",
                                     IMF_ECF="Extended Credit facilities",
                                     `IMF_SAF`="IMF Structural Adjustment Facility Arrangement agreed, dummy.",
                                         `IMF_PLL`="IMF Precautionary and Liquidity Line Arrangement agreed, dummy. provide financing to meet actual or potential balance of payment needs of countries with sound policies and is intended to serve as a backstrop of help
                                         resolve crsis under wide-ranging situations",
                                         `IMF_PCL`="IMF Precautionary Credit Line Arrangement agreed, dummy.",
                                         `IMF_ESF`="IMF Exogenous Shock Facility Arrangement agreed, dummy.",
                                         `IMF_FCL`="IMF Flexible Credit Line Arrangement agreed, dummy. Instrument  for crisis prevention ad crisis mitigation lending for countries with very strong policy frameworks abd track records in economic performance "),
                             Source="IMF Website: https://www.imf.org/external/np/fin/tad/extarr2.aspx?memberKey1=670&date1key=2015-06-30",
                             Description="Downloaded on IMF website by hand using following google search: 'history of lending arrangement country IMF' ")



#Merge datasets for yearly frequencies
mydata=init
datasets=c("CV_data","RR_data","BM_data","IMF_intervention_data")
for(dt in datasets){
  dt=eval(parse(text=dt)) %>% dplyr::select(-Period)
  mydata=merge(x=mydata,y=dt,by=c("ISO3_Code","year"),all.x=T)
}

#Merge datasets for monthly frequencies
datasets=c("CT_data","ISDA_data","LV_data","IMFprograms_dt")
for(dt in datasets){
  mydata=merge(x=mydata,y=eval(parse(text=dt)),by=c("ISO3_Code","Period"),all.x=T)
}


##

get_SDR_USD=function(){
  dt=read_excel("Data/Data on crisis/source_files/SDRV.xlsx","SDRV",col_names = F)
  first_row=11
  extract_dt=function(i){
    if(i<5172 ){
      dt= dt[first_row+i*9,4:5]
    }else if(i<=9197){
      first_row=46568
      i=i-5172
      dt= dt[(first_row+i*8)+6,4:5]
    }else{
      first_row=78784
      i=i-9197
      dt= dt[(first_row+i*9)+6,4:5]
    }
    dt
  }
  
  SDR_USD=lapply(1:10454,function(i){
    extract_dt(i)
  })
  SDR_USD=do.call(rbind,SDR_USD)
  SDR_USD=SDR_USD %>% rename(Period=X__4,SDR_USD=X__5) %>% mutate(Period=as.Date(Period),SDR_USD=as.numeric(SDR_USD))
  return(SDR_USD)
}

SDR_USD=get_SDR_USD()
SDR_USD=SDR_USD %>% mutate(year=year(Period),
                           quarter=quarter(Period)) %>% group_by(year,quarter) %>% summarize(SDR_USD=last(SDR_USD))

#export(catalogue,"Data/Data on crisis/catalogue.xlsx")
mydata =mydata %>% mutate(ISO3_Code=as.character(ISO3_Code))

## Plots dates defaults

#merge data of google trend
google_trend_data=import("Data/Data on crisis/source_files/IMF_countries_GoogleTrend2.csv")
google_trend_data=melt(google_trend_data,id=("Period"))
google_trend_data=google_trend_data %>% rename(ISO3_Code=variable) %>%
  mutate(Period=as.Date(as.yearmon(Period)),
         Spike_GoogleTrend=ifelse(value>100,1,0)) %>% dplyr::select(-value)

mydata=merge(x=mydata,y=google_trend_data,by=c("ISO3_Code","Period"),all.x=T) 

#summary stats google trend spikes
google_trend_spikes=google_trend_data %>%
  filter(Spike_GoogleTrend==1) %>% group_by(ISO3_Code) %>% 
  summarize(Spike_GoogleTrend=paste(Period,collapse=" , "))

#convert data to quaterly and impute the default to the start of the month for source with yearly frequency

mean.na.omit=function(x){
  x=mean(x,na.rm=T)
  return(x)
}

# sapply(names(mydata),function(x){
#   (paste(x,"  ",class(mydata[,x]),sep=""))
# })
# class(mydata$ISO3_Code)

vars=names(mydata)[4:length(names(mydata))] 
mydata=mydata %>% mutate_at(vars,as.numeric) %>% mutate(quarter=quarter(Period))

mydata_q= mydata %>% group_by(ISO3_Code,year,quarter) %>% summarize_if(is.numeric,mean.na.omit)

mydata_q=mydata_q %>% mutate(Period=as.Date(as.yearqtr(paste(year,quarter,sep="-Q"),format="%Y-Q%q")))
mydata_q=mydata_q %>% mutate(BC.LV=round(BC.LV*3))

impute_default_to_first_quarter=function(mydata){
  mydata=mydata %>% mutate(SD.CV=ifelse(quarter(Period)!=1,0,SD.CV),
                           SD_E.RR=ifelse(quarter(Period)!=1,0,SD_E.RR),
                           SD_D.RR=ifelse(quarter(Period)!=1,0,SD_D.RR),
                           SD.debt.IMF.BM=ifelse(quarter(Period)!=1,0,SD.debt.IMF.BM),
                           SD.debt.PrivateCred.BM=ifelse(quarter(Period)!=1,0,SD.debt.PrivateCred.BM),
                           SD.debt.ParisClub.BM=ifelse(quarter(Period)!=1,0,SD.debt.ParisClub.BM),
                           SD.debt.FCBonds.BM=ifelse(quarter(Period)!=1,0,SD.debt.FCBonds.BM),
                           SD.debt.FCBankloans.BM=ifelse(quarter(Period)!=1,0,SD.debt.FCBankloans.BM),
                           SD.debt.LCdebt.BM=ifelse(quarter(Period)!=1,0,SD.debt.LCdebt.BM),
                           SD.debt.LCdebt.BM=ifelse(quarter(Period)!=1,0,SD.debt.LCdebt.BM),
                           SD.debt.Tot.BM=ifelse(quarter(Period)!=1,0,SD.debt.Tot.BM),
                           IMFEFF=ifelse(quarter(Period)!=1,0,IMFEFF),
                           IMFEFF5=ifelse(quarter(Period)!=1,0,IMFEFF5),
                           IMFSBA=ifelse(quarter(Period)!=1,0,IMFSBA),
                           IMFSBA5=ifelse(quarter(Period)!=1,0,IMFSBA5),
                           IMFSAF=ifelse(quarter(Period)!=1,0,IMFSAF),
                           IMFESF=ifelse(quarter(Period)!=1,0,IMFESF),
                           IMFFCL=ifelse(quarter(Period)!=1,0,IMFFCL),
                           IMFPLL=ifelse(quarter(Period)!=1,0,IMFPLL),
                           IMFPCL=ifelse(quarter(Period)!=1,0,IMFPCL),
                           IMFPRGF=ifelse(quarter(Period)!=1,0,IMFPRGF),
                           IMFSBA=ifelse(quarter(Period)!=1,0,IMFSBA),
                           PRGFAU=ifelse(quarter(Period)!=1,0,PRGFAU),
                           SBAAU=ifelse(quarter(Period)!=1,0,SBAAU),
                           IMF_program=ifelse(IMFSBA==1 | IMFEFF==1,1,0),
                           IMF_precaution_program=ifelse(IMFFCL==1 | IMFPLL==1 | IMFPCL==1,1,0),
                           SMC.RR=ifelse(quarter(Period)!=1,0,SMC.RR),
                           CC.RR=ifelse(quarter(Period)!=1,0,CC.RR))
  
  #Compute dates when first event of defaults.
  mydata = mydata %>%
    group_by(ISO3_Code) %>%  arrange(ISO3_Code,Period) %>%
    mutate(SD.CT=ifelse(Debt.restruct.CT>0,1,0),
           SD.RR_first=ifelse((SD_E.RR==1 & lag(SD_E.RR,4)==0) | (SD_D.RR==1 & lag(SD_D.RR,4)==0),1,0),
           SD_E.RR_first=ifelse((SD_E.RR==1 & lag(SD_E.RR,4)==0),1,0),
           SD_E.RR_last=ifelse((SD_E.RR==1 & lead(SD_E.RR,4)==0),1,0),
           SD_D.RR_first=ifelse((SD_D.RR==1 & lag(SD_D.RR,4)==0),1,0),
           SD_IMF.BM=ifelse(SD.debt.IMF.BM>0,1,0),
           SD_IMF.BM_first=ifelse(SD.debt.IMF.BM>0 & dplyr::lag(SD.debt.IMF.BM,12)==0,1,0),
           SD_PrivCred.BM=ifelse(SD.debt.PrivateCred.BM>0,1,0),
           SD_PrivCred.BM_first=ifelse(SD.debt.PrivateCred.BM>0 & dplyr::lag(SD.debt.PrivateCred.BM,4)==0,1,0),
           SD_ParisClub.BM=ifelse(SD.debt.ParisClub.BM>0,1,0),
           SD_ParisClub.BM_first=ifelse(SD.debt.ParisClub.BM>0 & dplyr::lag(SD.debt.ParisClub.BM,4)==0,1,0),
           SD_FCBonds.BM=ifelse(SD.debt.FCBonds.BM>0,1,0),
           SD_FCBonds.BM_first=ifelse(SD.debt.FCBonds.BM>0 & dplyr::lag(SD.debt.FCBonds.BM,4)==0,1,0),
           SD_FCBankloans.BM=ifelse(SD.debt.FCBankloans.BM>0,1,0),
           SD_FCBankloans.BM_first=ifelse(SD.debt.FCBankloans.BM>0 & dplyr::lag(SD.debt.FCBankloans.BM,12)==0,1,0),
           SD_LCdebt.BM=ifelse(SD.debt.LCdebt.BM>0,1,0),
           SD_LCdebt.BM_first=ifelse(SD.debt.LCdebt.BM>0 & dplyr::lag(SD.debt.LCdebt.BM,4)==0,1,0),
           SD_Tot.BM=ifelse(SD.debt.Tot.BM>0,1,0),
           SD_Tot.BM_first=ifelse(SD.debt.Tot.BM>10 & dplyr::lag(SD.debt.Tot.BM,4)<=10,1,0),
           SD_Tot.BM_last=ifelse(SD.debt.Tot.BM>10 & dplyr::lead(SD.debt.Tot.BM,4)<=10,1,0),
           SD.manu_def=ifelse(SD_FCBonds.BM_first==1 | SD_LCdebt.BM_first==1 | SD_FCBankloans.BM_first==1,1,0),
           SD.manu_def_first=ifelse(SD.manu_def==1 & dplyr::lag(SD.manu_def,4)==0,1,0),
           IMFSBA_first=ifelse(IMFSBA==1 & dplyr::lag(IMFSBA,4)==0,1,0),
           IMFEFF_first=ifelse(IMFEFF==1 & dplyr::lag(IMFEFF,4)==0,1,0),
           IMFSBA5_first=ifelse(IMFSBA5==1 & dplyr::lag(IMFSBA5,4)==0,1,0),
           IMFEFF5_first=ifelse(IMFEFF5==1 & dplyr::lag(IMFEFF5,4)==0,1,0),
           IMFSAF_first=ifelse(IMFSAF==1 & dplyr::lag(IMFSAF,4)==0,1,0),
           IMF_program_first=ifelse(IMF_program==1 & dplyr::lag(IMF_program,4)==0,1,0),
           IMF_precaution_program_first=ifelse(IMF_precaution_program==1 & dplyr::lag(IMF_precaution_program,4)==0,1,0),
           SMC.RR_first=ifelse((SMC.RR==1 & lag(SMC.RR,4)==0),1,0),
           CC.RR_first=ifelse((CC.RR==1 & lag(CC.RR,4)==0),1,0))
  return(mydata)
}

mydata_q=mydata_q %>% impute_default_to_first_quarter()
mydata_q=merge(x=mydata_q,y=SDR_USD,by=c("year","quarter"),all.x=T)

Crisis_Data=list(metadata=list(`Cohen & Valadier`=CV_metadt,
                               `Reinhart & Rogoff`=RR_metadt,
                               `Laeven & Valencia`=LV_metadt,
                               `Cruces & Trebesch`=CT_metadt,
                               `Beers & Mavalwalla`=BM_metadt,
                               `ISDA`=ISDA_metadt,
                               `Dreher IMF`=IMF_intervention_metadt,
                               `Betin IMF programs`=IMF_programs_metadt),
                 data=mydata,
                 sources=source_data)

rio::export(mydata_q,"Data/Data on crisis/Data_crisis_q.RData")

rm(list=setdiff(ls(), "Crisis_Data"))
save(Crisis_Data, file = "Data/Data on crisis/Data_crisis.RData")

