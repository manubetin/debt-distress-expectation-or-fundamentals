#README: 

describe my list of functions

## Install/load packages

### install.my.packages()
 parameters: pkg a vector of string names for the package to install
 
### load.my.packages()

parameters: pkg a vector of string names for the package to load

## Cycles 

### Package_cycle.r (include several functions)
 
####  panel_hp_filter(mydata,var,lambda,smooth_cycle)
function that decompose the cyclical component of a serie using hp filter and giving the option to smooth the cyclical component. 

dependencies: dplyr, hpfilter

parameters:
  - mydata: a dataframe containing at least the following columns:
              "ISO3_Code" as character that gives the country code
              "Period" as Date format %Y-%m-%d at quarterly frequency
              "var" the numeric variable specify in the function
  - var: the variable on which you want to perform the filter
the algorithm will skip any country with less than 10 observations and with missing values

output:
  -the original dataframe with the trend, cycle and smooth cycle of the serie specified as var


#### Expansions_Contractions_4=function(mydata,var,mincycle=12,minphase=2)

function that apply the BBQ algorithm on a panel data and returns a dataframe with the turning points of the serie The index and duration of the cycles and dummy variables for expansion and recessions as well as for the first and second half of the expansion the recession

dependencies: dplyr, BCDating, function Locate_Peaks_Troughs

parameters are:
-  mydata: a dataframe containing at least the following columns:
              "ISO3_Code" as character that gives the country code
              "Period" as Date format %Y-%m-%d at quarterly frequency
              "var" the numeric variable specify in the function
-  var: the variable on which you want to perform the analysis
-  mincycle: the minimum length of a cycle
-  minphase: the minimum length of each phase

output:
   the original dataframe plus the following variables, Peaks, Troughs, Phase A1, Phase A2, Phase B1, Phase B2, Phase A,Phase B,Index_cycle, lenght of each phase,  Phase A1_t, Phase A2_t, Phase B1_t, Phase B2_t, Phase A_t,Phase B_t that are counters for each additional quarter since the respective peak/Trough/A1_to_A2/A2_to_B1

#### Expansions_Contractions_2=function(mydata,var,mincycle=12,minphase=2)

function that apply the BBQ algorithm on a panel data and returns a dataframe with the turning points of the serie The index and duration of the cycles and dummy variables for expansion and recessions
dependencies: dplyr, BCDating, function Locate_Peaks_Troughs

parameters are:
-  mydata: a dataframe containing at least the following columns:
              "ISO3_Code" as character that gives the country code
              "Period" as Date format %Y-%m-%d at quarterly frequency
              "var" the numeric variable specify in the function
-  var: the variable on which you want to perform the analysis
-  mincycle: the minimum length of a cycle
-  minphase: the minimum length of each phase

output:
  the original dataframe, Peaks, Troughs, Phase A,Phase B,Index_cycle, lenght of each phase, Phase A_t,Phase B_t that are counters for each additional quarter since the respective peak/Trough

#### cycle_stats(mydata,var)

Generate statistics for each cycle (Dates,duration, amplitude, slope)
 
parameters: 
 -mydata: data frame containing the variables: ISO3_Code,Period,var and the set of variables computed with Expansion_Contraction()
 - var: the variable on which the cycles have been computed and on which you want perform the analysis

dependency: Expansion_Contraction() output, dplyr

output:
dataframe with the following variables: ISO3_Code, Index_cycle, Period, Period_all, var_Trough_1, var_Peaks, var_Trough_2, duration.A,duration.B, duration.C,amplitude.A,amplitude.B,amplitude.C, quarter.amplitude.A,quarter.amplitude.B,quarter.amplitude.C
                    
Function to run after Expansion_Contractions()

#### Plot.Cycles(mydata,var,ctry,compare_var)

generate a set of figures characterizing the cycles and calling the following functions

parameters: 
-mydata: dataframe with at least the following variables: ISO3_Code,Period and variables from Expansion_recessions
-var: string corresponding to the variable on which the cycles are computed
-ctry: string corresponding the ISO3 code of the country to plot
-compare_var: string corresponding the name of variable to plot above the cycles on the variable var

dependencies: ggplot, dplyr, fun.Cycle(), fun.Turning.Points(), fun.Phase.AB(), fun.Phase.A1.A2.B1.B2(),
fun.Phase.AB.Var()
output:
list containing 5 different plots:
-"cycle": plot the serie underlying the cycles
-"Turning points" plot the serie underlying the cycles and the Peaks and troughs
-"Phase A.B" plot the serie underlying the cycles, the Peaks and troughs and Phase A and B
-"Phase.A1.A2.B1.B2" plot the serie underlying the cycles, the Peaks and troughs and Phase A1, A2 and B1, B2
-"Compare var in Cycle": plot phase A and Phase B and the compare var to observe its behavior across the cycle

#### fun.Cycle(mydata,var,country)

#### fun.Turning.Points(mydata,var,country)

#### fun.Phase.AB(mydata,var,country)

#### fun.Phase.A1.A2.B1.B2(mydata,var,country)

#### fun.Phase.AB.Var(mydata,var,country,compare_var=NULL)


#### cycles=function(mydata_q,var)

Create cycles for the selected var

Dependency: Expansion_contraction_4

#### CoIncidence_Index=function(list_var_cycles,var_1,var_2)

Compute the coincidence between cycles (Phase A and Phase B) of the two selected variables. 
first run cycles for the two variables and store results in a list that is then pass as first parameters 
of this function

Dependency: Expansion_contraction_4, cycles

#### get_h(mydata,var==NULL) & get_h2(mydata,var==NULL) 

Create variable associating each period in reference to the peak (dummy) selected. 

#### Plot_var_around_Peak=function(mydata_q,myvar,window=12)

Generate plot showing the average behavior around the peak for a preselected window

Dependency: get_h2

#### default_table_year_month=function(mydata_q,var)

Summarize dates for which an event happened. var has to be a dummy variables with 1 denoting the occurence of an event and 0 otherwise.

#### aggregate_defaults=function(mydata,vars)

Similar to default_table_year_month but with multiple variables. Summarize dates for which an event happened. vars  is a list of variables for which we want to perform the dating and aggregate the results 

Dependencies: default_table_year_month

#### Plot_list_var_around_Peak=function(mydata_q,myvars,window=12)

Same as *Plot_var_around_Peak* but allow to pass list of variables to compare their behavior around the peak

Dependency: get_h2
## Countries name and characteristics

### list_countries(sample="All")
generate a vector with iso3c of countries 
sample either "All" or "OECD"

### ctry_classif()

Gives the world bank classification of countries

### mydata_init(countries,start,end,frequency)

generate complete panel of ISO3_Code and Periods at 

parameters:
 countries: vector of ISO3_Codes 
 start: integer for starting year
 end: integer for ending year
 frequency:"quarter", "year"

output: panel with ISO3_Code and Period 

## Descriptive stats

### bar_chart_dispo_period(mydata,var,y_axis="N",name.file=NA)
generate barchart with the number of countries in the panel for which the variable has available data for each year in the sample

parameters: 
-mydata: dataframe with a last ISO3_Code, Period, var
-var: the name of the variable you want to see the availability:
-y_axis: "N" (the number of countries) or "perc" (percentage of countries)
-name.file; location where to save the plot.

dependency: gglpot, dplyr

### removeOutliers()

## Data generating process

### markovchains(trans,Periods)
generate markov process for given transition probability matrix

parameters: 
-trans: transition probability matrix
-Periods; number of periods to generates

## Regressions

### reg_by_phase()

### plot_reg_coefs()

### my_regs()

