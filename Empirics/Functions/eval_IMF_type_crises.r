eval_IMF_type_crises=function(mydata_q,type_IMF_program,t.liquid.crisis=0.1,t.solv.crisis=0.9){
  
  mydata_q=mydata_q %>% mutate(IMF.pure.liq.crisis=ifelse(IMF_All_diff<=t.liquid.crisis,1,0),
                               IMF.Non.pure.liq.crisis=ifelse(IMF_All_diff>t.liquid.crisis,1,0),
                               IMF.pure.solv.crisis=ifelse(IMF_All_diff>=t.solv.crisis,1,0),
                               IMF.Non.pure.solv.crisis=ifelse(IMF_All_diff<t.solv.crisis,1,0),
                               IMF.liq.crisis=ifelse(IMF_All_diff<=0.5,1,0),
                               IMF.solv.crisis=ifelse(IMF_All_diff>0.5,1,0))

  a=mydata_q %>%
    group_by(ISO3_Code) %>% arrange(ISO3_Code,Period) %>%
    mutate(
      g.L1=lead(g.R.GDPV,0),
      g.L2=lead(g.R.GDPV,1),
      g.L3=lead(g.R.GDPV,2),
      g.L4=lead(g.R.GDPV,3),
      B.d=Phase_B+lead(Phase_B,1)+lead(Phase_B,2)+lead(Phase_B,3)+lead(Phase_B,4)+lead(Phase_B,5)+lead(Phase_B,6)+lead(Phase_B,7),
      B.large.d=large_B+lead(large_B,1)+lead(large_B,2)+lead(large_B,3)+large_B+lead(large_B,4)+lead(large_B,5)+lead(large_B,6)+lead(large_B,7),
      L1=lead(Gov.Gross.debt.HPDD,0),
      L2=lead(Gov.Gross.debt.HPDD,4),
      L3=lead(Gov.Gross.debt.HPDD,8),
      L4=lead(Gov.Gross.debt.HPDD,12),
      SD=SD.debt.Tot.BM+lead(SD.debt.Tot.BM,1)+lead(SD.debt.Tot.BM,2)+lead(SD.debt.Tot.BM,3)+lead(SD.debt.Tot.BM,4),
      SD.d=SD.BM+lead(SD.BM,1)+lead(SD.BM,2)+lead(SD.BM,3)+lead(SD.BM,4)+lead(SD.BM,5)+lead(SD.BM,6)+lead(SD.BM,7),#+lead(SD.BM,12),
      #SD.d=SD.RR_first,lead(SD.RR_first,1)+lead(SD.RR_first,2)+lead(SD.RR_first,4)+lead(SD.RR_first,5)+lead(SD.RR_first,6)+lead(SD.RR_first,7),#+lag(SD.BM,12),
      #SD.d=SD_E.RR,lead(SD_E.RR,1)+lead(SD_E.RR,2)+lead(SD_E.RR,4)+lead(SD_E.RR,5)+lead(SD_E.RR,6)+lead(SD_E.RR,7),#+lag(SD.BM,12), 
      CC.L1=lead(d.XR,0),
      CC.L2=lead(d.XR,1),
      CC.L3=lead(d.XR,2),
      CC.L4=lead(d.XR,3),
      CC.L5=lead(d.XR,4),
      CC.L6=lead(d.XR,5),
      CC.L7=lead(d.XR,6),
      CC.L8=lead(d.XR,7),
      CC.L9=lead(d.XR,8),
      CC.d=CC.MB+lead(CC.MB,1)+lead(CC.MB,2)+lead(CC.MB,3)+lead(CC.MB,4)+lead(CC.MB,5)+lead(CC.MB,6)+lead(CC.MB,7),
      BC.L1=lead(BC_FiscalCost_percGDP.LV,0),
      BC.L2=lead(BC_FiscalCost_percGDP.LV,4),
      BC.L3=lead(BC_FiscalCost_percGDP.LV,8),
      BC.L4=lead(BC_FiscalCost_percGDP.LV,12),
      BC.d=BC.LV+lead(BC.LV,4)+lead(BC.LV,8),#+lead(BC.LV,9),
      SMC.L1=lead(d.Stock.market.index,0),
      SMC.L2=lead(d.Stock.market.index,1),
      SMC.L3=lead(d.Stock.market.index,2),
      SMC.L4=lead(d.Stock.market.index,3),
      SMC.L5=lead(d.Stock.market.index,4),
      SMC.L6=lead(d.Stock.market.index,5),
      SMC.L7=lead(d.Stock.market.index,6),
      SMC.L8=lead(d.Stock.market.index,7),
      SMC.L9=lead(d.Stock.market.index,8),
      SMC.d=SMC.MB+lead(SMC.MB,1)+lead(SMC.MB,2)+lead(SMC.MB,3),
      FR.L1=lead(d.RAFA_USD,0),
      FR.L2=lead(d.RAFA_USD,1),
      FR.L3=lead(d.RAFA_USD,2),
      FR.L4=lead(d.RAFA_USD,3),
      FR.L5=lead(d.RAFA_USD,4),
      FR.L6=lead(d.RAFA_USD,5),
      FR.L7=lead(d.RAFA_USD,6),
      FR.L8=lead(d.RAFA_USD,7),
      FR.L9=lead(d.RAFA_USD,8)
    ) %>% rowwise() %>%
    mutate(gdebt_max=pmax(pmax(L1,L2),L3),
           d.gdebt=(log(gdebt_max)-log(Gov.Gross.debt.HPDD))*100,
           B.d=ifelse(B.d>0,1,0),
           B.large.d=ifelse(B.large.d>0,1,0),
           SD.d=ifelse(SD.d>0,1,0),
           CC.d=ifelse(CC.d>0,1,0),
           BC.d=ifelse(BC.d>0,1,0),
           SMC.d=ifelse(SMC.d>0,1,0),
           g.R=mean(c(g.L1,g.L2,g.L3,g.L4),na.rm=T)*100,
           CC=mean(c(CC.L1,CC.L2,CC.L3,CC.L4,CC.L5,CC.L6,CC.L7,CC.L8),na.rm=T)*100,
           BC=mean(c(BC.L1,BC.L2,BC.L3),na.rm=T),
           SMC=mean(c(SMC.L1,SMC.L2,SMC.L3,SMC.L4),na.rm=T)*100,
           FR=mean(c(FR.L1,FR.L2,FR.L3,FR.L4),na.rm=T)*100) %>%
    group_by(ISO3_Code,year) %>% mutate(BC=mean(BC,na.rm=T)) %>%
    filter(base::get(type_IMF_program)==1) %>% #SD_0_4yBefAft.BM==1 &
    dplyr::select(ISO3_Code,Period,
                  gdebt=Gov.Gross.debt.HPDD,
                  gdebt_max,
                  d.gdebt,
                  B.d,
                  B.large.d,
                  g.R,
                  SD,
                  SD.d,
                  CC,
                  CC.d,
                  BC,
                  BC.d,
                  SMC,
                  SMC.d,
                  FR) 
  
  dt=data.frame(a) %>%
    summarize(B.d=mean(B.d,na.rm = T),
              B.large.d=mean(B.large.d,na.rm=T),
              g.R=mean(g.R,na.rm=T),
              gdebt=mean(gdebt,na.rm=T),
              d.gdebt=mean(d.gdebt,na.rm=T),
              SD.d=mean(SD.d,na.rm=T),
              SD=mean(SD,na.rm=T),
              CC.d=mean(CC.d,na.rm=T),
              CC=mean(CC,na.rm = T),
              BC.d=mean(BC.d,na.rm=T),
              BC=mean(BC,na.rm=T),
              SMC.d=mean(SMC.d,na.rm=T),
              SMC=mean(SMC,na.rm=T),
              FR=mean(FR,na.rm=T),
              n.obs=as.integer(n())) %>% round(.,2)
  rownames(dt)=type_IMF_program
  return(list(summary=dt,data=a))
}

eval_IMF_type_crises_bef=function(mydata_q,type_IMF_program,t.liquid.crisis=0.1,t.solv.crisis=0.9){
  
  mydata_q=mydata_q %>% mutate(IMF.pure.liq.crisis=ifelse(IMF_All_diff<=t.liquid.crisis,1,0),
                               IMF.Non.pure.liq.crisis=ifelse(IMF_All_diff>t.liquid.crisis,1,0),
                               IMF.pure.solv.crisis=ifelse(IMF_All_diff>=t.solv.crisis,1,0),
                               IMF.Non.pure.solv.crisis=ifelse(IMF_All_diff<t.solv.crisis,1,0),
                               IMF.liq.crisis=ifelse(IMF_All_diff<=0.5,1,0),
                               IMF.solv.crisis=ifelse(IMF_All_diff>0.5,1,0))
  
  a=mydata_q %>%
    group_by(ISO3_Code) %>% arrange(ISO3_Code,Period) %>%
    mutate(
      g.L1=lag(g.R.GDPV,0),
      g.L2=lag(g.R.GDPV,1),
      g.L3=lag(g.R.GDPV,2),
      g.L4=lag(g.R.GDPV,3),
      B.d=Phase_B+lag(Phase_B,1)+lag(Phase_B,2),#,+lag(Phase_B,3),
      B.large.d=large_B+lag(large_B,1)+lag(large_B,2),#+lag(large_B,3),
      L1=lag(Gov.Gross.debt.HPDD,0),
      L2=lag(Gov.Gross.debt.HPDD,4),
      L3=lag(Gov.Gross.debt.HPDD,8),
      L4=lag(Gov.Gross.debt.HPDD,12),
      SD=SD.debt.Tot.BM+lag(SD.debt.Tot.BM,1)+lag(SD.debt.Tot.BM,2)+lag(SD.debt.Tot.BM,3)+lag(SD.debt.Tot.BM,4),
      SD.d=SD.BM+lag(SD.BM,1)+lag(SD.BM,2)+lag(SD.BM,3)+lag(SD.BM,4)+lag(SD.BM,5)+lag(SD.BM,6)+lag(SD.BM,7),#+lead(SD.BM,12),
      #SD.d=SD.RR_first,lag(SD.RR_first,1)+lag(SD.RR_first,2)+lag(SD.RR_first,4)+lag(SD.RR_first,5)+lag(SD.RR_first,6)+lag(SD.RR_first,7),#+lag(SD.BM,12),
      #SD.d=SD_E.RR,lag(SD_E.RR,1)+lag(SD_E.RR,2)+lag(SD_E.RR,4)+lag(SD_E.RR,5)+lag(SD_E.RR,6)+lag(SD_E.RR,7),#+lag(SD.BM,12),
      CC.L1=lag(d.XR,0),
      CC.L2=lag(d.XR,1),
      CC.L3=lag(d.XR,2),
      CC.L4=lag(d.XR,3),
      CC.L5=lag(d.XR,4),
      CC.L6=lag(d.XR,5),
      CC.L7=lag(d.XR,6),
      CC.L8=lag(d.XR,7),
      CC.L9=lag(d.XR,8),
      CC.d=CC.MB+lag(CC.MB,1)+lag(CC.MB,2)+lag(CC.MB,3)+lag(CC.MB,4)+lag(CC.MB,5)+lag(CC.MB,6)+lag(CC.MB,7),
      BC.L1=lag(BC_FiscalCost_percGDP.LV,0),
      BC.L2=lag(BC_FiscalCost_percGDP.LV,4),
      BC.L3=lag(BC_FiscalCost_percGDP.LV,8),
      BC.L4=lag(BC_FiscalCost_percGDP.LV,12),
      BC.d=BC.LV+lag(BC.LV,4)+lag(BC.LV,8),#+lag(BC.LV,3),
      SMC.L1=lag(d.Stock.market.index,0),
      SMC.L2=lag(d.Stock.market.index,1),
      SMC.L3=lag(d.Stock.market.index,2),
      SMC.L4=lag(d.Stock.market.index,3),
      SMC.L5=lag(d.Stock.market.index,4),
      SMC.L6=lag(d.Stock.market.index,5),
      SMC.L7=lag(d.Stock.market.index,6),
      SMC.L8=lag(d.Stock.market.index,7),
      SMC.L9=lag(d.Stock.market.index,8),
      SMC.d=SMC.MB+lag(SMC.MB,1)+lag(SMC.MB,2)+lag(SMC.MB,3),
      FR.L1=lag(d.RAFA_USD,0),
      FR.L2=lag(d.RAFA_USD,1),
      FR.L3=lag(d.RAFA_USD,2),
      FR.L4=lag(d.RAFA_USD,3),
      FR.L5=lag(d.RAFA_USD,4),
      FR.L6=lag(d.RAFA_USD,5),
      FR.L7=lag(d.RAFA_USD,6),
      FR.L8=lag(d.RAFA_USD,7),
      FR.L9=lag(d.RAFA_USD,8)
    ) %>% rowwise() %>%
    mutate(gdebt_max=pmax(pmax(L1,L2),L3),
           d.gdebt=(log(gdebt_max)-log(Gov.Gross.debt.HPDD))*100,
           SD.d=ifelse(SD.d>0,1,0),
           B.large.d=ifelse(B.large.d>0,1,0),
           B.d=ifelse(B.d>0,1,0),
           CC.d=ifelse(CC.d>0,1,0),
           CC=mean(c(CC.L1,CC.L2,CC.L3,CC.L4,CC.L5,CC.L6,CC.L7,CC.L8),na.rm=T)*100,
           BC=mean(c(BC.L1,BC.L2,BC.L3),na.rm=T),
           g.R=mean(c(g.L1,g.L2,g.L3,g.L4),na.rm=T)*100,
           BC.d=ifelse(BC.d>0,1,0),
           SMC=mean(c(SMC.L1,SMC.L2,SMC.L3,SMC.L4),na.rm=T)*100,
           SMC.d=ifelse(SMC.d>0,1,0),
           FR=mean(c(FR.L1,FR.L2,FR.L3,FR.L4),na.rm=T)*100) %>%
    group_by(ISO3_Code,year) %>% mutate(BC=mean(BC,na.rm=T)) %>%
    filter(get(type_IMF_program)==1) %>% #SD_0_4yBefAft.BM==1 &
    dplyr::select(ISO3_Code,Period,
                  gdebt=Gov.Gross.debt.HPDD,
                  gdebt_max,
                  d.gdebt,
                  g.R,
                  B.d,
                  B.large.d,
                  SD,
                  SD.d,
                  CC,
                  CC.d,
                  BC,
                  BC.d,
                  SMC,
                  SMC.d,
                  FR) 
  
  dt=data.frame(a) %>%
    summarize(B.d=mean(B.d,na.rm = T),
              B.large.d=mean(B.large.d,na.rm=T),
              g.R=mean(g.R,na.rm=T),
              gdebt=mean(gdebt,na.rm=T),
              d.gdebt=mean(d.gdebt,na.rm=T),
              SD.d=mean(SD.d,na.rm=T),
              SD=mean(SD,na.rm=T),
              CC.d=mean(CC.d,na.rm=T),
              CC=mean(CC,na.rm = T),
              BC.d=mean(BC.d,na.rm=T),
              BC=mean(BC,na.rm=T),
              SMC.d=mean(SMC.d,na.rm=T),
              SMC=mean(SMC,na.rm=T),
              FR=mean(FR,na.rm=T),
              n.obs=as.integer(n())) %>% round(.,2) 
  rownames(dt)=type_IMF_program
  return(list(summary=dt,data=a))
}

plot.distrib.IMF_type_crisies=function(var,t.liquid.crisis=0.1,t.solv.crisis=0.9){
  
  IMF.pure.solv.data=eval_IMF_type_crises(mydata_q,"IMF.pure.solv.crisis",
                                          t.liquid.crisis=t.liquid.crisis,
                                          t.solv.crisis=t.solv.crisis)
  IMF.pure.liq.data=eval_IMF_type_crises(mydata_q,"IMF.pure.liq.crisis",
                                         t.liquid.crisis=t.liquid.crisis,
                                         t.solv.crisis=t.solv.crisis)
  
  mean.solv=data.frame(IMF.pure.solv.data$data) %>% summarize(variable=mean(get(var),na.rm=T))
  mean.solv=mean.solv$variable
  mean.liq=data.frame(IMF.pure.liq.data$data) %>% summarize(variable=mean(get(var),na.rm=T))
  mean.liq=mean.liq$variable
  
  ggplot(IMF.pure.solv.data$data,aes(x=get(var)))+
    geom_density(aes(fill="Pure solvency programs",color="Pure solvency programs"),alpha=0.5)+
    geom_density(data=IMF.pure.liq.data$data,aes(fill="Pure liquidity programs",color="Pure liquidity programs"),alpha=0.3)+
    geom_vline(xintercept = mean.solv,color="blue",linetype="dashed")+
    geom_vline(xintercept = mean.liq,color="red",linetype="dashed")+
    labs(x=var,y=NULL)+
    scale_fill_manual(name="",values=c("red","blue"))+
    scale_color_manual(guide=F,values=c("red","blue"))+
    theme_bw()+
    theme(legend.position="bottom")
}

plot.distrib.IMF_type_crisies_bef=function(var,t.liquid.crisis=0.1,t.solv.crisis=0.9){
  
  IMF.pure.solv.data=eval_IMF_type_crises_bef(mydata_q,"IMF.pure.solv.crisis",
                                              t.liquid.crisis=t.liquid.crisis,
                                              t.solv.crisis=t.solv.crisis)
  IMF.pure.liq.data=eval_IMF_type_crises_bef(mydata_q,"IMF.pure.liq.crisis",
                                             t.liquid.crisis=t.liquid.crisis,
                                             t.solv.crisis=t.solv.crisis)
  
  mean.solv=data.frame(IMF.pure.solv.data$data) %>% summarize(variable=mean(get(var),na.rm=T))
  mean.solv=mean.solv$variable
  mean.liq=data.frame(IMF.pure.liq.data$data) %>% summarize(variable=mean(get(var),na.rm=T))
  mean.liq=mean.liq$variable
  
  ggplot(IMF.pure.solv.data$data,aes(x=get(var)))+
    geom_density(aes(fill="Pure solvency programs",color="Pure solvency programs"),alpha=0.5)+
    geom_density(data=IMF.pure.liq.data$data,aes(fill="Pure liquidity programs",color="Pure liquidity programs"),alpha=0.3)+
    geom_vline(xintercept = mean.solv,color="blue",linetype="dashed")+
    geom_vline(xintercept = mean.liq,color="red",linetype="dashed")+
    labs(x=var,y=NULL)+
    scale_fill_manual(name="",values=c("red","blue"))+
    scale_color_manual(guide=F,values=c("red","blue"))+
    theme_bw()+
    theme(legend.position="bottom")
}
