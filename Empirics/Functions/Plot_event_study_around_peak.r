Plot_var_around_Peak=function(mydata_q,myvar,window=12){
  
  dt=mydata_q %>% arrange(ISO3_Code,Period)
  dt=get_h(dt,"GDPV_XDC_cycle")
  compare_var= mydata_q %>% dplyr::select(ISO3_Code,Period,myvar)
  dt=merge(x=dt,y=compare_var,by=c("ISO3_Code","Period"))
  dt=dt %>% na.omit() %>% rename(time_lag=value)
  
  dt_collapsed=dt %>% group_by(time_lag) %>% summarize(var=mean(eval(parse(text=myvar))))
  dt_collapsed=dt_collapsed %>% filter(time_lag<=window & time_lag >= - window)
  
  ggplot(data=dt_collapsed,aes(x=time_lag,y=var))+
    geom_point()+
    geom_vline(xintercept = 0)+
    geom_smooth(method='loess')+
    xlab("Distance to Peak")+
    ylab(myvar)+
    # ylim(c(0,700))+
    theme_bw(base_size = 15)
}

Plot_list_var_around_Peak=function(mydata_q,myvars,window=12){
  
  #myvars=c("X1Y_cds","X2Y_cds","X3Y_cds","X10Y_cds")
  dt=mydata_q %>% arrange(ISO3_Code,Period)
  dt=get_h(dt,"GDPV_XDC_cycle")
  compare_var= mydata_q %>% dplyr::select(ISO3_Code,Period,myvars)
  dt=merge(x=dt,y=compare_var,by=c("ISO3_Code","Period"))
  dt=dt %>% na.omit() %>% rename(time_lag=value)
  
  dt_collapsed=dt %>% group_by(time_lag) %>% summarize(var1=mean(eval(parse(text=myvars[1]))),
                                                       var2=mean(eval(parse(text=myvars[2]))),
                                                       var3=mean(eval(parse(text=myvars[3]))),
                                                       var4=mean(eval(parse(text=myvars[4]))))
  
  dt_collapsed=dt_collapsed %>% filter(time_lag<=window & time_lag >= - window)
  
  ggplot(data=dt_collapsed)+
    geom_vline(xintercept = 0)+
    #geom_point(aes(x=time_lag,y=var1))+
    geom_smooth(aes(x=time_lag,y=var1,color=myvars[1]),method='loess',fill="white",alpha=0)+
    # geom_point(aes(x=time_lag,y=var2))+
    geom_smooth(aes(x=time_lag,y=var2,color=myvars[2]),method='loess',fill="white",alpha=0)+
    geom_smooth(aes(x=time_lag,y=var3,color=myvars[3]),method='loess',fill="white",alpha=0)+
    geom_smooth(aes(x=time_lag,y=var4,color=myvars[4]),method='loess',fill="white",alpha=0)+
    scale_color_manual(name="",values=c("red","brown","green","blue"))+
    xlab("Distance to Peak")+
    ylab(NULL)+
    theme_bw(base_size = 15)+
    theme(legend.position = "bottom")
}
