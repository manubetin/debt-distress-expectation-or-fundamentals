

mydata=mydata %>% 
  group_by(ISO3_Code) %>%
  arrange(ISO3_Code,Period) %>%
  mutate(NGDP_XDC_Y=NGDP_XDC_Y*1000000,
         g.NGDP_XDC_y=log(NGDP_XDC_Y)-lag(log(NGDP_XDC_Y),4))

mydata = mydata %>% mutate(PCPI_IX=as.numeric(PCPI_IX),
                           infl=log(PCPI_IX)-lag(log(PCPI_IX),1),
                           g.R.NGDP_XDC_y=g.NGDP_XDC_y-infl,
                           g.R.GDPV=log(GDP_original)-lag(log(GDP_original),4))

# transform IMF programs in dollars ####
mydata=mydata %>% mutate(IMFEFF_Amount_agreed=IMFEFF_Amount_agreed*SDR_USD*1000,
                             IMFSBA_Amount_agreed=IMFSBA_Amount_agreed*SDR_USD*1000,
                             IMFECF_Amount_agreed=IMFECF_Amount_agreed*SDR_USD*1000,
                             IMFPLL_Amount_agreed=IMFPLL_Amount_agreed*SDR_USD*1000,
                             IMFSAF_Amount_agreed=IMFSAF_Amount_agreed*SDR_USD*1000,
                             IMFESF_Amount_agreed=IMFESF_Amount_agreed*SDR_USD*1000,
                             IMFFCL_Amount_agreed=IMFFCL_Amount_agreed*SDR_USD*1000,
                             IMFSCF_Amount_agreed=IMFSCF_Amount_agreed*SDR_USD*1000,
                             
                             IMFEFF_Amount_drawn=IMFEFF_Amount_drawn*SDR_USD*1000,
                             IMFSBA_Amount_drawn=IMFSBA_Amount_drawn*SDR_USD*1000,
                             IMFECF_Amount_drawn=IMFECF_Amount_drawn*SDR_USD*1000,
                             IMFPLL_Amount_drawn=IMFPLL_Amount_drawn*SDR_USD*1000,
                             IMFSAF_Amount_drawn=IMFSAF_Amount_drawn*SDR_USD*1000,
                             IMFESF_Amount_drawn=IMFESF_Amount_drawn*SDR_USD*1000,
                             IMFFCL_Amount_drawn=IMFFCL_Amount_drawn*SDR_USD*1000,
                             IMFSCF_Amount_drawn=IMFSCF_Amount_drawn*SDR_USD*1000)


# create relevant variables for IMF programs ####
mydata=mydata %>% mutate(IMF_ALL_agreed=IMFEFF_Amount_agreed+IMFSBA_Amount_agreed+IMFECF_Amount_agreed+
                         IMFPLL_Amount_agreed+IMFPLL_Amount_agreed+IMFSAF_Amount_agreed+
                         IMFESF_Amount_agreed+IMFFCL_Amount_agreed+IMFSCF_Amount_agreed,
                             
                         IMF_ALL_drawn=IMFEFF_Amount_drawn+IMFSBA_Amount_drawn+IMFECF_Amount_drawn+
                         IMFPLL_Amount_drawn+IMFPLL_Amount_drawn+IMFSAF_Amount_drawn+
                         IMFESF_Amount_drawn+IMFFCL_Amount_drawn+IMFSCF_Amount_drawn,
                             
                         IMF_All_diff=1-(IMF_ALL_agreed-IMF_ALL_drawn)/IMF_ALL_agreed,
                         IMF_pure_Expect_crisis=ifelse(IMF_All_diff<=0.1,1,0),
                         IMF_Non_pure_Expect_crisis=ifelse(IMF_All_diff>0.1,1,0),
                         IMF_Expect_crisis=ifelse(IMF_All_diff<=0.5,1,0),
                         IMF_pure_liquid_crisis=ifelse(IMF_All_diff>=0.9,1,0),
                         IMF_liquid_crisis=ifelse(IMF_All_diff>0.5,1,0)
)

# express IMF programs and defaults in terms of GDP ####
mydata=mydata %>% 
  group_by(ISO3_Code) %>%
  arrange(ISO3_Code,Period) %>%
  mutate(
         IMF_ALL_agreed_GDP=ifelse(!is.na(NGDP_XDC_Y),((IMF_ALL_agreed*XR_rate_BIS)/NGDP_XDC_Y)*100,NA),
         IMF_ALL_drawn_GDP=ifelse(!is.na(NGDP_XDC_Y),((IMF_ALL_drawn*XR_rate_BIS)/NGDP_XDC_Y)*100,NA),
         SD.debt.Tot.BM_GDP=(SD.debt.Tot.BM*1000*XR_rate_BIS/NGDP_XDC_Y)*100,
         SD.debt.IMF.BM_GDP=(SD.debt.IMF.BM*1000*XR_rate_BIS/NGDP_XDC_Y)*100,
         SD.debt.OthersOffCred.BM_GDP=(SD.debt.OthersOffCred.BM*1000*XR_rate_BIS/NGDP_XDC_Y)*100,
         SD.debt.IBRD.BM_GDP=(SD.debt.IBRD.BM*1000*XR_rate_BIS/NGDP_XDC_Y)*100,
         SD.debt.PrivateCred.BM_GDP=(SD.debt.PrivateCred.BM*1000*XR_rate_BIS/NGDP_XDC_Y)*100,
         SD.debt.ParisClub.BM_GDP=(SD.debt.ParisClub.BM*1000*XR_rate_BIS/NGDP_XDC_Y)*100,
         SD.debt.FCBonds.BM_GDP=(SD.debt.FCBonds.BM*1000*XR_rate_BIS/NGDP_XDC_Y)*100,
         SD.debt.FCBankloans.BM_GDP=(SD.debt.FCBankloans.BM*1000*XR_rate_BIS/NGDP_XDC_Y)*100,
         SD.debt.LCdebt.BM_GDP=(SD.debt.LCdebt.BM*1000*XR_rate_BIS/NGDP_XDC_Y)*100)

# express variable for exchange rate ####
mydata=mydata %>% 
  group_by(ISO3_Code) %>%
  arrange(ISO3_Code,Period) %>%
  mutate(d.XR=log(XR_rate_BIS)-log(lag(XR_rate_BIS,1)),
         CC.MB=ifelse(d.XR>0.2,1,0),
         CC.MB_first=ifelse(CC.MB==1 & lag(CC.MB,1)==0,1,0)
         )


# express variables for stock market ####

mydata=mydata %>% 
  group_by(ISO3_Code) %>%
  arrange(ISO3_Code,Period) %>%
  mutate(Stock.market.index=ifelse(!is.na(PEQ),PEQ,as.numeric(FPE_EOP_IX)),
         d.Stock.market.index=log(Stock.market.index)-log(lag(Stock.market.index,1))
         )
         
#cycles of Stocke markets
dt=data.frame(mydata) %>% dplyr::select(ISO3_Code,Period,Stock.market.index)
dt=Expansions_Contractions_4(dt,"Stock.market.index",mincycle=8,minphase = 3)
#fun.Phase.AB(dt,"Stock.market.index","MEX")
dt=dt %>% setNames(paste0("Stock.market.index_",names(.))) %>% rename(ISO3_Code=Stock.market.index_ISO3_Code,
                                                                    Period=Stock.market.index_Period)
mydata=merge(x=mydata,y=dt,by=c("ISO3_Code","Period"),all=T)
mydata=mydata %>% 
  group_by(ISO3_Code) %>% 
  arrange(ISO3_Code,Period) %>% 
  mutate(SMC.MB=ifelse(lag(Stock.market.index_Peaks)==1,1,0))



# Express foreign exchange reserves ####


mydata=mydata %>% mutate(RAFA_USD=as.numeric(RAFA_USD))

dt=data.frame(mydata) %>% dplyr::select(ISO3_Code,Period,RAFA_USD)

dt=Expansions_Contractions_4(dt,"RAFA_USD",mincycle=8,minphase = 3)
fun.Phase.AB(dt,"RAFA_USD","MEX")
dt=dt %>% setNames(paste0("RAFA_USD_",names(.))) %>% rename(ISO3_Code=RAFA_USD_ISO3_Code,
                                                            Period=RAFA_USD_Period)
mydata=merge(x=mydata,y=dt,by=c("ISO3_Code","Period"),all=T)

mydata=mydata %>% group_by(ISO3_Code) %>% arrange(ISO3_Code,Period) %>%
  mutate(FRC.MB=ifelse(RAFA_USD_Peaks==1,1,0),
         d.RAFA_USD=log(RAFA_USD)-log(lag(RAFA_USD,1)))

#express phases of cycles into a single catergorical variable

mydata= mydata %>% 
  mutate(Phase_4=ifelse(Phase_A1==1,"A1",ifelse(Phase_A2==1,"A2",ifelse(Phase_B1==1,"B1","B2"))))

# IMF programs and sovereign defaults ####

## IMF programs followed by default
mydata=data.frame(mydata) %>% group_by(ISO3_Code,year) %>%
  mutate(SD_Tot.BM_first_by_qrt=sum(SD_Tot.BM,na.rm=T))

mydata=data.frame(mydata) %>% group_by(ISO3_Code) %>% 
  mutate(SD_0_1yAft.BM=SD_Tot.BM_first_by_qrt+dplyr::lead(SD_Tot.BM_first_by_qrt,4),
         SD_0_2yAft.BM=SD_0_1yAft.BM+lead(SD_0_1yAft.BM,4),
         SD_0_3yAft.BM=SD_0_2yAft.BM+lead(SD_0_2yAft.BM,4),
         SD_0_4yAft.BM=SD_0_3yAft.BM+lead(SD_0_3yAft.BM,4))

mydata=data.frame(mydata) %>% group_by(ISO3_Code) %>% 
  mutate(SD_0y.BM=ifelse(SD_Tot.BM_first_by_qrt>0,1,0),
         SD_0_1yAft.BM=ifelse(SD_0_1yAft.BM>0,1,0),
         SD_0_2yAft.BM=ifelse(SD_0_2yAft.BM>0,1,0),
         SD_0_3yAft.BM=ifelse(SD_0_3yAft.BM>0,1,0),
         SD_0_4yAft.BM=ifelse(SD_0_4yAft.BM>0,1,0))

mydata=data.frame(mydata) %>% group_by(ISO3_Code) %>% 
  mutate(SD_0_1yBefAft.BM=lag(SD_Tot.BM_first_by_qrt,4)+SD_Tot.BM_first_by_qrt+dplyr::lead(SD_Tot.BM_first_by_qrt,4),
         SD_0_2yBefAft.BM=lag(SD_0_1yBefAft.BM,4)+SD_0_1yBefAft.BM+lead(SD_0_1yBefAft.BM,4),
         SD_0_3yBefAft.BM=lag(SD_0_2yBefAft.BM,4)+SD_0_2yBefAft.BM+lead(SD_0_2yBefAft.BM,4),
         SD_0_4yBefAft.BM=lag(SD_0_3yBefAft.BM,4)+SD_0_3yBefAft.BM+lead(SD_0_3yBefAft.BM,4))

mydata=data.frame(mydata) %>% group_by(ISO3_Code) %>% 
  mutate(SD_0_1yBefAft.BM=ifelse(SD_0_1yBefAft.BM>0,1,0),
         SD_0_2yBefAft.BM=ifelse(SD_0_2yBefAft.BM>0,1,0),
         SD_0_3yBefAft.BM=ifelse(SD_0_3yBefAft.BM>0,1,0),
         SD_0_4yBefAft.BM=ifelse(SD_0_4yBefAft.BM>0,1,0))

mydata=data.frame(mydata) %>% group_by(ISO3_Code) %>% 
  mutate(SD_0_1yBef.BM=lag(SD_Tot.BM_first_by_qrt,4)+SD_Tot.BM_first_by_qrt,
         SD_0_2yBef.BM=lag(SD_0_1yBef.BM,4)+SD_0_1yBef.BM,
         SD_0_3yBef.BM=lag(SD_0_2yBef.BM,4)+SD_0_2yBef.BM,
         SD_0_4yBef.BM=lag(SD_0_3yBef.BM,4)+SD_0_3yBef.BM)

mydata=data.frame(mydata) %>% group_by(ISO3_Code) %>% 
  mutate(SD_0_1yBef.BM=ifelse(SD_0_1yBef.BM>0,1,0),
         SD_0_2yBef.BM=ifelse(SD_0_2yBef.BM>0,1,0),
         SD_0_3yBef.BM=ifelse(SD_0_3yBef.BM>0,1,0),
         SD_0_4yBef.BM=ifelse(SD_0_4yBef.BM>0,1,0))

#IMF programs with no default 5 years before and 5 after the program
mydata=mydata %>%
  mutate(IMF_all_no_SD_0_4yBefAft=ifelse(IMF_ALL_agreed>0 & SD_0_4yBefAft.BM==0,1,0),
         IMF_ALL_agreed_dummy=ifelse(IMF_ALL_agreed>0,1,0),
         IMF_ALL_drawn_dummy=ifelse(IMF_ALL_drawn>0,1,0))

#proportion of programs with no defaults
data.frame(mydata) %>% filter(IMF_ALL_agreed>0) %>% summarize(mean(IMF_all_no_SD_0_4yBefAft,na.rm=T))

mydata %>% group_by(Phase_4) %>% summarize(mean=mean(IMF_all_no_SD_0_4yBefAft,na.rm=T),n=n())

#programs with default today, 1 year, 2 years, 3 years, 5 years in the future
mydata=mydata %>%
  mutate(IMF_all_SD_0_1yAft=ifelse(IMF_ALL_agreed>0 & SD_0_1yAft.BM==1,1,0),
         IMF_all_SD_0_2yAft=ifelse(IMF_ALL_agreed>0 & SD_0_2yAft.BM==1,1,0),
         IMF_all_SD_0_3yAft=ifelse(IMF_ALL_agreed>0 & SD_0_3yAft.BM==1,1,0),
         IMF_all_SD_0_4yAft=ifelse(IMF_ALL_agreed>0 & SD_0_4yAft.BM==1,1,0))

data.frame(mydata) %>%
  filter(IMF_ALL_agreed>0)%>%
  summarize(mean(IMF_all_SD_0_1yAft,na.rm=T),
            mean(IMF_all_SD_0_2yAft,na.rm=T),
            mean(IMF_all_SD_0_3yAft,na.rm=T),
            mean(IMF_all_SD_0_4yAft,na.rm=T))

#programs with default today, 1 year, 2 years,3 years or 4 years before
mydata=mydata %>%
  mutate(IMF_all_SD_0_1yBef=ifelse(IMF_ALL_agreed>0 & SD_0_1yBef.BM==1,1,0),
         IMF_all_SD_0_2yBef=ifelse(IMF_ALL_agreed>0 & SD_0_2yBef.BM==1,1,0),
         IMF_all_SD_0_3yBef=ifelse(IMF_ALL_agreed>0 & SD_0_3yBef.BM==1,1,0),
         IMF_all_SD_0_4yBef=ifelse(IMF_ALL_agreed>0 & SD_0_4yBef.BM==1,1,0))

data.frame(mydata) %>% 
  filter(IMF_ALL_agreed>0) %>%
  summarize(mean(IMF_all_SD_0_1yBef,na.rm=T),
            mean(IMF_all_SD_0_2yBef,na.rm=T),
            mean(IMF_all_SD_0_3yBef,na.rm=T),
            mean(IMF_all_SD_0_4yBef,na.rm=T))

mydata=mydata %>%
  mutate(IMF_all_SD_0_1yBefAft=ifelse(IMF_ALL_agreed>0 & SD_0_1yBefAft.BM==1,1,0),
         IMF_all_SD_0_2yBefAft=ifelse(IMF_ALL_agreed>0 & SD_0_2yBefAft.BM==1,1,0),
         IMF_all_SD_0_3yBefAft=ifelse(IMF_ALL_agreed>0 & SD_0_3yBefAft.BM==1,1,0),
         IMF_all_SD_0_4yBefAft=ifelse(IMF_ALL_agreed>0 & SD_0_4yBefAft.BM==1,1,0))

data.frame(mydata) %>% 
  filter(IMF_ALL_agreed>0) %>%
  summarize(mean(IMF_all_SD_0_1yBefAft,na.rm=T),
            mean(IMF_all_SD_0_2yBefAft,na.rm=T),
            mean(IMF_all_SD_0_3yBefAft,na.rm=T),
            mean(IMF_all_SD_0_4yBefAft,na.rm=T))




# Rename variables ####
mydata=mydata %>% mutate(Gov.Gross.debt=as.numeric(G_XWDG_G01_GDP_PT),
                         Gov.Gross.debt.HPDD=as.numeric(GGXWDG_GDP),
                         Gov.Primary.bal=as.numeric(GGXONLB_G01_GDP_PT))

# find profile of recessions/booms

cycles=cycle_stats(mydata,"GDPV_XDC_cycle")
cycles=cycles %>% na.omit()
cycles=cycles %>% filter(amplitude.A<25)
cycles=cycles %>% filter(amplitude.B> -25)
cycles=cycles %>% mutate(Output.Loss=(duration.B*amplitude.B)/2)

duration.A.q0.9=quantile(cycles$duration.A,probs = 0.90)
duration.B.q0.9=quantile(cycles$duration.B,probs = 0.90)

amplitude.A.q0.9=quantile(cycles$amplitude.A,probs = 0.90)
amplitude.B.q0.1=quantile(cycles$amplitude.B,probs = 0.10)

cycles=cycles %>% mutate(large_B=ifelse(amplitude.B<=amplitude.B.q0.1,1,0),
                         large_A=ifelse(amplitude.A>=amplitude.A.q0.9,1,0),
                         long_B=ifelse(duration.B>=duration.B.q0.9,1,0),
                         long_A=ifelse(duration.A>=duration.A.q0.9,1,0))

extrem_cycles=cycles %>% dplyr::select(ISO3_Code,Index_cycle,large_B,large_A,long_B,long_A,Output.Loss)

mydata=merge(x=mydata,y=extrem_cycles,by=c("ISO3_Code","Index_cycle"),all.x=T)
mydata=mydata %>% mutate(large_B=ifelse(Phase_B==1 & large_B==1,1,0),
                             large_A=ifelse(Phase_A==1 & large_A==1,1,0),
                             long_A=ifelse(Phase_A==1 & long_A==1,1,0),
                             long_B=ifelse(Phase_B==1 & long_B==1,1,0),
                             Output.Loss=ifelse(Phase_B==1,Output.Loss,0))




