cycle_stats=function(mydata,var){
 
 # mydata<-mydata %>% dplyr::select(ISO3_Code,Period,var)
 # mydata<-Expansion_recession(mydata,var,mincycle = mincycle,minphase = minphase)
  
#  list of variables that are need to use the function if no then disply error message
 vars= c("Phase_A","Phase_A_t","Index_Phase_A","length.Phase_A","Phase_B","Phase_B_t","Index_Phase_B","length.Phase_B", "Peaks","Troughs", "Index_cycle")

 if(all(vars %in% names(mydata))){
   mystats<-data.frame(mydata) %>% filter((Peaks==1 | Troughs==1)) %>% group_by(ISO3_Code) %>%
    mutate(Period_all=paste(dplyr::lag(Period),Period,dplyr::lead(Period),sep="/"),
           var_Trough_1=dplyr::lag(eval(parse(text=var))),
           var_Peaks=eval(parse(text=var)),
           var_Trough_2=dplyr::lead(eval(parse(text=var))),
           duration.B=dplyr::lead(length.Phase_B),
           duration.A=length.Phase_A,
           duration.C=duration.A+duration.B,
           amplitude.A=abs(var_Trough_1-var_Peaks),#/var_Trough_1,
           amplitude.B=-abs(var_Peaks-var_Trough_2),#/var_Peaks,
           amplitude.C=abs(var_Trough_1-var_Trough_2),#/var_Trough_1,
           quarter.amplitude.A=amplitude.A/duration.A,
           quarter.amplitude.B=amplitude.B/duration.B,
           quarter.amplitude.C=amplitude.C/duration.C) %>% filter(Peaks!=0) %>%
     dplyr::select(ISO3_Code,Index_cycle,Period,Period_all,var_Trough_1,var_Peaks,var_Trough_2,
                   duration.A,duration.B,duration.C,
                   amplitude.A,amplitude.B,amplitude.C,
                   quarter.amplitude.A,quarter.amplitude.B,quarter.amplitude.C) 
   return(mystats)
   }else{
     mystats=NULL
     print(warning("Cannot compute cycle stats, please first apply the function Expansion_recession)"))
     return(mystats)
     
     }
  
}


cycle_stats_2=function(mydata,var,original_var){
  
  # mydata<-mydata %>% dplyr::select(ISO3_Code,Period,var)
  # mydata<-Expansion_recession(mydata,var,mincycle = mincycle,minphase = minphase)
  
  #  list of variables that are need to use the function if no then disply error message
  vars= c("Phase_A","Phase_A_t","Index_Phase_A","length.Phase_A","Phase_B","Phase_B_t","Index_Phase_B","length.Phase_B", "Peaks","Troughs", "Index_cycle")
  
  if(all(vars %in% names(mydata))){
    mystats<-data.frame(mydata) %>% group_by(ISO3_Code)%>% filter((Peaks==1 | Troughs==1)) %>% group_by(ISO3_Code) %>%
      mutate(Period_all=paste(dplyr::lag(Period),Period,dplyr::lead(Period),sep="/"),
             var_Trough_1=dplyr::lag(eval(parse(text=var))),
             var_Peaks=eval(parse(text=var)),
             var_Trough_2=dplyr::lead(eval(parse(text=var))),
             origin_Trough_1=dplyr::lag(eval(parse(text=original_var))),
             origin_var_Peaks=eval(parse(text=original_var)),
             origin_var_Trough_2=dplyr::lead(eval(parse(text=original_var))),
             duration.B=dplyr::lead(length.Phase_B),
             duration.A=length.Phase_A,
             duration.C=duration.A+duration.B,
             amplitude.A=-(var_Trough_1-var_Peaks)/origin_Trough_1,
             amplitude.B=(var_Peaks-var_Trough_2)/origin_var_Peaks,
             amplitude.C=(var_Trough_1-var_Trough_2)/origin_Trough_1,
             quarter.amplitude.A=amplitude.A/duration.A,
             quarter.amplitude.B=amplitude.B/duration.B,
             quarter.amplitude.C=amplitude.C/duration.C) %>% filter(Peaks!=0) %>%
      dplyr::select(ISO3_Code,Index_cycle,Period,Period_all,var_Trough_1,var_Peaks,var_Trough_2,
                    duration.A,duration.B,duration.C,
                    amplitude.A,amplitude.B,amplitude.C,
                    quarter.amplitude.A,quarter.amplitude.B,quarter.amplitude.C) 
    return(mystats)
  }else{
    mystats=NULL
    print(warning("Cannot compute cycle stats, please first apply the function Expansion_recession)"))
    return(mystats)
    
  }
  
}