
slide.1.Introduction=function(){
    box(title="Introduction",width=12,id="Slide.1",class="SlideBox",
        collapsible = T,
        collapsed = T,
        tags$hr(),
        h4(strong("Research Question:"),"Empirical investigation of the relationship between sovereign defaults and business cycles"),
        tags$ol(
        tags$li("When countries defaults?", strong("Recession -> default")),
        tags$li("What is the Cost of default?",strong("Default -> Recession"))),
        tags$hr(),
        h4(strong("Theoretical Intuition:")," Bad state of nature -> Default -> Consolidation of recession"),
        br(),
        tags$hr(),
        h4(strong("Strategy:")," be precise on the definition and the timing"))
}

slide.2.Compute.BC=function(){
    box(title="1. Measuring Economic Activity",width=12,id="Slide.2",class="SlideBox",
        collapsible = T,
        collapsed = T,
        hr(),
        h4("Notations"),
        tags$ol(
        tags$li(strong("Phase B:"),"Peak to Trough",
                tags$ol(
                  tags$li(strong("B1"),": First half of downturn"),
                  tags$li(strong("B2"),": Second half of downturn")
                )),
        tags$li(strong("Phase A:"),"Trough to Peak",
                tags$ol(
                  tags$li(strong("A1"),": First half of expansion"),
                  tags$li(strong("A2"),": Second half of expanson")
                ))),
        h4("The case of the US"),
        imageOutput("ctry_BC"),
        hr(),
        p("Notes:"),
        tags$ol(tags$li("Business cycles as the cyclical component of the HP filter on quarterly Real GDP"),
                tags$li("43 countries: Emerging and Developped since 1980")))
}
  
slide.3.defaults.def=function(){
    box(title="2. Defining Sovereign Crisis and getting the timing right.",width=12,id="Slide.3",class="SlideBox",
        collapsible = T,
        collapsed = T,
        h5(strong("The definition")),
        tags$ol(
          tags$li("Perimeters of the debt: Public, Publicly guaranteed, Public + private"),
          tags$li("Status of creditors: Domestic vs External debt, Private vs Official"),
          tags$li("Currency denomination: Domestic vs foreign denominated debt"),
          tags$li("Quality of the data: dummy vs amount of debt in default")
          ),
        h5(strong("... The frequency:")," yearly, monthly, daily"),
        h5(strong("... And the Authors:")," Reinhart & Rogoff (2009), Cruces & Trebesch (2013), Beers and Nadeau (2015), Cohen et Valladier(2012) ...."))
}

slide.4.BC.defaults=function(){
  box(
    collapsible = T,
    collapsed = T,
    width=12,
    title="3. Business Cycles and Defaults ",
    id="Slide.4",class="SlideBox",
    #sliderInput(inputId="Pres.val_SDR",label="Select minimum Special Drawing Rights agreed",min = 0,max=25000000,value=0),
    #plotOutput("Pres.SDR_cycle"),
    #plotOutput("Pres.plotdates_SDR_IMF"),
    uiOutput("Pres.select_ctry"),
    uiOutput("Pres.select_crisis"),
    plotOutput("Pres.plot_Cycles",width = "100%"),
    tags$ol("Notes:",
    tags$li("Dotted line corresponds to Extend Fund Facilities"),
    tags$li("Dashed line Corresponds to Standby Arrangement"),
    tags$li("Red line Corresponds to defaults according to the selected definition")))
}

slide.5.TwinDs=function(){
  box(
    collapsible = T,
    collapsed = T,
    width=12,
    id="Slide.5",class="SlideBox",
    title="4. Simple D, Twin Ds, Triple Ds, ... and The Business Cycle",
    h4("Sovereign and Currency crisis"),
    uiOutput("Pres.select_ctry.twinD"),
    uiOutput("Pres.select_crisis.twinD"),
    plotOutput("Pres.plot_all_crisis.twinD",width = "100%"),
    tags$ol("Notes:",
            tags$li("Dotted blue line corresponds to a quarterly depreciation of exchange rate by more than 20%, if crisis expend on several 
                    quarters then only the start of the serie is considered.")))
}

slide.10.Intuition=function(){
  box(
  collapsible = T,
  collapsed = T,
  width=12,
  title="5. Intuition of Timing",
  id="Slide.10",class="SlideBox",
  h4(strong("My intuition:")),
  plotOutput("diagram_crisis"),
  tags$hr(),
  h4(strong("What the data suggests:"),"probability of each type of crisis"),
  tableOutput("Prob_crisis_table")
  )
}

slide.6.BC.intensity=function(){
  box(
    collapsible = T,
    collapsed = T,
    width=12,
    title="6. Heterogenous Business Cycles: Intensity of Crisis",
    id="Slide.6",class="SlideBox",
    tags$ol("Crisis vs Severe Crisis",
    tags$li(strong("Amplitude:")," distance from Peak to Trough"),
    tags$li(strong("Duration:")," number of quarters of each phase"),
    tags$li(strong("Slop (Intensity):"),"amplitude/duration")),
    box(title="Details on mexican business cycles",width=12,collapsible = T,collapsed=T,style = "font-size:80%",tableOutput("Pres.cycle_stat_ctry"),
    tags$ol("Notes:",
            tags$li("durations are expressed in quarters"),
            tags$li("Amplitude in % of trend real GDP"),
            tags$li("Quarter amplitude in % of trend real GDP per quarter")))
  )
}

slide.7.Spread.fig=function(){
  box(
    collapsible = T,
    collapsed = T,
    width=12,
    title="7. What if IMF program is Arriving to Late...",
    id="Slide.7",class="SlideBox",
    h4("Empirical Strategy: Look if there is some signs of increasing tension on sovereign debt before switch of regime"),
    h4("Data: use Credit default Swap (CDS) as proxy for risk perception"),
        box(title="5 year contracts",collapsible=T,collapsed=F,imageOutput("Peak_5Y_cds")),
        box(title="1 year contracts",collapsible=T,collapsed=F,imageOutput("Peak_1Y_cds")))
}

slide.8.Macro.fig=function(){
  box(
    collapsible = T,
    collapsed = T,
    width=12,
    title="8. Macroeconomic variables around Peak",
    id="Slide.8",class="SlideBox",
        box(title="Gross Government Debt",imageOutput("Peak_Gov.Gross.debt"),width=6,collapsible=T,collapsed=F),
        box(title="Primary Balance",imageOutput("Peak_Gov.Primary.bal"),width=6,collapsible=T,collapsed=F),
        box(title="Exchange Rate Difference",imageOutput("Peak_d.EXCHUD"),width=6,collapsible=T,collapsed=F),
        box(title="Current Account",imageOutput("Peak_CBGDPR"),width=6,collapsible=T,collapsed=F)
    )
}

slide.9.econometrics=function(){
  box(
    collapsible = T,
    collapsed = T,
    width=12,
    title="9. A bit of Econometrics",
    id="Slide.9",class="SlideBox",
    h4("Set up similar to Regression discontinuity design"),
    p("Add formula "),
    p("Add regression results"))
}

slide.11.Conclusions=function(){
  box(
    collapsible = T,
    collapsed = T,
    width=12,
    title="10. Workstream and Improvments",
    id="Slide.10",class="SlideBox",
    p("- Be more precise on the timing of the stock market crashes (from yearly to quarterly)"),
    p("- Refine the definition of sovereign debt distress (i.e clean small IMF programs or when 
      no IMF program was agreed (ECUADOR 2009), look at default to different creditors: Paris Club, Official,... or 
      difference in debt instrument (bond vs bank loans)"),
    p("- Leverage  the information on the amount of debt in default and the size of the IMF programs."),
    p("- Leverage the information on the intensity of crisis (duration and amplitude)"),
    p("- Leverage information on the fiscal cost or output cost of banking crisis"),
    p("- Increase the sample and the available time dimension using the EBMI index from JP morgan
      or from the recent paper of Meyer, Reinhart and Trebesch (2019) that compute excess returns 
      for several countries on a monthly basis"),
    p("- Under estimation of structural crisis where trend growth is also affected."),# How to link it to Aguiar and Gopinath, 
     # the trend is the cycle, to identify the shock to trend growth that are those that push the countries into default."),
    p("- Where to find quarterly GDP for a more extended samples of countries?"),
    p("- Real time GDP at quarterly frequency available for long period? (vintage for OECD up to 2005)")
  )
}

fun.body.Pres03.2019=function(){
  fluidPage(
    div(id="header.presention",align="center",
        h1("Sovereign Debt Crisis and the Business Cycle"),
        h2("Manuel Betin"),
        h3("Macroeconomic Group Work shop"),
        h4("28-02-2019")
    ),
    slide.1.Introduction(),
    slide.2.Compute.BC(),
    slide.3.defaults.def(),
    slide.4.BC.defaults(),
    slide.5.TwinDs(),
    slide.10.Intuition(),
    slide.6.BC.intensity(),
    slide.7.Spread.fig(),
    slide.8.Macro.fig(),
    slide.9.econometrics(),
    slide.11.Conclusions()
  )
}


