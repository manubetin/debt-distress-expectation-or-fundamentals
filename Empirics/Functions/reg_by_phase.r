reg_by_phase_2=function(mydata,my_vars,ctrls=NULL,ctry.FE=F){ #NOT FINISHED YET
  #the function compute the average value of each policy variable in phase A and B in interaction with low and high
  
  if(ctry.FE==F & is.null(ctrls)){
    vars=c("Phase_A")
    vars_spec=vars
  }else if(ctry.FE==F & !is.null(ctrls)){
    vars=c("Phase_A",ctrls)
    vars_spec=vars
  }else if(ctry.FE==T & !is.null(ctrls)){ 
    vars=c("Phase_A",ctrls)
    vars_spec=c(vars,"factor(ISO3_Code)","factor(Period)")
  }else if(ctry.FE==T & is.null(ctrls)){
    vars=c("Phase_A")
    vars_spec=c(vars,"factor(ISO3_Code)","factor(Period)")
  }
  specification=paste(vars_spec,collapse="+")
  res_regs=data.frame(t(rep(NA,2)))
  colnames(res_regs)=c("Phase_B","Phase_A")
  reg_list=list()
  for(my_var in my_vars){
  
    mydata_reg=mydata %>% dplyr::select(ISO3_Code,Period,year,my_var,vars) %>% na.omit()
    ctries_sample=unique(mydata_reg$ISO3_Code)
    periods_sample=paste(min(mydata_reg$Period), max(mydata_reg$Period),sep=" - ")
    
    #set the specification of the regression
    #my_var=my_vars[1]
   
    fml=as.formula(paste(my_var,specification,sep="~"))
    reg=lm(fml,data=mydata_reg)
    betas=cbind(coef(summary(reg))[,"Estimate"][1:2],coef(summary(reg))[,"Pr(>|t|)"][1:2],sum(summary(reg)$df))
    colnames(betas)=c(paste(my_var,"est",sep="_"),paste(my_var,"pval",sep="_"),paste(my_var,"obs",sep="_"))
    rownames(betas)[1]="Phase_B"
    reg_list[[my_var]]=summary(reg)
    #res_pol_regs[[my_var]]=betas
    #names(res_pol_regs)[i]=my_var
    res_regs=rbind(res_regs,t(betas))
  }
  res_regs=data.frame(res_regs)
  res_regs=round(res_regs,4)
  res_regs=res_regs[-1,]
  plot=bar_chart_dispo_period(mydata_reg,my_var)$figure
  return(list(coefs=res_regs,Sample_countries=ctries_sample,Sample_periods=periods_sample,Figure=plot,summary_regression=reg))
}

reg_by_phase_4=function(mydata,my_vars,ctrls=NULL,ctry.FE=F){ #NOT FINISHED YET
  #the function compute the average value of each policy variable in phase A and B in interaction with low and high
  
  if(ctry.FE==F & is.null(ctrls)){
    vars=c("Phase_A1","Phase_A2","Phase_B1")
    vars_spec=vars
  }else if(ctry.FE==F & !is.null(ctrls)){
    vars=c("Phase_A1","Phase_A2","Phase_B1",ctrls)
    vars_spec=vars
  }else if(ctry.FE==T & !is.null(ctrls)){ 
    vars=c("Phase_A1","Phase_A2","Phase_B1",ctrls)
    vars_spec=c(vars,"factor(ISO3_Code)","factor(Period)")
  }else if(ctry.FE==T & is.null(ctrls)){
    vars=c("Phase_A1","Phase_A2","Phase_B1")
    vars_spec=c(vars,"factor(ISO3_Code)","factor(Period)")
  }
  specification=paste(vars_spec,collapse="+")
  res_regs=data.frame(t(rep(NA,4)))
  colnames(res_regs)=c("Phase_B2","Phase_A1","Phase_A2","Phase_B1")
  reg_list=list()
  for(my_var in my_vars){
    mydata_reg=mydata %>% dplyr::select(ISO3_Code,Period,year,my_var,vars) %>% na.omit()
    ctries_sample=unique(mydata_reg$ISO3_Code)
    periods_sample=paste(min(mydata_reg$Period), max(mydata_reg$Period),sep=" - ")
    #set the specification of the regression
    fml=as.formula(paste(my_var,specification,sep="~"))
    reg=lm(fml,data=mydata_reg)
    a=data.frame(coef(summary(reg)))
    a$vars=rownames(a)
    a=a %>% filter((str_detect(vars,"Phase_") | str_detect(vars,"(Intercept)")))
    rownames(a)=a$vars
    rownames(a)[1]="Phase_B2"
    a=a[,1:dim(a)[2]-1]
    reg_list[[my_var]]=reg
    res_regs=rbind(res_regs,t(a))
  }
  res_regs=data.frame(res_regs)
  res_regs=round(res_regs,4)
  res_regs=res_regs[-1,]
  plot=bar_chart_dispo_period(mydata_reg,my_var)$figure
  return(list(coefs=res_regs,Sample_countries=ctries_sample,Sample_periods=periods_sample,Figure=plot,summary_regression=reg))

  # ctry_FE=coef(summary(reg))
  # ctry_FE=data.frame(ctry_FE)
  # ctry_FE$vars=rownames(ctry_FE)
  # ctry_FE=ctry_FE %>% filter(str_detect(vars, 'ISO3_Code') | vars=="(Intercept)")
  # ctry_FE=ctry_FE %>% mutate(ISO3_Code=substr(vars,18,21),
  #                            value=Estimate+ctry_FE[1,1]) %>% dplyr::select(ISO3_Code,value)
  
  }
