# Phd: chap1 Sovereign CDS and Business cycles

## Explanations of scripts

### DATABASE CONSTRUCTION

#### 1.Bloomberg_consolidation_MP_BC.r

Consolidate the data for the CDS from the excel file of extraction from Bloomberg

#### 2.business_cycle_construction.r

Construct Business cycles and descriptive stat of BC for the selected sample

#### 3_Financial_market_data.r

Extract and consolidate data from financial markets 

#### 3_quarterly_macro_data.r

Extract quaterly data on debt (not used actually)

#### 4.Data_merging_MP_BC.r

Consolidate all the data into a single dataset

#### consolidate_EMBI.r

Consolidate and create database for yearly EMBI

#### Merge_sovereign_default_data.r

Merge the different sources of sovereign defaults into a single database 

### DESCRIPTIVE STATISTICS AND ANALYSIS

#### 5.Descriptive stats_MP_BC.r

Full set of descriptive stats on cds, exchange rates and analysis on Spreads

#### Compare_default_crisis.r

Analysis on Sovereign defaults and modifications of variables to have final variables 

#### Plot_event_study_around_Peak.r

Analysis of variables around episodes of switch of regime

#### New_regressions.r

Regression analysis on sovereign CDS spreads and in its behavior around peak

#### Compute_cycles.r

Compute and look at coincidence between different cycles 




