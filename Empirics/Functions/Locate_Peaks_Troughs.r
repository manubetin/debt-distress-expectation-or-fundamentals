Locate_Peaks_Troughs=function(mydata,var,mincycle=12,minphase=2){
  list_country=unique(mydata$ISO3_Code) #find list of countries available in the sample
  n_ctry=length(list_country) 
  j=0
  #res=vector("list",length=n_ctry)
  for(i in 1:n_ctry){
    country=list_country[i]
    #Subsample the dataset with only the column of interest
    
    mydata_country= mydata %>% filter(ISO3_Code==country & !is.na(eval(parse(text=var)))) %>% 
      mutate(ISO3_Code=as.character(ISO3_Code), Period=as.Date(Period),year=year(Period),quarter=quarter(Period))
    mydata_country=data.frame(mydata_country)
    
    if(dim(mydata_country)[1]!=0){ #only start the algorithm if the dataset is not empty
      start=min(mydata_country %>% dplyr::select(year)) #find effective start date of the serie
      end=max(mydata_country %>% dplyr::select(year)) #find effective end date of the serie
      start_quarter=min(mydata_country %>% filter(year==start) %>% dplyr::select(quarter))
      end_quarter=max(mydata_country %>% filter(year==end) %>% dplyr::select(quarter))
      
      #transform data into time series 
      mydata_country=ts(mydata_country,start=c(start,start_quarter), end=c(end,end_quarter),frequency = 4)
      
      #use algorithm to generate the cycles and handle error when no data available
      
      t <- try(BBQ(mydata_country[,var],mincycle=mincycle,minphase =minphase, name="Dating Business Cycles"), silent=T)
      if("try-error" %in% class(t)) {
        warning(paste(country,": Error in BBQ algorithm",sep=""))
      }else {
        j=j+1
        Peaks_troughs=show(t) #use show to convert the output of BBQ into a data.frame
        Peaks_troughs$ISO3_Code=country #include code countries for all rows
        #Peaks_troughs_sum=summary(t)
        #Peaks_troughs_sum$ISO3_Code=country
        if(j==1){ #initialize the dataset in the first loop
          Peaks_troughs_data=Peaks_troughs
          #Peaks_troughs_summary=Peaks_troughs_sum
        }else {
          Peaks_troughs_data=rbind(Peaks_troughs_data,Peaks_troughs) #append all the results for countries
          #Peaks_troughs_summary=rbind(Peaks_troughs_summary,Peaks_troughs_sum)
        }
      }
    }  
  }
  
  Peaks_troughs_data$Duration=as.numeric(Peaks_troughs_data$Duration)
  #transform periods in the proper quaterly format
  Peaks_troughs_data$Peaks=as.Date(as.yearqtr(Peaks_troughs_data$Peaks, format = "%YQ%q")) 
  Peaks_troughs_data$Troughs=as.Date(as.yearqtr(Peaks_troughs_data$Troughs, format = "%YQ%q"))
  
  #match the two dataset to have a columns of dummies for peaks and Troughs in the main dataset
  list_country=unique(as.character(mydata$ISO3_Code))
  n_ctry=length(list_country)
  mydata=data.frame(mydata)
  mydata<-mydata %>% mutate(Period=as.Date(Period))
  for(i in 1:n_ctry){
    country=list_country[i]
    #create a dummy variable for each peak in the data
    mydata[mydata$ISO3_Code==country,"Peaks"]=ifelse(mydata[mydata$ISO3_Code==country,"Period"] %in% Peaks_troughs_data[Peaks_troughs_data$ISO3_Code==country,"Peaks"],1,NA) 
    #create a dummy variable for each trough in the data
    mydata[mydata$ISO3_Code==country,"Troughs"]=ifelse(mydata[mydata$ISO3_Code==country,"Period"] %in% Peaks_troughs_data[Peaks_troughs_data$ISO3_Code==country,"Troughs"],1,NA)
  }
  mydata=mydata %>% group_by(ISO3_Code,Period) %>% distinct()#make sure there is no duplicate in the dataset
  return(list(data=mydata,Peaks_troughs=Peaks_troughs_data))
  #the function returns two elements:
  # mydata is the dataset in panel format with the dummies
  # Peaks_troughs_data is the summary of peaks and troughs dates
}
