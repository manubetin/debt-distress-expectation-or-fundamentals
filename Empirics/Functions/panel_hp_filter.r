
panel_hp_filter=function(mydata,var,lambda,smooth_cycle=T){
  list_country=unique(mydata$ISO3_Code) #find list of countries available in the sample
  n_ctry=length(list_country) #find the number of countries
  min_quarters=10 #set the minimum number of quarter available to start the algorithm
  j=0
  for(i in 1:n_ctry){
    country=list_country[i]
    #Subsample the columns with only the variable that will be used
    mydata_country= mydata %>% filter(ISO3_Code==country) %>%
      mutate(year=year(Period),quarter(Period)) %>%
      dplyr::select(Period,ISO3_Code,year,quarter,var)
    
    #Remove Periods with missing values
    mydata_country_noNA= mydata_country %>% filter(!is.na(eval(parse(text=var)))) %>% dplyr::select(Period,ISO3_Code,year,quarter,var)
    
    #check whether there are missing quarters in dataset
    a=mydata_country_noNA %>% group_by(year,ISO3_Code) %>%summarize(obs=n()) %>% filter(obs!=4)
    completion_condition=(dim(data.frame(a))[1]==0 & dim(mydata_country_noNA)[1]>=min_quarters)
    
    if(completion_condition) { #apply the filter if the serie has no missing quarter and has more than 10 observations
      
      filter=hpfilter(mydata_country_noNA[,var],type="lambda",freq=lambda)
      filter_cycle=filter$cycle
      
      #find the start and end periods for which the information is available
      start=min(mydata_country_noNA %>% dplyr::select(year))
      start_quarter=min(mydata_country_noNA %>% filter(year==start) %>% dplyr::select(quarter))
      end=max(mydata_country_noNA %>% dplyr::select(year))
      end_quarter=max(mydata_country_noNA %>% filter(year==end) %>% dplyr::select(quarter))
      
      #transform serie into time series
      filter_cycle=ts(filter_cycle,start=c(start,start_quarter),end=c(end,end_quarter),frequency = 4)
      dates=as.Date(filter_cycle) # store the serie of dates
      
      if(smooth_cycle==T){
        #use algorithm to smooth the cyclical component (DS for Deseasonalized) using STL decomposition
        filter_cycle_trend=stl(filter_cycle,s.window = "periodic")$time.series[,"trend"]
      }else filter_cycle_trend=filter_cycle #use the cycle with no seasonal adjustment
      
      filter_cycle_trend=as.numeric(data.frame(unlist(filter_cycle_trend))[,1]) #reset the serie into proper format
      filter_cycle_trend=cbind(dates,data.frame(filter_cycle_trend)) #include the dates to the serie
      filter_cycle_trend$ISO3_Code=country
      colnames(filter_cycle_trend)=c("Period","Value_cycle_DS","ISO3_Code") #rename the columns of the dataset
      mydata_country=cbind(filter_cycle_trend, filter$x,filter$cycle,filter$trend)
      colnames(mydata_country)=c("Period","Value_cycle_DS","ISO3_Code","Value_filtered","Value_cycle","Value_trend")
      j=j+1
      if(j==1){ mydata_filtered=mydata_country} #in the first loop initialize the dataset with all countries
      mydata_filtered=rbind(mydata_filtered,mydata_country) 
      # print(paste(country," data has been succesfully detrended",sep=""))
    }else warning(paste(country," data has not been detrended",sep="")) #warning message for countries that did not pass the conditions to launch the algorithm
  }
  return(mydata_filtered)
}
