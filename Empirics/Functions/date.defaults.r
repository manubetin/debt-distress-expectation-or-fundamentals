show_defaults=function(mydata_q,var){
  mydata_q=data.frame(mydata_q) %>%
    group_by(eval(parse(text=var)),Period) %>%
    mutate(defaults=ifelse(eval(parse(text=var))==1,paste(ISO3_Code,collapse="-"),""),
           test=NA)
  
  a=mydata_q %>% 
    filter(!is.na(defaults) & quarter==1) %>% 
    dplyr::select(year,defaults) %>% 
    distinct()
  
  p=ggplot(mydata_q,aes(x=Period,y=eval(parse(text=var))))+
    # geom_bar(stat = "identity",aes(x=Period,y=test))+
    geom_text(data=a,aes(label=defaults,y=5),angle = 0,size=2)+
    #geom_vline(xintercept = as.Date(SD_default$Period),aes(color="Sov. Default"),alpha=0.4) +
    geom_vline(xintercept = as.Date(a$Period),aes(color="Sov. Default"),alpha=0.3,linetype="dashed") +
    scale_x_date(breaks = seq(as.Date("1070-01-01"), as.Date("2017-12-31"), by="1 years"),date_labels =  "%Y")+
    xlab("") +
    #ylim(c(-100,200))+
    ylab(NULL) +
    coord_flip() +
    labs(title=NULL,subtitle =NULL ,caption=paste0("Definition:",var))+
    theme_classic(base_size = 8) + 
    theme(legend.position = "bottom",axis.text=element_text(size=8),axis.text.x=element_blank(),
          axis.ticks.y=element_blank())
  return(p)
}
default_table=function(mydata_q,var){
  dt=mydata_q %>% 
    filter(eval(parse(text=var))==1) %>% 
    dplyr::select(Period,year,var,ISO3_Code) %>% 
    distinct() %>% group_by(ISO3_Code) %>% summarize(years_defaults=paste(year,collapse="-"))
  return(dt)
}   
default_table_year_month=function(mydata_q,var){
  dt=mydata_q %>% 
    filter(eval(parse(text=var))==1) %>% 
    dplyr::select(Period,year,var,ISO3_Code) %>% 
    distinct() %>% group_by(ISO3_Code) %>% summarize(years_defaults=paste(paste(year,"(",quarter(Period),")",sep=""),collapse="-")) %>%
    mutate(ISO3_Code=as.character(ISO3_Code)) %>% arrange(ISO3_Code)
  return(dt)
}   
aggregate_defaults=function(mydata,vars){
  crisis=vars
  ctries=(data.frame(as.character(unique(mydata$ISO3_Code))))
  names(ctries)="ISO3_Code"
  ctries=ctries %>% arrange(ISO3_Code)
  
  tables_dates_crisis=lapply(crisis,function(x){
    dt=default_table_year_month(mydata,x) %>% arrange(ISO3_Code)
    names(dt)[2]=x
    dt=merge(x=dt,y=ctries,by=c("ISO3_Code"),all.y=T)
    dt=dt %>% arrange(ISO3_Code) %>% dplyr::select(-ISO3_Code)
  })
  #names(tables_dates_crisis)=crisis
  tables_dates_crisis=do.call(cbind,tables_dates_crisis)
  tables_dates_crisis=cbind(tables_dates_crisis,ctries)
  tables_dates_crisis=data.frame(tables_dates_crisis) %>% dplyr::select(ISO3_Code,everything())
  names(tables_dates_crisis)[1]="ISO3_Code"
  return(tables_dates_crisis)
}
