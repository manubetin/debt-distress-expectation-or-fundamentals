##### *********************************************************************************************#####

#                       New project                                                         ####

##### *********************************************************************************************#####


##### *********************************************************************************************#####
##### set up#####
##clean environment
rm(list = ls())

## set the working directory were the script is located
current_path = rstudioapi::getActiveDocumentContext()$path 
#setwd(dirname(current_path))

#download own functions
file.sources = list.files("Functions",  pattern="*.R$", full.names=TRUE, ignore.case=TRUE)
sapply(file.sources,source,.GlobalEnv) #download the functions necessary for the simulator.

##install common packages
packages <- c("dplyr","ggplot2","rio","reshape2","zoo","grDevices","foreign","car","countrycode","tidyquant","lubridate","readxl","XML","readr","knitr",
              "kableExtra","stargazer","survival","rmarkdown","IMFData","wbstats","factoextra","Quandl","BCDating")
#install.my.packages(packages)

## load common packages
load.my.packages(packages)

##### *********************************************************************************************#####

mydata=mydata_init(countries=list_countries("All"),start=1960,end=2018,frequency="quarter")
mydata=mydata %>% mutate(quarter=quarter(Period),
                         year=year(Period))

# Import FPI data ####
FPI_data <- import("Data/Macroeconomic variables/Fiscal_Performance_Indicators_VF_All.RData")$data
FPI_data=FPI_data %>% dplyr::select(ISO3_Code,
                                    Period,
                                    NGDP_WEO=GDP,
                                    G_XWDG_G01_GDP_PT,
                                    GGXONLB_G01_GDP_PT,
                                    cds5y,
                                    sovrate,
                                    GE.EST,
                                    RQ.EST)

mydata=merge(x=mydata,y=FPI_data,by.x=c("year","ISO3_Code"),by.y=c("Period","ISO3_Code"),all.x = T)


# Import yearly nominal GDP

NGDP_Y=import("Data/Business cycles/Input/NGDP_XDC_Y.RData")
NGDP_Y=NGDP_Y %>% mutate(Period=year(Period))
mydata=merge(x=mydata,y=NGDP_Y,by.x=c("year","ISO3_Code"),by.y=c("Period","ISO3_Code"),all.x = T)


# Import yearly HPDD 

HPDD=import("Data/Macroeconomic variables/Input/HPDD.RData")
HPDD=HPDD %>% mutate(Period=year(Period))
mydata=merge(x=mydata,y=HPDD,by.x=c("year","ISO3_Code"),by.y=c("Period","ISO3_Code"),all.x = T)

# Import ADB data ####

ADB_data <- import("Data/Macroeconomic variables/Input/ADB_data.csv")
ADB_data=ADB_data %>% mutate(year=year(Period),quarter=quarter(Period)) %>% dplyr::select(ISO3_Code,year,quarter,EXCHUD,CBGDPR,UNR,PEQ,TTRADE,IRL,IRS,FIGB_PA,bond.spread,bond.spread.IFS)
mydata=merge(x=mydata,y=ADB_data,by.x=c("ISO3_Code","year","quarter"),by.y=c("ISO3_Code","year","quarter"),all.x = T)

# Data on crisis ####
Crisis_Data=import("Data/Data on crisis/Data_crisis_q.RData") %>% dplyr::select(-c(year,quarter))
mydata=merge(x=mydata,y=Crisis_Data,by.x=c("Period","ISO3_Code"),by.y=c("Period","ISO3_Code"),all.x = T)

# Data on IMF quotas ####
IMF_quotas_Data=import("Data/Data IMF quota/IMF_quotas.RData")
IMF_quotas_Data=IMF_quotas_Data %>% mutate(year=year(Period),quarter=quarter(Period))
IMF_quotas_Data=IMF_quotas_Data %>% group_by(ISO3_Code,year,quarter) %>% summarize(Period=first(Period),
                                                                                   IMF_quotas=first(IMF_quotas))
IMF_quotas_Data=data.frame(IMF_quotas_Data) %>% dplyr::select(ISO3_Code,Period,IMF_quotas)


mydata=merge(x=mydata,y=IMF_quotas_Data,by.x=c("Period","ISO3_Code"),by.y=c("Period","ISO3_Code"),all.x = T)

# Data on cycles ####

load("Data/Business cycles/BC_output_hpfilter_GDPV from ADB.RData")
BC_data <-x
BC_data=BC_data %>% mutate(ISO3_Code=as.character(ISO3_Code))
BC_data=BC_data %>% dplyr::select(-Period)
mydata=merge(x=mydata,y=BC_data,by.x=c("ISO3_Code","year","quarter"),by.y=c("ISO3_Code","year","quarter"),all.x = T)

# Data from Risk premium database ####

SovDebt_riskPrem=import("Data/SovDebt_riskPrem_BusCycles_q.RData")

SovDebt_riskPrem=SovDebt_riskPrem %>%
  dplyr::select(ISO3_Code,Period,
                oil_price,bid_ask_spread,yield_curve,
                X5Y_cds=`5Y_cds`,X1Y_cds=`1Y_cds`,X2Y_cds=`2Y_cds`,
                X3Y_cds=`3Y_cds`,X4Y_cds=`4Y_cds`,X7Y_cds=`7Y_cds`,
                X10Y_cds=`10Y_cds`,X5Y_cds_vol=`5Y_cds_vol90`,
                US.yield.curve,bd_spread,cds_curve
)

mydata=merge(x=mydata,y=SovDebt_riskPrem,by=c("ISO3_Code","Period"),all.x = T)

# Download quarterly macro from API ####

macro_dt=import("Data/Macroeconomic variables/macro_variables_new.RData") %>% dplyr::select(-FIGB_PA)
mydata=merge(x=mydata,y=macro_dt,by=c("ISO3_Code","Period"),all.x=T)

# Country classification ####
classif=import("Data/World Bank classification/wb_country_classif_2018.xls") %>% dplyr::select(Code,Income_group)
mydata=merge(x=mydata,y=classif,by.x=c("ISO3_Code"),by.y=c("Code"),all.x=T)
mydata=mydata %>% 
  mutate(group=ifelse(Income_group %in% c("High income"),"Developped countries",
                      ifelse( Income_group %in% c("Upper middle income","Lower middle income","Low income"),
                              "Emerging countries",NA)))

# Construct variables #####
source("construct_variable_interest.r")

# Export data ####
rio::export(mydata,"Shiny_presentation/data/data.RData")
rio::export(mydata,"Data/Data on crisis/Data_crisis_q.RData")


