fun.body.Tabid_3=function(){
  ctries_cycles=mydata_q %>% filter(!is.na(GDPV_XDC_cycle))
  ctries_cycles=unique(ctries_cycles$ISO3_Code)
  #varCycles=c("X5Y_cds","Gov.Gross.debt","Gov.Primary.bal","UNR","d.EXCHUD","EXCHUD")
  fluidPage(
    box(selectInput(inputId="idCountryCycle",label="Select the Country",
                    choices=ctries_cycles)),
    box(title="Figures Business Cycles",width=12,collapsible = T,collapsed=T,
         box(title="Original data",width=6,plotOutput("GDP_original")),
         box(title="Cyclical Component of Real GDP",width=6,plotOutput("Cycles")),
         box(title="Locating Turning Points",width=6,plotOutput("Turning_points")),
         box(title="Locating Each Phase",width=6,plotOutput("PhaseA1.A2.B1.B2"))),
  box(title="Business cycles statistics",width=12,collapsed = T,collapsible = T,
      tableOutput("cycle_stat_ctry"),style = "font-size:80%"),
  box(title="Sample of Business cycles",width=12,collapsed = T,collapsible = T,
      tableOutput("table_Available_BC"),style = "font-size:80%")
  # box(title="Cycles of Macroeconomic Variables",
  #     selectInput(inputId="idAllVarCycle",label="Select the variable",
  #                 choices=varCycles))
  #     #plotOutput("var_cycle_plot"))
   )
}