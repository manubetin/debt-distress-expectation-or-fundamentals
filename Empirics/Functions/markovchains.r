# trans = matrix(c(0.5,0.5,
#                  0.5,0.5), ncol=2, byrow=TRUE);

markovchain = function(trans,periods=20){
  ts=matrix(NA,periods,2)
  # The state that we're starting in
  state=sample(1:dim(trans)[1], 1)
  ts[1,]=c(1,state)
  # Make twenty steps through the markov chain
  for (i in 2:periods){
    newState <- sample(1:ncol(trans), 1, prob=trans[state,])
    ts[i,]=c(i,newState)
    state=newState
  }
  ts=data.frame(ts)
  colnames(ts)=c("Time","State")
  return(ts)
}
