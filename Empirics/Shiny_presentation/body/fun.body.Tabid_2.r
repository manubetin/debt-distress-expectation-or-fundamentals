fun.body.Tabid_2=function(){
  fluidPage(
    box(collapsible=T,
        collapsed = T,
       title="Behavior of CDS Around Peak",
       checkboxGroupInput(inputId="idCDS", label="Choice CDS", 
                          choices = list("X1Y_cds","X2Y_cds","X3Y_cds","X4Y_cds","X5Y_cds","X7Y_cds","X10Y_cds"),
                          selected="X5Y_cds"),
       plotOutput("plot_CDSs_around_Peak") %>% withSpinner()
    ),
    box(
      width=12,
      collapsible = T,
      collapsed=T,
      title="Behavior of Macroeconomic Variables Around Peak",
      box(collapsible=T,collapsed=T,title="Stock Market Index Difference",plotOutput("d.Stock.market.index_aroundPeak"),width=6),
      box(collapsible=T,collapsed=T,title="Sovereign 5 years CDS",plotOutput("X5Y_cds_aroundPeak"),width=6),
      box(collapsible=T,collapsed=T,title="Exchange Rate Difference",plotOutput("d.XR_aroundPeak"),width=6),
      box(collapsible=T,collapsed=T,title="Government Gross Debt",plotOutput("Gov.Gross.debt.HPDD_aroundPeak"),width=6),
      box(collapsible=T,collapsed=T,title="Government Primary Balance",plotOutput("Gov.Primary.bal_aroundPeak"),width=6),
      box(collapsible=T,collapsed=T,title="Foreign Exchange Reserves",plotOutput("d.RAFA_USD_aroundPeak"),width=6),
      box(collapsible=T,collapsed=T,title="Unemployment Rate",plotOutput("UNR_aroundPeak"),width=6),
      box(collapsible=T,collapsed=T,title="Terms of Trade",plotOutput("TTRADE_aroundPeak"),width=6))
    ) 
}