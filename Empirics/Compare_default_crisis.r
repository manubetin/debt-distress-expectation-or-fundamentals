##### *********************************************************************************************#####

#                       Analyse event of defaults                                                       ####

##### *********************************************************************************************#####


##### *********************************************************************************************#####
##### set up#####
##clean environment
rm(list = ls())

## set the working directory were the script is located
#current_path = rstudioapi::getActiveDocumentContext()$path 

#download own functions
file.sources = list.files("Functions",  pattern="*.R$", full.names=TRUE, ignore.case=TRUE)
sapply(file.sources,source,.GlobalEnv) #download the functions necessary for the simulator.

##install common packages
packages <- c("dplyr","ggplot2","reshape2","mFilter","car","countrycode","naniar","tidyquant","lubridate",'tictoc',"rio","BCDating","gridExtra","stargazer")

#packages <- c("dplyr","ggplot2","countrycode","rio","tictoc","tidyquant")
#install.my.packages(packages)

## load common packages
load.my.packages(packages)

#***************************************************************************************####

load("Data/Data on crisis/Data_crisis.RData")
mydata_q=import("Data/Data on crisis/Data_crisis_q.RData")

run_vars_around_IMF==F
## Create catalogue of variables as dataframe #####

metadata=Crisis_Data$metadata
catalogue=lapply(names(Crisis_Data$metadata),function(x){
  dt=data.frame(Crisis_Data$metadata[[x]])
  dt
})
catalogue=do.call(rbind,catalogue)
catalogue =catalogue %>%
  rename(var_description=variables,
        general_Description=Description) %>% 
  mutate(var_name=rownames(catalogue)) %>% dplyr::select(var_name,everything())


IMF_programs_size=mydata_q %>% 
  group_by(ISO3_Code,year) %>% 
  summarise(IMF_agreed=sum(IMF_ALL_agreed,na.rm=T),
            IMF_drawn=sum(IMF_ALL_drawn,na.rm=T),
            diff=1-(IMF_agreed-IMF_drawn)/IMF_agreed,
            IMF_ALL_agreed_GDP=sum(IMF_ALL_agreed_GDP,na.rm=T),
            IMF_ALL_drawn_GDP=sum(IMF_ALL_drawn_GDP,na.rm=T),
            IMF_ALL_agreed_GDP=ifelse(IMF_ALL_agreed_GDP==0,NA,IMF_ALL_agreed_GDP),
            IMF_ALL_drawn_GDP=ifelse(IMF_ALL_drawn_GDP==0,NA,IMF_ALL_drawn_GDP),
            BC_FiscalCost_percGDP.LV=mean(BC_FiscalCost_percGDP.LV,na.rm=T),
            d.EXCHUD=mean(d.XR,na.rm=T)) %>%
  arrange(-IMF_ALL_agreed_GDP) %>%
  filter(IMF_agreed>0)

t.liquid.crisis=0.1
t.solv.crisis=0.9

bucket=0:20/20
pure.liq.crisis=lapply(bucket,function(x){
  a=Prob.Type.IMF.program(mydata_q,t.liquid.crisis = x,t.solv.crisis = 0.99)
  a$Proba[c("IMF.pure.liq.crisis")]
})
names(pure.liq.crisis)=bucket
pure.liq.crisis=do.call(rbind,pure.liq.crisis)

pure.solv.crisis=lapply(bucket,function(x){
  a=Prob.Type.IMF.program(mydata_q,t.liquid.crisis = 0.1,t.solv.crisis = x)
  a$Proba[c("IMF.pure.solv.crisis")]
})
names(pure.solv.crisis)=bucket
pure.solv.crisis=do.call(rbind,pure.solv.crisis)

cum.distrib.type.IMFprograms=cbind(pure.liq.crisis,pure.solv.crisis)

ggplot()+
geom_line(aes(x=as.numeric(rownames(cum.distrib.type.IMFprograms)),y=cum.distrib.type.IMFprograms$IMF.pure.solv.crisis,color="Pure solv crisis"))+
geom_line(aes(x=as.numeric(rownames(cum.distrib.type.IMFprograms)),y=cum.distrib.type.IMFprograms$IMF.pure.liq.crisis,color="Pure liquidity crisis"))+
theme_bw()+
  theme(legend.position = "bottom")+
  labs(x="threshold",
       y="% of episodes")


t.liquid.crisis=0.1
t.solv.crisis=0.9
mydata_q=mydata_q %>% mutate(IMF.pure.liq.crisis=ifelse(IMF_All_diff<=t.liquid.crisis,1,0),
                             IMF.Non.pure.liq.crisis=ifelse(IMF_All_diff>t.liquid.crisis,1,0),
                             IMF.pure.solv.crisis=ifelse(IMF_All_diff>=t.solv.crisis,1,0),
                             IMF.Non.pure.solv.crisis=ifelse(IMF_All_diff<t.solv.crisis,1,0),
                             IMF.liq.crisis=ifelse(IMF_All_diff<=0.5,1,0),
                             IMF.solv.crisis=ifelse(IMF_All_diff>0.5,1,0))

pure.solv.crisis_default=mydata_q %>% filter(IMF.pure.solv.crisis==1) %>% 
  summarize(SD_0_1yAft.BM=mean(SD_0_1yAft.BM,na.rm=T)*100,
            SD_0_2yAft.BM=mean(SD_0_2yAft.BM,na.rm=T)*100,
            SD_0_3yAft.BM=mean(SD_0_3yAft.BM,na.rm=T)*100,
            SD_0_4yAft.BM=mean(SD_0_4yAft.BM,na.rm=T)*100,
            SD_0_1yBef.BM=mean(SD_0_1yBef.BM,na.rm=T)*100,
            SD_0_2yBef.BM=mean(SD_0_2yBef.BM,na.rm=T)*100,
            SD_0_3yBef.BM=mean(SD_0_3yBef.BM,na.rm=T)*100,
            SD_0_4yBef.BM=mean(SD_0_4yBef.BM,na.rm=T)*100,
            SD_0_4yBefAft.BM=mean(SD_0_4yBefAft.BM,na.rm=T)*100)

pure.liq.crisis_default=mydata_q %>% filter(IMF.pure.liq.crisis==1) %>% 
  summarize(SD_0_1yAft.BM=mean(SD_0_1yAft.BM,na.rm=T)*100,
            SD_0_2yAft.BM=mean(SD_0_2yAft.BM,na.rm=T)*100,
            SD_0_3yAft.BM=mean(SD_0_3yAft.BM,na.rm=T)*100,
            SD_0_4yAft.BM=mean(SD_0_4yAft.BM,na.rm=T)*100,
            SD_0_1yBef.BM=mean(SD_0_1yBef.BM,na.rm=T)*100,
            SD_0_2yBef.BM=mean(SD_0_2yBef.BM,na.rm=T)*100,
            SD_0_3yBef.BM=mean(SD_0_3yBef.BM,na.rm=T)*100,
            SD_0_4yBef.BM=mean(SD_0_4yBef.BM,na.rm=T)*100,
            SD_0_4yBefAft.BM=mean(SD_0_4yBefAft.BM,na.rm=T)*100)

pure.liqSolv.crisis_default=cbind(t(pure.solv.crisis_default),t(pure.liq.crisis_default))
colnames(pure.liqSolv.crisis_default)=c("pure.solv.crisis","pure.liq.crisis")
names=rownames(pure.liqSolv.crisis_default)

pure.liqSolv.crisis_default=data.frame(pure.liqSolv.crisis_default) %>% mutate(diff=pure.solv.crisis-pure.liq.crisis)
rownames(pure.liqSolv.crisis_default)=names

names(mydata_q)
a=mydata_q %>% group_by(ISO3_Code) %>% filter(year==2009) %>% summarize(g=mean(g.NGDP_XDC_y,na.rm=T))

a=mydata_q %>% group_by(Income_group,IMF_Non_pure_Expect_crisis) %>% filter(year>1960) %>% summarize(g=mean(g.R.NGDP_XDC_y,na.rm=T),
                                                                          g.R=mean(g.R.GDPV,na.rm=T))
a

b=mydata_q %>% filter(ISO3_Code=="ARG")

ggplot()+ 
  geom_line(data=b,aes(x=Period,y=g.R.NGDP_XDC_y))+
  geom_line(data=b,aes(x=Period,y=g.NGDP_XDC_y),color="red")+
geom_line(data=b,aes(x=Period,y=g.R.GDPV),color="blue")

ggplot()+ 
  geom_density(data=b,aes(g.NGDP_XDC_y))


if(run_vars_around_IMF==T){
  
# average variables 2 years after the program
#vars=c("IMF.pure.liq.crisis","IMF.pure.solv.crisis","IMF.liq.crisis","IMF.solv.crisis","IMF.Non.pure.solv.crisis","IMF.Non.pure.liq.crisis")
vars=c("IMF.pure.liq.crisis","IMF.pure.solv.crisis","IMF.Non.pure.liq.crisis")
liqSolv.Crisis_summary=sapply(vars,function(x){
  eval_IMF_type_crises(mydata_q,x,t.liquid.crisis=0.1,t.solv.crisis=0.9)$summary
})

#liqSolv.Crisis_summary=t(liqSolv.Crisis_summary)
#liqSolv.Crisis_summary=data.frame(liqSolv.Crisis_summary)

liqSolv.Crisis_summary=liqSolv.Crisis_summary[c("B.d","B.large.d","gdebt","d.gdebt","SD.d","CC.d","CC","BC.d","BC","n.obs"),] 

colnames(liqSolv.Crisis_summary)=c("Pure liquidity","Pure solvency",
                                           "Solvency")

rownames(liqSolv.Crisis_summary)=c("P[Downturn=1]","P[Large Downturn=1]",
                                           "Gov. Gross Debt (% GDP)","difference in Gov. Gross Debt (%)",
                                           "P[Sov. Default=1]",
                                           "P[Currency Crisis=1]"," - Avg. qrt Exch.Rate depreciation (CC) (%)",
                                           "P[Banking Crisis=1]"," - Fisc.Cost Bank Crisis (BC) (% GDP)",
                                           "N.Obs")

vars=c("gdebt","d.gdebt","gdebt_max","CC","BC","SMC","FR")
list_plots_IMF_type_crisis=lapply(vars,function(x){
  plot.distrib.IMF_type_crisies(x,0.1,0.9)
})
names(list_plots_IMF_type_crisis)=vars

## average variables 2 years before the programs
vars=c("IMF.pure.liq.crisis","IMF.pure.solv.crisis","IMF.Non.pure.liq.crisis")
liqSolv.Crisis_summary_bef=sapply(vars,function(x){
  eval_IMF_type_crises_bef(mydata_q,x,t.liquid.crisis=0.1,t.solv.crisis=0.9)$summary
})
#liqSolv.Crisis_summary_bef=t(liqSolv.Crisis_summary_bef) 
#liqSolv.Crisis_summary_bef=data.frame(liqSolv.Crisis_summary_bef)

liqSolv.Crisis_summary_bef=liqSolv.Crisis_summary_bef[c("B.d","B.large.d","gdebt","d.gdebt","SD.d","CC.d","CC","BC.d","BC","n.obs"),] 

colnames(liqSolv.Crisis_summary_bef)=c("Pure liquidity","Pure solvency",
                                   "Solvency")

rownames(liqSolv.Crisis_summary_bef)=c("P[Downturn=1]","P[Large Downturn=1]",
                                       "Gov. Gross Debt (% GDP)","difference in Gov. Gross Debt (%)",
                                       "P[Sov. Default=1]",
                                       "P[Currency Crisis=1]"," - Avg. qrt Exch.Rate depreciation (CC) (%)",
                                       "P[Banking Crisis=1]"," - Fisc.Cost Bank Crisis (BC) (% GDP)",
                                       "N.Obs")

 vars=c("gdebt","d.gdebt","gdebt_max","CC","BC","SMC","FR")
 list_plots_IMF_type_crisis_bef=lapply(vars,function(x){
   plot.distrib.IMF_type_crisies_bef(x,0.1,0.9)
 })
 names(list_plots_IMF_type_crisis_bef)=vars

 list_plots_IMF=list(before_programs=list_plots_IMF_type_crisis_bef,
                     After_programs=list_plots_IMF_type_crisis)

list_tables_IMF=list(before_programs=liqSolv.Crisis_summary_bef,
                    After_programs=liqSolv.Crisis_summary)

list_tables_IMF$before_programs
list_tables_IMF$After_programs

list_plots_IMF[["before_programs"]][["FR"]]+lims(y=c(0,0.2),x=c(-100,100))
list_plots_IMF[["After_programs"]][["FR"]]+lims(y=c(0,0.2),x=c(-100,100))

list_plots_IMF[["before_programs"]][["CC"]] +lims(y=c(0,0.25),x=c(-10,40))
list_plots_IMF[["After_programs"]][["CC"]] +lims(y=c(0,0.25),x=c(-10,40))

list_plots_IMF[["before_programs"]][["gdebt_max"]] +lims(y=c(0,0.025),x=c(-100,600))
list_plots_IMF[["After_programs"]][["gdebt_max"]] +lims(y=c(0,0.025),x=c(-100,600))

list_plots_IMF[["before_programs"]][["d.gdebt"]] +lims(y=c(0,0.075),x=c(-10,300))
list_plots_IMF[["After_programs"]][["d.gdebt"]] +lims(y=c(0,0.075),x=c(-10,300))

list_plots_IMF[["before_programs"]][["BC"]] +lims(y=c(0,0.25),x=c(-10,40))
list_plots_IMF[["After_programs"]][["BC"]] +lims(y=c(0,0.25),x=c(-10,40))

export(list_plots_IMF,"shiny_presentation/data/Figures_IMFprogram_vars.RData")
export(list_tables_IMF,"shiny_presentation/data/Table_average_IMFprogram_vars.RData")

}else{
load("data/Figures_IMFprogram_vars.RData")
list_plots_IMF=list(After_programs=After_programs,
                    before_programs=before_programs)

load("data/Table_average_IMFprogram_vars.RData")
list_tables_IMF=list(After_programs=After_programs,
                     before_programs=before_programs)
}


IMFcompare_dt<<-import("Data/Data on crisis/source_files/IMF_programs_clean.RData")

IMFcompare_dt=IMFcompare_dt %>% mutate(year=year(Period),
                           diff=Amount_drawn/Amount_agreed) %>% arrange(year)

a=IMFcompare_dt  %>% group_by(year) %>%  summarize(agreed=sum(Amount_agreed,na.rm=T),
                                                  drawn=sum(Amount_drawn,na.rm=T),
                                                  diff=mean(diff)) %>% arrange(year)

fig_diff_TS=ggplot(data=IMFcompare_dt %>% filter(year>=1984)) +
  geom_rect(aes(xmin=1950,xmax=2020,ymin=0,ymax=0.1),fill="lightgreen",alpha=0.3)+
  geom_rect(aes(xmin=1950,xmax=2020,ymin=0.9,ymax=1),fill="darkred",alpha=0.3)+
  geom_rect(aes(xmin=1950,xmax=2020,ymin=0.1,ymax=0.9),fill="red",alpha=0.1)+
  geom_point(aes(x=year,y=diff,color=ISO3_Code,size=Amount_agreed_Quotas))+
  annotate("text",x=1954,y=0.05,label="Expectation driven",size=2)+
  annotate("text",x=1954,y=0.95,label="Pure fundamental driven",size=2)+
  annotate("text",x=1954,y=0.5,label="Fundamental driven ",size=2)+
  theme_bw()+
  labs(x=NULL,y="% of program drawn")+
  theme(legend.position = "none",axis.title.y=element_text(size=6))+
  lims(y=c(0,1))

ggplotly(fig_diff_TS)

IMFcompare_dt=IMFcompare_dt %>% mutate(year=year(Period),quarter=quarter(Period))
dt_quotas=mydata_q %>% dplyr::select(ISO3_Code,year,quarter,IMF_quotas)

IMFcompare_dt=merge(x=IMFcompare_dt,y=dt_quotas,by=c("ISO3_Code","year","quarter"),all.x=T)
IMFcompare_dt=IMFcompare_dt %>% mutate(Amount_agreed_quotas=(Amount_agreed/(IMF_quotas)*1000)*100,
                                       Amount_drawn_quotas=(Amount_drawn/(IMF_quotas)*1000)*100)


a=IMFcompare_dt %>% group_by(year) %>% summarize(agreed=mean(Amount_agreed_quotas,na.rm=T),
                                                 drawn=mean(Amount_drawn_quotas,na.rm=T))


ggplot(data=a) +
  geom_bar(stat="identity",aes(x=year,y=agreed,fill="Agreed"))+
  geom_bar(stat="identity",aes(x=year,y=drawn,fill="drawn"))+
  theme_bw()


# years=unique(IMFcompare_dt$year)
# for(var in years){
#   b=IMFcompare_dt %>% filter(year==var)
#   plot=ggplot(data=b) +
#     labs(title=var)+
#     #lims(y=c(0,2))+
#     geom_histogram(aes(diff))+
#     theme_bw()
#   print(plot)
# }



dt=import("Data/Data on crisis/source_files/IMF_programs_clean.RData")

dt=dt %>% mutate(diff=(1-(Amount_agreed-Amount_drawn)/Amount_agreed),
                 pure.liq=ifelse(diff<=t.liquid.crisis,1,0),
                 non.pure.liq=ifelse(diff>t.liquid.crisis,1,0),
                 pure.solv=ifelse(diff>=t.solv.crisis,1,0),
                 duration=as.numeric(year(as.Date(Date_Expiration)))-as.numeric(year(as.Date(Period))),
                 name.variable=substring(variable,4,6))

ggplot(dt)+
  geom_histogram(aes(duration,fill=name.variable),binwidth = 0.5)+
  scale_fill_discrete(name=NULL)+
  scale_x_continuous(breaks=0:6)+
  theme_bw()+
  theme(legend.position="bottom")



IMFcompare=dt %>% group_by(variable) %>% summarize(Agreed=sum(Amount_agreed,na.rm=T),
                                        Drawn=sum(Amount_drawn,na.rm=T),
                                        diff=mean(diff,na.rm=T)*100,
                                        duration=mean(duration,na.rm=T),
                                        Sup.Rfacilities=sum(Sup_reserves_facilities,na.rm=T),
                                        pure.liq=mean(pure.liq,na.rm=T)*100,
                                        pure.solv=mean(pure.solv,na.rm=T)*100,
                                        non.pure.liq=mean(non.pure.liq,na.rm=T)*100,
                                        n.programs=n()) %>% mutate(variable=substr(variable,4,6))

var="pure.liq"
ggplot(IMFcompare)+
  geom_bar(stat="identity",aes(x=variable,y=get(var)),fill="darkblue")+
  theme(legend.position="bottom")+
  theme_bw()+
  labs(y=var,x=NULL)

dt=import("Data/SovDebt_riskPrem_BusCycles.RData")
dt=dt$data
ctries=c("PRT","IRL","MEX","COL","GRC","ESP","ARG","VEN")

dt=dt %>% filter(ISO3_Code %in% ctries) %>% 
  dplyr::select(ISO3_Code,Period,X5Y_cds=`5Y_cds`,
                X1Y_cds=`1Y_cds`) %>% filter(Period>="2007-01-01")

dt_IMF=import("Data/Data on crisis/source_files/IMF_programs_clean.RData")
dt_IMF=dt_IMF %>% filter(ISO3_Code %in% ctries & Period>="2007-01-01") %>%
  mutate(day_period=as.Date(paste0(year(as.Date(Period)),"-",month(as.Date(Period)),"-",day_arrangement)),
         diff=(1-(Amount_agreed-Amount_drawn)/Amount_agreed),
         pure.liq=ifelse(diff<=t.liquid.crisis,1,0),
         non.pure.liq=ifelse(diff>t.liquid.crisis,1,0),
         pure.solv=ifelse(diff>=t.solv.crisis,1,0))

SD_NPL=dt_IMF %>% filter(non.pure.liq==1) %>% dplyr::select(Period,ISO3_Code,diff)
SD_PL=dt_IMF %>% filter(pure.liq==1) %>% dplyr::select(Period,ISO3_Code,diff)
SD_PS=dt_IMF %>% filter(pure.solv==1) %>% dplyr::select(Period,ISO3_Code,diff)

SD_1=dt_IMF %>% filter(ISO3_Code=="IRL") %>% dplyr::select(Period,diff)
SD_2=dt_IMF %>% filter( ISO3_Code=="PRT") %>% dplyr::select(Period,diff)
SD_3=dt_IMF %>% filter( ISO3_Code=="MEX") %>% dplyr::select(Period,diff)
SD_4=dt_IMF %>% filter( ISO3_Code=="COL") %>% dplyr::select(Period,diff)

names(mydata_q)
mydata_q %>% filter(year==2009 & ISO3_Code %in% c("COL","MEX")) %>% dplyr::select(ISO3_Code,Period,Gov.Gross.debt.HPDD)

Fig_CDS_IMFprograms=list()
Fig_CDS_IMFprograms[["Solvency"]][["series"]]=ggplot(dt %>% filter(ISO3_Code %in% c("IRL","PRT","ESP")))+
                  geom_line(aes(x=Period,y=X5Y_cds,color=ISO3_Code))+
                  scale_x_date(date_breaks = "1 year",date_labels =  "%Y")+
                  annotate("rect",xmin=as.Date("2014-02-01"),xmax=as.Date("2018-01-01"),ymin=1000,ymax=1650,fill="lightgrey",alpha=0.4)+
                  annotate("text",y=1550,x=as.Date("2016-01-01"),label="Gov. Debt:",color="black",size=3)+
                  annotate("text",y=1450,x=as.Date("2015-01-01"),label="2008:",color="black",size=3)+
                  annotate("text",y=1300,x=as.Date("2014-10-01"),label=" PRT: 71%",color="blue",size=3)+
                  annotate("text",y=1200,x=as.Date("2014-10-01"),label="IRL:  44%",color="darkgreen",size=3)+
                  annotate("text",y=1100,x=as.Date("2014-10-01"),label=" ESP: 39%",color="darkred",size=3)+
                  annotate("text",y=1450,x=as.Date("2017-01-01"),label="2010:",color="black",size=3)+
                  annotate("text",y=1300,x=as.Date("2017-01-01"),label=" 96% (+25pp)",color="blue",size=3)+
                  annotate("text",y=1200,x=as.Date("2017-01-01"),label=" 86% (+42pp)",color="darkgreen",size=3)+
                  annotate("text",y=1100,x=as.Date("2017-01-01"),label=" 60% (+21pp)",color="darkred",size=3)+
                  scale_color_discrete(name=NULL,guide=F)+
                  labs(x=NULL,y="5 years CDS (bp)",title="A case of Solvency Crisis: Europe")+
                  theme_bw()

Fig_CDS_IMFprograms[["Solvency"]][["IMFprograms"]]=ggplot(dt %>% filter(ISO3_Code %in% c("IRL","PRT","ESP")))+
  geom_line(aes(x=Period,y=X5Y_cds,color=ISO3_Code))+
  scale_color_discrete(name=NULL,guide=F)+
  scale_x_date(date_breaks = "1 year",date_labels =  "%Y")+
  labs(x=NULL,y="5 years CDS (bp)",
       title="A case of Solvency Crisis: Europe")+
  annotate("rect",xmin=as.Date("2014-02-01"),xmax=as.Date("2018-01-01"),ymin=500,ymax=1650,fill="lightgrey",alpha=0.4)+
  annotate("text",y=1550,x=as.Date("2016-01-01"),label="Gov. Debt:",color="black",size=3)+
  annotate("text",y=1450,x=as.Date("2015-01-01"),label="2008:",color="black",size=3)+
  annotate("text",y=1300,x=as.Date("2014-10-01"),label=" PRT: 71%",color="blue",size=3)+
  annotate("text",y=1200,x=as.Date("2014-10-01"),label="IRL:  44%",color="darkgreen",size=3)+
  annotate("text",y=1100,x=as.Date("2014-10-01"),label=" ESP: 39%",color="darkred",size=3)+
  annotate("text",y=1450,x=as.Date("2017-01-01"),label="2010:",color="black",size=3)+
  annotate("text",y=1300,x=as.Date("2017-01-01"),label=" 96% (+25pp)",color="blue",size=3)+
  annotate("text",y=1200,x=as.Date("2017-01-01"),label=" 86% (+42pp)",color="darkgreen",size=3)+
  annotate("text",y=1100,x=as.Date("2017-01-01"),label=" 60% (+21pp)",color="darkred",size=3)+
  annotate("text",y=920,x=as.Date("2016-02-01"),label="Proportion of program drawn:",color="black",size=3)+
  annotate("text",y=800,x=as.Date("2015-11-01"),label="PRT: 67% (Solvency program) ",color="blue",size=2.8)+
  annotate("text",y=700,x=as.Date("2016-02-01"),label="IRL: 100% (Pure Solvency program) ",color="darkgreen",size=2.8)+
  annotate("text",y=600,x=as.Date("2015-03-01"),label="ESP: No program  ",color="darkred",size=2.8)+
  geom_line(aes(x=Period,y=X1Y_cds),color="darkred")+
  geom_vline(xintercept=as.numeric(SD_1$Period),color="green",linetype="solid")+
  geom_vline(xintercept=as.numeric(SD_2$Period),color="blue",linetype="solid")+
  theme_bw()

Fig_CDS_IMFprograms[["Liquidity"]][["series"]]=ggplot(dt %>% filter(ISO3_Code %in% c("MEX","COL")))+
                geom_line(aes(x=Period,y=X5Y_cds,color=ISO3_Code))+
                scale_color_discrete(name=NULL,guide=F)+
                scale_x_date(date_breaks = "1 year",date_labels =  "%Y")+
                annotate("rect",xmin=as.Date("2014-02-01"),xmax=as.Date("2018-01-01"),ymin=400,ymax=600,fill="lightgrey",alpha=0.4)+
                annotate("text",y=580,x=as.Date("2016-01-01"),label="Gov. Debt:",color="black",size=3)+
                annotate("text",y=550,x=as.Date("2015-01-01"),label="2007:",color="black",size=3)+
                annotate("text",y=500,x=as.Date("2014-10-01"),label=" MEX: 37%",color="blue",size=3)+
                annotate("text",y=450,x=as.Date("2014-10-01"),label="COL:  32%",color="darkgreen",size=3)+
                annotate("text",y=550,x=as.Date("2017-01-01"),label="2009:",color="black",size=3)+
                annotate("text",y=500,x=as.Date("2017-01-01"),label=" 44% (+7pp)",color="blue",size=3)+
                annotate("text",y=450,x=as.Date("2017-01-01"),label=" 35% (+3pp)",color="darkgreen",size=3)+
                labs(x=NULL,y="5 years CDS (bp)",
                     title="A case of Liquidity Crisis: Latin America")+
                theme_bw()

Fig_CDS_IMFprograms[["Liquidity"]][["IMFprograms"]]=ggplot(dt %>% filter(ISO3_Code %in% c("MEX","COL")))+
  geom_line(aes(x=Period,y=X5Y_cds,color=ISO3_Code))+
  scale_color_discrete(name=NULL,guide=F)+
  scale_x_date(date_breaks = "1 year",date_labels =  "%Y")+
  labs(x=NULL,y="5 years CDS (bp)",
       title="A case of Liquidity Crisis: Latin America")+
  geom_vline(xintercept=as.numeric(SD_3$Period),color="lightblue",linetype="dashed")+
  geom_vline(xintercept=as.numeric(SD_4$Period),color="red",linetype="dotted")+
  annotate("rect",xmin=as.Date("2014-02-01"),xmax=as.Date("2018-01-01"),ymin=270,ymax=600,fill="lightgrey",alpha=1)+
  annotate("text",y=580,x=as.Date("2016-01-01"),label="Gov. Debt:",color="black",size=3)+
  annotate("text",y=550,x=as.Date("2015-01-01"),label="2007:",color="black",size=3)+
  annotate("text",y=500,x=as.Date("2014-10-01"),label=" MEX: 37%",color="blue",size=3)+
  annotate("text",y=450,x=as.Date("2014-10-01"),label="COL:  32%",color="darkred",size=3)+
  annotate("text",y=550,x=as.Date("2017-01-01"),label="2009:",color="black",size=3)+
  annotate("text",y=500,x=as.Date("2017-01-01"),label=" 44% (+7pp)",color="blue",size=3)+
  annotate("text",y=450,x=as.Date("2017-01-01"),label=" 35% (+3pp)",color="darkred",size=3)+
  annotate("text",y=400,x=as.Date("2016-02-01"),label="Proportion of program drawn:",color="black",size=3)+
  annotate("text",y=350,x=as.Date("2015-12-01"),label="MEX: 0% (Pure Liquidity)",color="blue",size=3)+
  annotate("text",y=300,x=as.Date("2015-12-01"),label="COL:  0% (Pure Liquidity)",color="darkred",size=3)+
  theme_bw()

Fig_CDS_IMFprograms$Solvency$series
Fig_CDS_IMFprograms$Solvency$IMFprograms

Fig_CDS_IMFprograms$Liquidity$series
Fig_CDS_IMFprograms$Liquidity$IMFprograms

save(Fig_CDS_IMFprograms,file="Output/Graphs/IMFprograms/CDS_IMFprogram_example.RData")

CDS_liq_program

plots= plot.Cycles(mydata_q,"GDPV_XDC_cycle","BRA","X5Y_cds")

plots$`Phases A.B`

names(mydata_q)
vars=c("d.XR","UNR","TTRADE","d.Stock.market.index","X5Y_cds","Gov.Gross.debt.HPDD","Gov.Primary.bal","d.RAFA_USD")
var_around_peak=lapply(vars,function(x){
  Plot_var_around_Peak(mydata_q,x)
})
names(var_around_peak)=vars
var_around_peak$X5Y_cds

save(var_around_peak,file="Output/Graphs/Vars around peak/vars.around.Peak.RData")

#Currency crisis and profile of recessions
prob_CC_AB=mydata_q %>%
  filter(!is.na(Phase_4)) %>%
  group_by(Phase_B) %>%
  summarize(prob_CC=mean(CC.MB,na.rm=T),
            prob_CC_first=mean(CC.MB_first,na.rm=T),
            n=n())

prob_CC=data.frame(mydata_q) %>%
  filter(!is.na(Phase_4)) %>%
  summarize(prob_CC=mean(CC.MB,na.rm=T),
            prob_CC_first=mean(CC.MB_first,na.rm=T),
            n=n())

prob_CC_AB_intensity=mydata_q %>%
  filter(!is.na(Phase_4)) %>%
  group_by(Phase_B,large_B,long_B) %>%
  summarize(prob_CC=mean(CC.MB,na.rm=T),
            prob_CC_first=mean(CC.MB_first,na.rm=T),
            n=n())

## IMF programs followed by default
mydata_q=data.frame(mydata_q) %>% group_by(ISO3_Code,year) %>%
  #mutate(Tot.arrears_qrt=sum(SD.debt.Tot.BM,na.rm=T)/4)
  mutate(Tot.arrears_qrt=sum(SD_Tot.BM_first,na.rm=T))

names(mydata_q)
mydata_q=data.frame(mydata_q) %>% group_by(ISO3_Code) %>% 
  mutate(SD_0_1yAft.BM=Tot.arrears_qrt+dplyr::lead(Tot.arrears_qrt,4),
         SD_0_2yAft.BM=SD_0_1yAft.BM+lead(SD_0_1yAft.BM,4),
         SD_0_3yAft.BM=SD_0_2yAft.BM+lead(SD_0_2yAft.BM,4),
         SD_0_4yAft.BM=SD_0_3yAft.BM+lead(SD_0_3yAft.BM,4))

mydata_q=data.frame(mydata_q) %>% group_by(ISO3_Code) %>% 
  mutate(SD_0y.BM=ifelse(Tot.arrears_qrt>0,1,0),
         SD_0_1yAft.BM=ifelse(SD_0_1yAft.BM>0,1,0),
         SD_0_2yAft.BM=ifelse(SD_0_2yAft.BM>0,1,0),
         SD_0_3yAft.BM=ifelse(SD_0_3yAft.BM>0,1,0),
         SD_0_4yAft.BM=ifelse(SD_0_4yAft.BM>0,1,0))

mydata_q=data.frame(mydata_q) %>% group_by(ISO3_Code) %>% 
  mutate(SD_0_1yBefAft.BM=lag(Tot.arrears_qrt,4)+Tot.arrears_qrt+dplyr::lead(Tot.arrears_qrt,4),
         SD_0_2yBefAft.BM=lag(SD_0_1yBefAft.BM,4)+SD_0_1yBefAft.BM+lead(SD_0_1yBefAft.BM,4),
         SD_0_3yBefAft.BM=lag(SD_0_2yBefAft.BM,4)+SD_0_2yBefAft.BM+lead(SD_0_2yBefAft.BM,4),
         SD_0_4yBefAft.BM=lag(SD_0_3yBefAft.BM,4)+SD_0_3yBefAft.BM+lead(SD_0_3yBefAft.BM,4))

mydata_q=data.frame(mydata_q) %>% group_by(ISO3_Code) %>% 
  mutate(SD_0_1yBefAft.BM=ifelse(SD_0_1yBefAft.BM>0,1,0),
         SD_0_2yBefAft.BM=ifelse(SD_0_2yBefAft.BM>0,1,0),
         SD_0_3yBefAft.BM=ifelse(SD_0_3yBefAft.BM>0,1,0),
         SD_0_4yBefAft.BM=ifelse(SD_0_4yBefAft.BM>0,1,0))

mydata_q=data.frame(mydata_q) %>% group_by(ISO3_Code) %>% 
  mutate(SD_0_1yBef.BM=lag(Tot.arrears_qrt,4)+Tot.arrears_qrt,
         SD_0_2yBef.BM=lag(SD_0_1yBef.BM,4)+SD_0_1yBef.BM,
         SD_0_3yBef.BM=lag(SD_0_2yBef.BM,4)+SD_0_2yBef.BM,
         SD_0_4yBef.BM=lag(SD_0_3yBef.BM,4)+SD_0_3yBef.BM)

mydata_q=data.frame(mydata_q) %>% group_by(ISO3_Code) %>% 
  mutate(SD_0_1yBef.BM=ifelse(SD_0_1yBef.BM>0,1,0),
         SD_0_2yBef.BM=ifelse(SD_0_2yBef.BM>0,1,0),
         SD_0_3yBef.BM=ifelse(SD_0_3yBef.BM>0,1,0),
         SD_0_4yBef.BM=ifelse(SD_0_4yBef.BM>0,1,0))

#IMF programs with no default 5 years before and 5 after the program
mydata_q=mydata_q %>%
  mutate(IMF_all_no_SD_0_4yBefAft=ifelse(IMF_ALL_agreed>0 & SD_0_4yBefAft.BM==0,1,0),
         IMF_ALL_agreed_dummy=ifelse(IMF_ALL_agreed>0,1,0),
         IMF_ALL_drawn_dummy=ifelse(IMF_ALL_drawn>0,1,0))

#proportion of programs with no defaults
data.frame(mydata_q) %>% filter(IMF_ALL_agreed>0) %>% summarize(mean(IMF_all_no_SD_0_4yBefAft,na.rm=T))

mydata_q %>% group_by(Phase_4) %>% summarize(mean=mean(IMF_all_no_SD_0_4yBefAft,na.rm=T),n=n())

#programs with default today, 1 year, 2 years, 3 years, 5 years in the future
mydata_q=mydata_q %>%
  mutate(IMF_all_SD_0_1yAft=ifelse(IMF_ALL_agreed>0 & SD_0_1yAft.BM==1,1,0),
         IMF_all_SD_0_2yAft=ifelse(IMF_ALL_agreed>0 & SD_0_2yAft.BM==1,1,0),
         IMF_all_SD_0_3yAft=ifelse(IMF_ALL_agreed>0 & SD_0_3yAft.BM==1,1,0),
         IMF_all_SD_0_4yAft=ifelse(IMF_ALL_agreed>0 & SD_0_4yAft.BM==1,1,0))

data.frame(mydata_q) %>%
  filter(IMF_ALL_agreed>0)%>%
  summarize(mean(IMF_all_SD_0_1yAft,na.rm=T),
            mean(IMF_all_SD_0_2yAft,na.rm=T),
            mean(IMF_all_SD_0_3yAft,na.rm=T),
            mean(IMF_all_SD_0_4yAft,na.rm=T))

#programs with default today, 1 year, 2 years,3 years or 4 years before
mydata_q=mydata_q %>%
  mutate(IMF_all_SD_0_1yBef=ifelse(IMF_ALL_agreed>0 & SD_0_1yBef.BM==1,1,0),
         IMF_all_SD_0_2yBef=ifelse(IMF_ALL_agreed>0 & SD_0_2yBef.BM==1,1,0),
         IMF_all_SD_0_3yBef=ifelse(IMF_ALL_agreed>0 & SD_0_3yBef.BM==1,1,0),
         IMF_all_SD_0_4yBef=ifelse(IMF_ALL_agreed>0 & SD_0_4yBef.BM==1,1,0))

data.frame(mydata_q) %>% 
  filter(IMF_ALL_agreed>0) %>%
  summarize(mean(IMF_all_SD_0_1yBef,na.rm=T),
            mean(IMF_all_SD_0_2yBef,na.rm=T),
            mean(IMF_all_SD_0_3yBef,na.rm=T),
            mean(IMF_all_SD_0_4yBef,na.rm=T))

mydata_q=mydata_q %>%
  mutate(IMF_all_SD_0_1yBefAft=ifelse(IMF_ALL_agreed>0 & SD_0_1yBefAft.BM==1,1,0),
         IMF_all_SD_0_2yBefAft=ifelse(IMF_ALL_agreed>0 & SD_0_2yBefAft.BM==1,1,0),
         IMF_all_SD_0_3yBefAft=ifelse(IMF_ALL_agreed>0 & SD_0_3yBefAft.BM==1,1,0),
         IMF_all_SD_0_4yBefAft=ifelse(IMF_ALL_agreed>0 & SD_0_4yBefAft.BM==1,1,0))

data.frame(mydata_q) %>% 
  filter(IMF_ALL_agreed>0) %>%
  summarize(mean(IMF_all_SD_0_1yBefAft,na.rm=T),
            mean(IMF_all_SD_0_2yBefAft,na.rm=T),
            mean(IMF_all_SD_0_3yBefAft,na.rm=T),
            mean(IMF_all_SD_0_4yBefAft,na.rm=T))

#cycles of Stocke markets
mydata_q=mydata_q %>% mutate(StockMarket_perf=ifelse(!is.na(PEQ),PEQ,FPE_EOP_IX),
                             StockMarket_perf=as.numeric(StockMarket_perf))

dt=data.frame(mydata_q) %>% dplyr::select(ISO3_Code,Period,StockMarket_perf)
dt=Expansions_Contractions_4(dt,"StockMarket_perf",mincycle=8,minphase = 4)
fun.Phase.AB(dt,"StockMarket_perf","MEX")
dt=dt %>% setNames(paste0("StockMarket_perf_",names(.))) %>% rename(ISO3_Code=StockMarket_perf_ISO3_Code,
                                                       Period=StockMarket_perf_Period)
mydata_q=merge(x=mydata_q,y=dt,by=c("ISO3_Code","Period"),all=T)
mydata_q=mydata_q %>% mutate(SMC.MB=ifelse(StockMarket_perf_Peaks==1,1,0))

mydata_q=mydata_q %>% mutate(RAFA_USD=as.numeric(RAFA_USD))

dt=data.frame(mydata_q) %>% dplyr::select(ISO3_Code,Period,RAFA_USD)

dt=Expansions_Contractions_4(dt,"RAFA_USD",mincycle=5,minphase = 2)
fun.Phase.AB(dt,"RAFA_USD","MEX")
dt=dt %>% setNames(paste0("RAFA_USD_",names(.))) %>% rename(ISO3_Code=RAFA_USD_ISO3_Code,
                                                            Period=RAFA_USD_Period)
mydata_q=merge(x=mydata_q,y=dt,by=c("ISO3_Code","Period"),all=T)
mydata_q=mydata_q %>% group_by(ISO3_Code) %>% arrange(ISO3_Code,Period) %>%
  mutate(FRC.MB=ifelse(RAFA_USD_Peaks==1,1,0),
         d.RAFA_USD=log(RAFA_USD)-log(lag(RAFA_USD,1)))

export(mydata_q,"Shiny_presentation/data/data.RData")

#disponibility data
vars=c("Phase_A","XR_rate_BIS","BC_FiscalCost_percGDP.LV","IMF_ALL_agreed_dummy","StockMarket_perf","Gov.Gross.debt.HPDD")
sample_full=table_dispo_periods_generic(mydata_q,vars)
sample_full=merge(x=sample_full,y=ctry_classif(),by=c("ISO3_Code"),all.x=T)
sample_full = sample_full %>% arrange(Classification,ISO3_Code) %>% dplyr::select(ISO3_Code,Classification,everything())

sample=sample_full %>% filter(!is.na(StockMarket_perf) | !is.na(Phase_A))
rownames(sample)=1:dim(sample)[1]


test=mydata_q %>% filter(year==2005) %>% filter(is.na(Phase_A)) %>% dplyr::select(ISO3_Code) %>% distinct()



