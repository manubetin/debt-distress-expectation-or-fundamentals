fun.body.Tabid_4=function(){
fluidPage(
  box(title="Cohen and Valladier (SD.CV)",width=12,collapsible=T,collapsed=T,
      h3("Default on external debt, yearly, all type of creditors, (XX countries), 1970-2012 if:"),
      p("- The sum of interest and principal arrears of a country is larger than 5% of total long term debt outstanding. (quantitative measure)"),
      p("- It receives support from the Paris Club (in a form of rescheduling or debt reduction). (Event)"),
      p("- It receives balance of payments support by the IMF in the form of Standby Arrangements or Extended Fund Facility, which depends on the quota the country gets from the IMF. (Event)")),
  box(title="Reinhart and Rogoff (SD_E.RR and SD_D.RR)",width=12,collapsible=T,collapsed=T,
      h3("External and domestic debt distress, all type of creditors (from the first year up to the date of resolution), 1800-2010, yearly, 68 countries according to the following criterion:"),
      p("- External default: failure to meet a principal or interest payment on the due date (or within the specified grace period). The episodes also include instance where reschedule debt is ultimately extinguished in terms less favourable than the original obligations. (Event)"),
      p("- Domestic default: same definition apply augmented with events involving freezing of bank deposits and or forcible conversions of such deposits and or forcible conversions of such deposits from dollars to local currency. (Event)")),
  box(title="Cruces and Trebesch (SD.CT)",width=12,collapsible=T,collapsed=T,
      h3("Public and publicly guaranteed debt defaults, private creditors, monthly,182 countries, 1970-2014 using the following criterion:"),
      p("- Restructurings(foreign commercial bank (London Club) and foreign bondholders), excluding default on domestic creditors and official creditors."),
      p("- Distressed debt exchanges are defined as restructurings of bonds (bank loans) at less favorable terms than the original bond (loan)."),
      p("- The sample only includes medium and long-term debt restructurings and cases in which short-term debt is exchanged into debt with maturity of more than one year.")),
  box(title="Laeven and Valencia (SD.LV)",width=12,collapsible=T,collapsed=T),
  box(title="Beers and Mavalwalla: the Bank's Credit Rating Assessment Group (GRAG), Bank of Canada (SD_tot.debt_BM)",width=12,collapsible=T,collapsed=T,
     h3("1860-2016, data on debt are expressed in nominal US dollars"),
     h4("Default occured when debt service is not paid on the due date or within a specified grace period, when payments are
        not made within the time frame specified under a guarantee, or, absent an outright payment default, in any of the following 
        circumstances where creditors incur material economic losses on the sovereign debt they hold:"),
      p("-Agreement between governemnts and creditors that reduce interesst rates and/or extend maturity on outstanding debt"),
      p("-Government exchange offers to creditors where existing debt is swapped for new debt on less-economic terms"),
      p("-Government purchase of debt at substantial discounts to par"),
      p("-Governemnt redenomination of foreign currency debt into new local currency obligations on less economic terms"),
      p("-Swap of sovereign debt for equity (usually relating to privatization programs) on less economic terms"),
      p("-Retrospective taxes targeting sovereign debt service payments"),
      p("-Conversion of central bank notes into new currency of less-than-equivalent face value")),
  box(title="International Swap Derivative Association (ISDA) (SD.ISDA)",width=12,collapsible=T,collapsed=T,
      h3("CDS triggering defaults: since 2005, for countries with existing CDS markets. A credit event is define ex-ante by at least one of the following event"),
      p("- Bankrupcy: insolvency or its inability to pay its debts."),
      p("- Failure to pay: if after expiration of the applicable grace period, the reference entity fails to make payment with respect to principal or interest on one or more of its obligations."),
      p("- Repudiation/moratorium: if the reference entity or government authority indicates that one or more of its obligations is no longer valid, or if the entity or government stops payment on such obligations"),
      p("- Obligation acceleration: when an obligation has become due and payable earlier than it would otherwise have been due because of a borrower's default or similar condition."),
      p("- Restructuring: %Refers to a change in the terms of a borrower's debt obligations that are deemed to be adverse for creditors. It is considered as a credit event only if it 
        occurs as a result of deterioration in the creditworthiness or financial condition of the sovereign, and if this restructuring is binding on all holders."),
      a(href="http://amwellclear.co.uk/w/index.php?title=Events_of_Default_-_ISDA_Provision","More details on ISDA definition")),
  box(title="International Monetary Fund programs (IMFEFF,IMFSBA,IMFPCL,IMFPLL...)",width=12,collapsible=T,collapsed=T,
      h3("Standby Arrangement"),
      p("- The SBA framework allows the Fund to respond quickly to countries' external financing needs, and to support policies
        designed to help them emerge from crisis and restore sustainable growth. The length of a SBA is flexible, and typically 
        covers a period of 12 to 24 months, but no more than 36 months, consistent with addressing short-term balance of payments problems,
        When a country borrows from the IMF, it agrees to adjust its economic policies to overcome the problems that led it to seek funding 
        in the first place. These commitments, including specific conditionality, are described in the member country's letter of intent 
        (which often includes memorandum of economic and financial policies). Repayment of borrowed resources under the SBA
        are due within 3 to 5 years of disbursement, which means each disbursement is repaid in eight equal quarterly installments beginning 3 
        years after the date of each disbursement."),
      a(href="https://www.imf.org/en/About/Factsheets/Sheets/2016/08/01/20/33/Stand-By-Arrangement","Documentation SBA"),
      h3("Extended Fund Facilities"),
      p("- The EFF was established to provide assistance to countries: (i) experiencing serious payments imbalances because of structural impediments; or 
        (ii) characterized by slow growth and an inherently weak balance of payments position. 
        The EFF provides assistance in support of comprehensive programs that include policies of the scope and character 
        required to correct structural imbalances over an extended period. Extended arrangements would normally be approved
        for periods not exceeding three years, with a maximum extension of up to one year where appropriate. However, a maximum
        duration of up to four years at approval is also allowed,
        There is also a longer repayment period of between 4 and 10 years, with repayments in twelve equal semiannual installments.
        Under an EFF, these commitments, including specific conditionality, are expected to have a strong focus on structural reforms to address institutional
        or economic weaknesses, in addition to policies that maintain macroeconomic stability."),
      a(href="https://www.imf.org/en/About/Factsheets/Sheets/2016/08/01/20/56/Extended-Fund-Facility","Documentation EFF"),
      h3("Flexible Credit Line (FCL):"),
       p("- Countries with very strong economic fundamentals and policy track records can apply for the FCL when faced with potential or actual balance of payments pressures.
         The FCL assures qualified countries they have large and up-front access to IMF resources with no ongoing conditions, given the strength of the policy frameworks."),
      a(href="https://www.imf.org/en/About/Factsheets/Sheets/2016/08/01/20/40/Flexible-Credit-Line","Documentation FCL"),
      h3("Precautionary and Liquidity Line (PPL):"),
      p("- The PLL provides financing to meet actual or potential balance of payments needs of countries with sound policies, and is intended to serve as a backstop or help resolve
        crises under wide-ranging situations. The PLL combines a qualification process (similar to that for the FCL but with a lower bar) with focused ex-post conditionality 
        aimed at addressing the remaining moderate vulnerabilities identified during qualification. 
        PLL arrangements can have duration of either six months, or one to two years. The six-month duration is available for countries with
        actual or potential short-term balance of payments needs that can make credible progress in addressing their vulnerabilities during the six-month period 
        Countries using the PLL commit to policies aimed at reducing their remaining vulnerabilities identified in the qualification process with focused conditionality."),
      a(href="https://www.imf.org/en/About/Factsheets/Sheets/2016/08/01/20/45/Precautionary-and-Liquidity-Line","Documentation PPL"),
      h3("Extend Credit Facility (ECF):"),
      p("- The ECF is available to all PRGT-eligible member countries that face a protracted balance of payments problem,
        i.e. when the resolution of the underlying macroeconomic imbalances would be expected to extend over the medium or longer term 
        Assistance under an ECF arrangement is provided for an initial duration from three to up to four years, with an overall maximum duration of five years.
        Access to ECF financing is determined on a case-by-case basis, taking into account the country's balance of payments need, the strength of its economic 
        program and capacity to repay the Fund, the amount of outstanding Fund credit and the member's record of past use of Fund credit, and is guided by access norms,
        Access to ECF financing is determined on a case-by-case basis, taking into account the country’s balance of payments need, the strength of its economic program and 
        capacity to repay the Fund, the amount of outstanding Fund credit and the member’s record of past use of Fund credit, 
        and is guided by access norms.1 Total access to concessional financing under the PRGT is limited to 75 percent of quota per year,
        and total outstanding concessional credit to 225 percent of quota. These limits can be exceeded in exceptional circumstances.
        Access may be augmented during an arrangement if needed.
        Quantitative conditions are used to monitor macroeconomic policy variables such as monetary aggregates, international reserves,
        fiscal balances, and external borrowing, reflecting the country’s program objectives.
        ECF-supported programs aim to safeguard social and other priority spending, including through explicit quantitative targets where possible.
        Financing under the ECF carries a zero interest rate at least through end-2018, with a grace period of 5½ years, and a final maturity of 10 years. 
        On October 3, 2016, the Executive Board approved a modification of the mechanism governing interest rate setting of PRGT facilities and PRGT interest rates will
        remain at zero for as long as and whenever global market rates are very low. The Fund reviews the level of interest rates for concessional facilities under the PRGT 
        every two years, with the next review expected to take place before end-2018."),
      a(href="https://www.imf.org/en/About/Factsheets/Sheets/2016/08/02/21/04/Extended-Credit-Facility","Documentation ECF")
  )
)
}