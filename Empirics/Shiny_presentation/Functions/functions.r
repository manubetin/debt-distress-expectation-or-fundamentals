show_defaults=function(mydata_q,var){
  mydata_q=data.frame(mydata_q) %>%
    group_by(eval(parse(text=var)),Period) %>%
    mutate(defaults=ifelse(eval(parse(text=var))==1,paste(ISO3_Code,collapse="-"),""),
           test=NA)
  
  a=mydata_q %>%
    filter(!is.na(defaults) & quarter==1) %>%
    dplyr::select(year,defaults) %>%
    distinct()
  
  p=ggplot(mydata_q,aes(x=Period,y=eval(parse(text=var))))+
    # geom_bar(stat = "identity",aes(x=Period,y=test))+
    geom_text(data=a,aes(label=defaults,y=5),angle = 0,size=4)+
    #geom_vline(xintercept = as.Date(SD_default$Period),aes(color="Sov. Default"),alpha=0.4) +
    geom_vline(xintercept = as.Date(a$Period),aes(color="Sov. Default"),alpha=0.3,linetype="dashed") +
    scale_x_date(breaks = seq(as.Date("1070-01-01"), as.Date("2017-12-31"), by="1 years"),date_labels =  "%Y")+
    xlab("") +
    #ylim(c(-100,200))+
    ylab(NULL) +
    coord_flip() +
    labs(title=NULL,subtitle =NULL ,caption=paste0("Definition:",var))+
    theme_classic(base_size = 14) +
    theme(legend.position = "bottom",axis.text=element_text(size=8),axis.text.x=element_blank(),
          axis.ticks.y=element_blank())
  return(p)
}

show_defaults2=function(mydata_q,var){
  mydata_q=data.frame(mydata_q) %>%
    group_by(eval(parse(text=var)),Period) %>%
    mutate(defaults=ifelse(eval(parse(text=var))==1,paste(ISO3_Code,collapse="-"),""))
  
  a=mydata_q %>%
    filter(!is.na(defaults)) %>%
    dplyr::select(year,defaults) %>%
    distinct()
  
  p=ggplot(mydata_q,aes(x=Period,y=eval(parse(text=var))))+
    geom_text(data=a,aes(label=defaults,y=5),angle = 0,size=4)+
    #geom_vline(xintercept = as.Date(SD_default$Period),aes(color="Sov. Default"),alpha=0.4) +
    geom_vline(xintercept = as.Date(a$Period),aes(color="Sov. Default"),alpha=0.3,linetype="dashed") +
    scale_x_date(breaks = seq(as.Date("1070-01-01"), as.Date("2017-12-31"), by="1 years"),date_labels =  "%Y")+
    xlab("") +
    #ylim(c(-100,200))+
    ylab(NULL) +
    coord_flip() +
    labs(title=NULL,subtitle =NULL ,caption=paste0("Definition:",var))+
    theme_classic(base_size = 14) +
    theme(legend.position = "bottom",axis.text=element_text(size=8),axis.text.x=element_blank(),
          axis.ticks.y=element_blank())
  return(p)
}

default_table=function(mydata_q,var){
  dt=mydata_q %>% 
    filter(eval(parse(text=var))==1) %>% 
    dplyr::select(year,var,ISO3_Code) %>% 
    distinct() %>% group_by(ISO3_Code) %>% summarize(years_defaults=paste(year,collapse="-"))
  #dt=merge(x=list_countries("All"),y=)
  return(dt)
}   

Plot_list_var_around_Peak=function(mydata_q,myvars,window=12){
  
  #myvars=c("X1Y_cds","X2Y_cds","X3Y_cds","X10Y_cds")
  dt=mydata_q %>% arrange(ISO3_Code,Period)
  dt=get_h(dt,"GDPV_XDC_cycle")
  compare_var= mydata_q %>% dplyr::select(ISO3_Code,Period,myvars)
  dt=merge(x=dt,y=compare_var,by=c("ISO3_Code","Period"))
  dt=dt %>% na.omit() %>% rename(time_lag=value)
  
  dt_collapsed=dt %>% group_by(time_lag) %>% summarize(var1=mean(eval(parse(text=myvars[1]))),
                                                       var2=mean(eval(parse(text=myvars[2]))),
                                                       var3=mean(eval(parse(text=myvars[3]))),
                                                       var4=mean(eval(parse(text=myvars[4]))))
  
  dt_collapsed=dt_collapsed %>% filter(time_lag<=window & time_lag >= - window)
  
  ggplot(data=dt_collapsed)+
    geom_vline(xintercept = 0)+
    #geom_point(aes(x=time_lag,y=var1))+
    geom_smooth(aes(x=time_lag,y=var1,color=myvars[1]),method='loess',fill="white",alpha=0)+
    # geom_point(aes(x=time_lag,y=var2))+
    geom_smooth(aes(x=time_lag,y=var2,color=myvars[2]),method='loess',fill="white",alpha=0)+
    geom_smooth(aes(x=time_lag,y=var3,color=myvars[3]),method='loess',fill="white",alpha=0)+
    geom_smooth(aes(x=time_lag,y=var4,color=myvars[4]),method='loess',fill="white",alpha=0)+
    scale_color_manual(name="",values=c("red","brown","green","blue"))+
    xlab("Distance to Peak")+
    ylab(NULL)+
    theme_bw(base_size = 15)+
    theme(legend.position = "bottom")
}

Plot_var_around_Peak=function(mydata_q,myvar,window=12){
  
  dt=mydata_q %>% arrange(ISO3_Code,Period)
  dt=get_h(dt,"GDPV_XDC_cycle")
  compare_var= mydata_q %>% dplyr::select(ISO3_Code,Period,myvar)
  dt=merge(x=dt,y=compare_var,by=c("ISO3_Code","Period"))
  dt=dt %>% na.omit() %>% rename(time_lag=value)
  
  dt_collapsed=dt %>% group_by(time_lag) %>% summarize(var=mean(eval(parse(text=myvar))))
  dt_collapsed=dt_collapsed %>% filter(time_lag<=window & time_lag >= - window)
  
  ggplot(data=dt_collapsed,aes(x=time_lag,y=var))+
    geom_point()+
    geom_vline(xintercept = 0)+
    geom_smooth(method='loess')+
    xlab("Distance to Peak")+
    ylab(myvar)+
    # ylim(c(0,700))+
    theme_bw(base_size = 15)
}

plot_IMF=function(mydata_q,ctry,var){
  country=ctry
  a=fun.Phase.A1.A2.B1.B2(mydata_q,"GDPV_XDC_cycle",country)
  IMFEFF_default=(mydata_q %>% filter(IMF_EFF==1 & ISO3_Code==country) %>% dplyr::select(Period))$Period
  IMFSBA_default=(mydata_q %>% filter(IMF_SBA==1 & ISO3_Code==country) %>% dplyr::select(Period))$Period
  IMFECF_default=(mydata_q %>% filter(IMF_ECF==1 & ISO3_Code==country) %>% dplyr::select(Period))$Period
  SD.CT_default=(mydata_q %>% filter(eval(parse(text=var))==1 & ISO3_Code==country) %>% dplyr::select(Period))$Period
  #IMF_prec_program=(mydata_q %>% filter(IMF_precaution_program_first==1 & ISO3_Code==country) %>% dplyr::select(Period))$Period
  
  a+
    geom_vline(xintercept = as.Date(IMFEFF_default),aes(color="EFF program"),alpha=0.9,linetype="dotted") +
    geom_vline(xintercept = as.Date(IMFSBA_default),aes(color="SBA program"),alpha=0.9,linetype="dashed")+
    geom_vline(xintercept = as.Date(IMFECF_default),aes(color="ECF program"),alpha=0.9,linetype="dotdash")+
    geom_vline(xintercept = as.Date(SD.CT_default),aes(color="Prec program"),alpha=0.9,linetype="solid",color="red")+
    labs(title=country,
         caption=var)
  # theme(axis.text.x=element_text(angle=90,hjust=1),legend.position = "bottom",axis.text=element_text(size=10))+
  #  labs(title=country,subtitle =NULL ,caption=NULL)
}


plot_all_crisis=function(mydata_q,ctry,var_SD,var_CC=NULL,var_SMC=NULL,var_IC=NULL){
  country=ctry
  a=fun.Phase.A1.A2.B1.B2(mydata_q,"GDPV_XDC_cycle",country)
  SD_crisis=(mydata_q %>% filter(eval(parse(text=var_SD))==1 & ISO3_Code==country) %>% dplyr::select(Period))$Period

    #IMF_prec_program=(mydata_q %>% filter(IMF_precaution_program_first==1 & ISO3_Code==country) %>% dplyr::select(Period))$Period
  
  if(!is.null(var_SD) & (is.null(var_CC) & is.null(var_SMC) & is.null(var_IC))){
     a+ geom_vline(xintercept = as.Date(SD_crisis),aes(color="Defaults"),alpha=0.9,linetype="solid",color="red")+
      labs(title=country,
           caption=NULL)
  }else if(!is.null(var_SD) & !is.null(var_CC) & (is.null(var_SMC) & is.null(var_IC))){
    CC_crisis=(mydata_q %>% filter(eval(parse(text=var_CC))==1 & ISO3_Code==country) %>% dplyr::select(Period))$Period
  
    a+ geom_vline(xintercept = as.Date(SD_crisis),aes(color="Defaults"),alpha=0.9,linetype="solid",color="red")+
       geom_vline(xintercept = as.Date(CC_crisis),aes(color="Currency Crisis"),alpha=0.9,linetype="dotdash",color="blue")+
      labs(title=country,
           caption=NULL)
  }else if(!is.null(var_SD) & !is.null(var_CC) & !is.null(var_SMC) & is.null(var_IC)){
    SMC_crisis=(mydata_q %>% filter(eval(parse(text=var_SMC))==1 & ISO3_Code==country) %>% dplyr::select(Period))$Period
    CC_crisis=(mydata_q %>% filter(eval(parse(text=var_CC))==1 & ISO3_Code==country) %>% dplyr::select(Period))$Period
    a+ geom_vline(xintercept = as.Date(SD_crisis),aes(color="Defaults"),alpha=0.9,linetype="solid",color="red")+
       geom_vline(xintercept = as.Date(CC_crisis),aes(color="Stock Market Crisis"),alpha=0.9,linetype="dotdash",color="blue")+
       geom_vline(xintercept = as.Date(SMC_crisis),aes(color="Currency Crisis"),alpha=0.9,linetype="dashed",color="green")+
       labs(title=country,
           caption=NULL)
  # theme(axis.text.x=element_text(angle=90,hjust=1),legend.position = "bottom",axis.text=element_text(size=10))+
  #  labs(title=country,subtitle =NULL ,caption=NULL)
}else if(!is.null(var_SD) & !is.null(var_CC) & !is.null(var_SMC) & !is.null(var_IC)){
  SMC_crisis=(mydata_q %>% filter(eval(parse(text=var_SMC))==1 & ISO3_Code==country) %>% dplyr::select(Period))$Period
  CC_crisis=(mydata_q %>% filter(eval(parse(text=var_CC))==1 & ISO3_Code==country) %>% dplyr::select(Period))$Period
  IC_crisis=(mydata_q %>% filter(eval(parse(text=var_IC))==1 & ISO3_Code==country) %>% dplyr::select(Period))$Period
  
  a+ geom_vline(xintercept = as.Date(SD_crisis),aes(color="Defaults"),alpha=0.9,linetype="solid",color="red")+
     geom_vline(xintercept = as.Date(CC_crisis),aes(color="Currency Crisis"),alpha=0.9,linetype="dotdash",color="blue")+
     geom_vline(xintercept = as.Date(SMC_crisis),aes(color="Stock market Crisis"),alpha=0.9,linetype="dashed",color="green")+
     geom_vline(xintercept = as.Date(IC_crisis),aes(color="Inflation Crisis"),alpha=0.9,linetype="dotted",color="orange")+
    labs(title=country,
         caption=NULL)
}
  
}

fun_Amount_IMF_programs=function(dt,var){
  dt_figure= dt %>% mutate(year=year(Period),
                           variable=substr(variable,4,7)) %>% group_by(year,variable) %>% summarize(myvar=sum(eval(parse(text=var)),na.rm=T)) %>% filter(!is.na(variable))
  plot=ggplot(dt_figure)+
    geom_bar(stat = "identity",aes(x=year,y=myvar,fill=variable)) +
    theme_bw()+
    labs(fill=NULL,
         x=NULL,
         y=var,
         caption="In thousands of SDRs")+
    #ylim(c(0,250000))+
    theme(legend.position="bottom")
  plot
}


