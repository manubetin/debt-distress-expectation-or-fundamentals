stats_by_pol=function(mystats,mydata,my_pols,list_stats){
  res_pol_regs_data=data.frame(NA,NA,NA,NA,NA,NA)
  colnames(res_pol_regs_data)=c("statistic","policy","HL","Estimate","Pval","degree_freedom")
  for(stat in list_stats){
    for(my_pol in my_pols[,"var.id"]){
      #value= mean(mydata[,my_pol],na.rm=T)
      pol_data<-mydata %>% filter(!is.na(eval(parse(text=my_pol)))) %>% select(ISO3_Code, Period, my_pol)
      temp<-try(data.frame(predict(lm(eval(parse(text=my_pol))~factor(ISO3_Code)+factor(Period), pol_data),pol_data)))
      if("try-error" %in% class(temp)) {
        warning(paste(my_pol,": no data for this policy variable"))
      }else {
        colnames(temp)="pred"
        mypol<-merge(pol_data,temp,by=0) %>% select(-Row.names) %>%
          mutate(HL=ifelse(eval(parse(text=my_pol))<pred,"low","high"))
        mydt<-merge(x=mystats,y=mypol,by=c("ISO3_Code","Period"))
      }
      #run the regression
      specification="HL"
      fml=as.formula(paste(stat,specification,sep="~"))
      reg=try(lm(fml,data=mydt), silent=T)
      #handle possible error in regression if not possible to compute then skip the policy and display warning message
      if("try-error" %in% class(reg)) {
        warning(paste(my_pol,": Error in reg for statistic ",stat,sep=""))
      }else {
        #only select the point estimate and pvalue
        betas=cbind(coef(summary(reg))[,"Estimate"],coef(summary(reg))[,"Pr(>|t|)"])
        colnames(betas)=c("Estimate","Pval")
        #handle possible error if no data for high or low, add the line to the dataframe with missing values
        if(dim(betas)[1]==1){betas=rbind(betas,c(NA,NA))}
        #create the necessart columns for the final dataset
        betas=data.frame(betas) %>% mutate(policy=my_pol,statistic=stat,degree_freedom=reg$df.residual)
        betas$HL=c("Low","High")
        betas=betas %>% dplyr::select(statistic,policy,HL,Estimate,Pval,degree_freedom)
        res_pol_regs_data=rbind(res_pol_regs_data,betas) #join all the result ub ibe colum
      }
    }
  }
  res_pol_regs_data=res_pol_regs_data[-1,] #remove first row that is NA
  res_pol_regs_data=res_pol_regs_data %>% mutate(significance=ifelse(Pval<=0.01,"***",ifelse(Pval>0.01 & Pval<=0.05,"**",ifelse(Pval>0.05 & Pval<=0.1,"*",""))))
  return(res_pol_regs_data)
}
