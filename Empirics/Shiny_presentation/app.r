
#libraries
suppressMessages(library(shiny))
suppressMessages(library(shinydashboard)) #template for applcations
suppressMessages(library(shinyjs)) #js interaction
suppressMessages(library(dplyr)) #manipulate data
suppressMessages(library(rio)) #import/export data
suppressMessages(library(V8)) #publish
suppressMessages(library(rsconnect)) #publish
suppressMessages(library(shinycssloaders)) #add loader when computing is long
suppressMessages(library(stargazer)) #tables in latex style
suppressMessages(library(bib2df)) #bibliography
suppressMessages(library(bibtex)) #bibliography
suppressMessages(library(DT)) #bibliography

#import generic functions
file.sources = list.files("../Functions",  pattern="*.R$", full.names=TRUE, ignore.case=TRUE)
sapply(file.sources,source,.GlobalEnv) #download the functions necessary for the simulator.

##install common packages
packages <- c("shiny","dplyr","ggplot2","reshape2",
              "mFilter","car","countrycode","tidyquant",
              "lubridate",'tictoc',"rio","BCDating","gridExtra",
              'timevis',"plotly")

#packages <- c("dplyr","ggplot2","countrycode","rio","tictoc","tidyquant")
#install.my.packages(packages)

## load common packages
load.my.packages(packages)

#import main quarterly dataset 
mydata_q<<-import("data/data.RData")
mydata_q<<- mydata_q %>% 
  #mutate(Phase_4=ifelse(Phase_A1==1,"A1",ifelse(Phase_A2==1,"A2",ifelse(Phase_B1==1,"B1","B2")))) %>% 
  filter(year>=1960)


#import data with dates of IMF programs (monthly frequency)
IMFcompare_dt<<-import("../Data/Data on crisis/source_files/IMF_programs_clean.RData")

#list of countries for the dropdown menu of countries

myctries<<-unique((mydata_q %>% filter(!is.na(Phase_4)))$ISO3_Code)
#myctries<<-unique(mydata_q$ISO3_Code)

#import bibliography
biblio=bib2df("data/bib_chap1.bib",separate_names = F)
biblio$AUTHOR <- unlist(lapply(biblio$AUTHOR, paste, collapse = " "))
biblio=biblio %>% dplyr::select(AUTHOR,TITLE,YEAR,JOURNAL)

#list of variables for dropdown menu of crisis
defs_default<<-c(
          'SD_Tot.BM',
          'SD_E.RR',
          'SD.CT',
          'SD.CV',
          'SD_D.RR',
          'SD_Tot.BM_first',
          #'SD.ISDA', #measures of sovereign defaults
          
          #first event of default
          'SD.RR_first',
          'SD_E.RR_first',
          'SD_D.RR_first',
          'SD_E.RR_last',
          'SD_Tot.BM_first',
          'SD_Tot.BM_last',
          "Spike_GoogleTrend",
          
          #first event of default by type of default
          'SD_IMF.BM_first','SD_PrivCred.BM_first','SD_ParisClub.BM_first',
          'SD_FCBonds.BM_first','SD_FCBankloans.BM_first','SD_LCdebt.BM_first',
          
          #IMF programs combined with defaults
          "IMF_ALL_agreed_dummy","IMF_ALL_drawn_dummy",
          "IMF_all_no_SD_0_4yBefAft","IMF_all_SD_0_1yAft","IMF_all_SD_0_2yAft",
          "IMF_all_SD_0_3yAft","IMF_all_SD_0_4yAft","IMF_all_SD_0_1yBef","IMF_all_SD_0_2yBef",
          "IMF_all_SD_0_3yBef","IMF_all_SD_0_4yBef","IMF_all_SD_0_1yBefAft","IMF_all_SD_0_2yBefAft",
          "IMF_all_SD_0_3yBefAft","IMF_all_SD_0_4yBefAft",
          "IMF_pure_Expect_crisis","IMF_pure_liquid_crisis","IMF_Non_pure_Expect_crisis",
          
          #IMF programs personal download
          'IMF_EFF','IMF_SBA','IMF_SAF','IMF_SCF',
          'IMF_ECF','IMF_ESF','IMF_PLL','IMF_FCL',
          
          #other crisis
          'IC.RR','BC.LV','CC.RR',"SMC.RR","CC.RR_first","CC.MB_first",
          "CC.MB","SMC.RR_first","SMC.MB","FRC.MB"
        )

#source functions used in the script
source("Functions/functions.r")


#source files containing the ui for each tab
files=list.files("body",full.names = T)
lapply(files,source)

# UI   
header=dashboardHeader(title = "04-04-2019")

sidebar=dashboardSidebar(
  # width = sidebar.wide,
  collapsed = T,
  sidebarMenuOutput("sidebar")
)

# 3) body of the app  
body=dashboardBody(
  tabItems(
    tabItem(tabName="Tabid_1",fun.body.Tabid_1()),
    tabItem(tabName="Tabid_2",fun.body.Tabid_2()),
    tabItem(tabName="Tabid_3",fun.body.Tabid_3()),
    tabItem(tabName="Tabid_4",fun.body.Tabid_4()),
    tabItem(tabName="Tabid_5",fun.body.Tabid_5()),
    tabItem(tabName="Tabid_6",fun.body.Tabid_6()),
    tabItem(tabName="Tabid_7",fun.body.Pres04.2019())
  ),
  includeCSS("css/custom.css")
  #includeCSS("css/custom.css")
) # close dashboardBody


ui=dashboardPage(skin="black",header,sidebar,body)

cycles_statistics<<-cycle_stats(mydata_q,"GDPV_XDC_cycle")

#T if you want to run the server side of the presentation 
#if T make sure the ui of the presentation is uncommented
run_presentation=T

##************************************************************************************************####
##  server ####
##************************************************************************************************#### 

server=function(input,output,session){
  
  # Render sidebar   [GENERAL]                                              #####
  output$sidebar=renderMenu({
    sidebarMenu(
      menuItem("Presentation",tabName="Tabid_7"),
      menuItem("Debt Distress / Debt Default",tabName ="Tabid_1"),#,icon = icon("fas fa-home")),#icon = icon("folder")  
      menuItem("Definitions",tabName="Tabid_4"),
      menuItem("Story around the cycle ",tabName="Tabid_2"),#,icon = icon("fas fa-search")),
      menuItem("Details on Cycles",tabName="Tabid_3"),
      menuItem("A bit of Econometrics",tabName="Tabid_5"),
      menuItem("References",tabName="Tabid_6")
    )
  })
  
  # Episodes of sovereign defaults: tabid_1 (ui code in fun.body.tabid_1.r) #####
      ## Table of crisis events by country       ####
  Crisis_events=mydata_q %>% 
    group_by(ISO3_Code,year,quarter) %>% 
    summarise(phase_4=last(Phase_4),
              IMF_agreed=sum(IMF_ALL_agreed,na.rm=T),
              IMF_drawn=sum(IMF_ALL_drawn,na.rm=T),
              diff=1-(IMF_agreed-IMF_drawn)/IMF_agreed,
              #SD=mean(SD_Tot.BM_first,na.rm=T),
              IMF_ALL_agreed_GDP=sum(IMF_ALL_agreed_GDP,na.rm=T),
              IMF_ALL_drawn_GDP=sum(IMF_ALL_drawn_GDP,na.rm=T),
              IMF_ALL_agreed_GDP=ifelse(IMF_ALL_agreed_GDP==0,NA,IMF_ALL_agreed_GDP),
              IMF_ALL_drawn_GDP=ifelse(IMF_ALL_drawn_GDP==0 & diff!=0,NA,IMF_ALL_drawn_GDP),
              BC_FiscalCost_percGDP.LV=mean(BC_FiscalCost_percGDP.LV,na.rm=T),
              g.R.NGDP_XDC_y=mean(g.R.NGDP_XDC_y,na.rm=T),
              d.Exch.Rate=mean(d.XR,na.rm=T),
              CC=mean(CC.MB_first,na.rm=T),
              debt_GDP=mean(Gov.Gross.debt.HPDD,na.rm=T),
              Gov.Primary.bal=mean(Gov.Primary.bal,rm=T)) %>%
    arrange(-year,-quarter) %>%
    filter(IMF_agreed>0 | !is.na(BC_FiscalCost_percGDP.LV) | CC==1) #%>% dplyr::select(-c(IMF_agreed,IMF_drawn))
  
  #show table of crisis for the selected country
  observe({
    Crisis_event= data.frame(Crisis_events) %>% filter(ISO3_Code==input$idCtry_crisis_events) %>% dplyr::select(-ISO3_Code)
    Crisis_event=Crisis_event %>% dplyr::select(-c(IMF_agreed,IMF_drawn))
    output$Crisis_events=renderTable(Crisis_event)
  })
      ## Sovereign debt distress                 ####
  
  
  #probability of each type (liquidity vs solvency) of IMF programs
  Prob_type_IMF_program=Prob.Type.IMF.program(data.frame(mydata_q),0.1,0.9)$Proba
  
   colnames(Prob_type_IMF_program)=c("Pure Liquidity (<10%)",
                                     "Pure Solvency(>90%)",
                                     "Solvency(>10%)")
  output$IMF_prob_type_program=renderTable(Prob_type_IMF_program)
   
  #Figures of Example reaction of market when liquidity or solvency programs
  load("../Output/Graphs/IMFprograms/CDS_IMFprogram_example.RData")
  
  output$CDS.liq=renderPlotly({ggplotly(Fig_CDS_IMFprograms$Liquidity$series)})
  
  output$CDS.liq.IMF=renderPlotly({ggplotly(Fig_CDS_IMFprograms$Liquidity$IMFprograms)})
  
  output$CDS.solv=renderPlotly({ggplotly(Fig_CDS_IMFprograms$Solvency$series)})
  
  output$CDS.solv.IMF=renderPlotly({ggplotly(Fig_CDS_IMFprograms$Solvency$IMFprograms)})
  
  # Number of IMF programs in the cycle
  observe({
    
    IMF_prog=input$type_IMF_prog
    
    if(IMF_prog=="agreed"){
    dt=mydata_q %>%  filter((IMF_ALL_agreed_GDP>input$min_size_imfprogram[1] & IMF_ALL_agreed_GDP<input$min_size_imfprogram[2]) & !is.na(Phase_4)) %>%
      mutate(IMF_program=ifelse((IMF_ALL_agreed_GDP>input$min_size_imfprogram[1] & IMF_ALL_agreed_GDP<input$min_size_imfprogram[2]),1,0))
    }else{
      dt=mydata_q %>%  filter((IMF_ALL_drawn_GDP>input$min_size_imfprogram[1] & IMF_ALL_drawn_GDP<input$min_size_imfprogram[2]) & !is.na(Phase_4)) %>%
        mutate(IMF_program=ifelse((IMF_ALL_drawn_GDP>input$min_size_imfprogram[1] & IMF_ALL_drawn_GDP<input$min_size_imfprogram[2]),1,0))
    }
    
    IMF_programs_size=mydata_q %>% 
      group_by(ISO3_Code,year) %>% 
      summarise(IMF_agreed=sum(IMF_ALL_agreed,na.rm=T),
                IMF_drawn=sum(IMF_ALL_drawn,na.rm=T),
                diff=1-(IMF_agreed-IMF_drawn)/IMF_agreed,
                IMF_ALL_agreed_GDP=sum(IMF_ALL_agreed_GDP,na.rm=T),
                IMF_ALL_drawn_GDP=sum(IMF_ALL_drawn_GDP,na.rm=T),
                IMF_ALL_agreed_GDP=ifelse(IMF_ALL_agreed_GDP==0,NA,IMF_ALL_agreed_GDP),
                IMF_ALL_drawn_GDP=ifelse(IMF_ALL_drawn_GDP==0,NA,IMF_ALL_drawn_GDP),
                Phase=first(Phase_4)
                #BC_FiscalCost_percGDP.LV=mean(BC_FiscalCost_percGDP.LV,na.rm=T),
                #d.EXCHUD=mean(d.XR,na.rm=T)
      ) %>%
      filter(!is.na(IMF_ALL_agreed_GDP) &  (IMF_ALL_agreed_GDP>input$min_size_imfprogram[1] & IMF_ALL_agreed_GDP<input$min_size_imfprogram[2])) %>% 
      dplyr::select(year,Phase,Agreed=IMF_ALL_agreed_GDP,Drawn=IMF_ALL_drawn_GDP,diff) %>%
      arrange(-Agreed)
    
    #list of countries with IMF programs by years and Agreed/drawn
    output$size_IMF_program=renderTable(IMF_programs_size)
    
    dt=mydata_q %>%  filter((IMF_ALL_agreed_GDP>input$min_size_imfprogram[1] & IMF_ALL_agreed_GDP<input$min_size_imfprogram[2]) & !is.na(Phase_4)) %>%
      mutate(IMF_program=ifelse((IMF_ALL_agreed_GDP>input$min_size_imfprogram[1] & IMF_ALL_agreed_GDP<input$min_size_imfprogram[2]),1,0))
    
    #bar plot with IMF program in each phase of the cycle
    output$SDR_cycle=renderPlotly({
      ggplotly(ggplot(dt)+
        geom_bar(aes(x=Phase_4))+
        theme_bw()
    )})
  
  })
 
  
  # Proportion of liquidity and solvency crisis that end/start in default 
  bucket=0:20/20
  pure.liq.crisis=lapply(bucket,function(x){
    a=Prob.Type.IMF.program(mydata_q,t.liquid.crisis = x,t.solv.crisis = 0.99)
    a$Proba[c("IMF.pure.liq.crisis")]
  })
  names(pure.liq.crisis)=bucket
  pure.liq.crisis=do.call(rbind,pure.liq.crisis)
  
  pure.solv.crisis=lapply(bucket,function(x){
    a=Prob.Type.IMF.program(mydata_q,t.liquid.crisis = 0.1,t.solv.crisis = x)
    a$Proba[c("IMF.pure.solv.crisis")]
  })
  names(pure.solv.crisis)=bucket
  pure.solv.crisis=do.call(rbind,pure.solv.crisis)
  
  cum.distrib.type.IMFprograms=cbind(pure.liq.crisis,pure.solv.crisis)
  
  #Figure showing the distribution of IMF programs in type of crisis depending on the thresholds
  output$Plot_cumDistrib_IMFprograms=renderPlotly({
    ggplot()+
      geom_line(aes(x=as.numeric(rownames(cum.distrib.type.IMFprograms)),y=cum.distrib.type.IMFprograms$IMF.pure.solv.crisis,color="Pure solv crisis"))+
      geom_line(aes(x=as.numeric(rownames(cum.distrib.type.IMFprograms)),y=cum.distrib.type.IMFprograms$IMF.pure.liq.crisis,color="Pure liquidity crisis"))+
      geom_vline(xintercept = 0.1,color="red",linetype="dotted")+
      geom_vline(xintercept = 0.9,color="lightblue",linetype="dotted")+
      scale_color_discrete(name=NULL)+
      theme_bw()+
      theme(legend.position = "bottom")+
      labs(x="% difference between amount agreed and drawn",
           y="% of episodes",
           title="Sensitivity of type of programs to threshold")
  })
  
  t.liquid.crisis=0.1
  t.solv.crisis=0.9
  mydata_q=mydata_q %>% mutate(IMF.pure.liq.crisis=ifelse(IMF_All_diff<=t.liquid.crisis,1,0),
                               IMF.Non.pure.liq.crisis=ifelse(IMF_All_diff>t.liquid.crisis,1,0),
                               IMF.pure.solv.crisis=ifelse(IMF_All_diff>=t.solv.crisis,1,0),
                               IMF.Non.pure.solv.crisis=ifelse(IMF_All_diff<t.solv.crisis,1,0),
                               IMF.liq.crisis=ifelse(IMF_All_diff<=0.5,1,0),
                               IMF.solv.crisis=ifelse(IMF_All_diff>0.5,1,0))
  
  pure.solv.crisis_default=mydata_q %>% filter(IMF.pure.solv.crisis==1) %>% 
    summarize(SD_0_1yAft.BM=mean(SD_0_1yAft.BM,na.rm=T)*100,
              SD_0_2yAft.BM=mean(SD_0_2yAft.BM,na.rm=T)*100,
              SD_0_3yAft.BM=mean(SD_0_3yAft.BM,na.rm=T)*100,
              SD_0_4yAft.BM=mean(SD_0_4yAft.BM,na.rm=T)*100,
              SD_0_1yBef.BM=mean(SD_0_1yBef.BM,na.rm=T)*100,
              SD_0_2yBef.BM=mean(SD_0_2yBef.BM,na.rm=T)*100,
              SD_0_3yBef.BM=mean(SD_0_3yBef.BM,na.rm=T)*100,
              SD_0_4yBef.BM=mean(SD_0_4yBef.BM,na.rm=T)*100,
              SD_0_4yBefAft.BM=mean(SD_0_4yBefAft.BM,na.rm=T)*100)
  
  pure.liq.crisis_default=mydata_q %>% filter(IMF.pure.liq.crisis==1) %>% 
    summarize(SD_0_1yAft.BM=mean(SD_0_1yAft.BM,na.rm=T)*100,
              SD_0_2yAft.BM=mean(SD_0_2yAft.BM,na.rm=T)*100,
              SD_0_3yAft.BM=mean(SD_0_3yAft.BM,na.rm=T)*100,
              SD_0_4yAft.BM=mean(SD_0_4yAft.BM,na.rm=T)*100,
              SD_0_1yBef.BM=mean(SD_0_1yBef.BM,na.rm=T)*100,
              SD_0_2yBef.BM=mean(SD_0_2yBef.BM,na.rm=T)*100,
              SD_0_3yBef.BM=mean(SD_0_3yBef.BM,na.rm=T)*100,
              SD_0_4yBef.BM=mean(SD_0_4yBef.BM,na.rm=T)*100,
              SD_0_4yBefAft.BM=mean(SD_0_4yBefAft.BM,na.rm=T)*100)
  
  pure.liqSolv.crisis_default=cbind(t(pure.solv.crisis_default),t(pure.liq.crisis_default))
  colnames(pure.liqSolv.crisis_default)=c("pure.solv.crisis","pure.liq.crisis")
  names=rownames(pure.liqSolv.crisis_default)
  
  pure.liqSolv.crisis_default=data.frame(pure.liqSolv.crisis_default) %>% mutate(diff=pure.solv.crisis-pure.liq.crisis) %>% round(.,0)
  rownames(pure.liqSolv.crisis_default)=c("Prob. default 0 and 1 year after the program",
                                          "Prob. default 0 and 2 year after the program",
                                          "Prob. default 0 and 3 year after the program",
                                          "Prob. default 0 and 4 year after the program",
                                          "Prob. default 0 and 1 year before the program",
                                          "Prob. default 0 and 2 year before the program",
                                          "Prob. default 0 and 3 year before the program",
                                          "Prob. default 0 and 4 year before the program",
                                          "Prob. default 0 and 4 year before or after the program")
  colnames(pure.liqSolv.crisis_default)=c("Pure solvency","Pure liquidity","Difference")
    
  output$Type_IMFprogram_defaults=renderTable({pure.liqSolv.crisis_default},rownames = T)
  
  ### time series of number and Agreed/drawn IMF programs
  
  IMFcompare_dt=IMFcompare_dt %>% mutate(year=year(Period),quarter=quarter(Period))
  dt_quotas=mydata_q %>% dplyr::select(ISO3_Code,year,quarter,IMF_quotas)
  
  IMFcompare_dt=merge(x=IMFcompare_dt,y=dt_quotas,by=c("ISO3_Code","year","quarter"),all.x=T)
  IMFcompare_dt=IMFcompare_dt %>% mutate(Amount_agreed_quotas=(Amount_agreed*1000/(IMF_quotas))*100,
                                         Amount_drawn_quotas=(Amount_drawn*1000/(IMF_quotas))*100)
  
  dt=IMFcompare_dt %>% 
    mutate(year=year(Period)) %>% 
    group_by(year) %>% 
    summarize(Agreed=sum(Amount_agreed,na.rm=T),
              Drawn=sum(Amount_drawn,na.rm=T),
              Agreed_quotas=sum(Amount_agreed_quotas,na.rm=T),
              Drawn_quotas=sum(Amount_drawn_quotas,na.rm=T),
              N.obs=n())
  
  output$TS_size_IMF_progs=renderPlotly({
    ggplotly(ggplot(dt)+
      geom_bar(stat="identity",aes(x=year,y=Agreed,fill="Agreed"))+
      geom_bar(stat="identity",aes(x=year,y=Drawn,fill="Drawn"))+
        #geom_line(aes(x=year,y=N.obs*mean(IMF_ALL_agreed_GDP)/5,color="N. programs"))+
        theme_bw()+
        scale_y_continuous(sec.axis = sec_axis(~./2906052,name="Number of programs"))+
        scale_fill_discrete(name=NULL)+
        scale_color_manual(values="black",name=NULL)+
        labs(y="SRDs",
             x=NULL,
             title="Evolution of IMF programs")+
      theme(legend.position = "bottom")
  )})
  
  
  output$TS_size_IMF_progs_quotas=renderPlotly({
    ggplotly(ggplot(dt)+
               geom_bar(stat="identity",aes(x=year,y=Agreed_quotas,fill="Agreed"))+
               geom_bar(stat="identity",aes(x=year,y=Drawn_quotas,fill="Drawn"))+
               #geom_line(aes(x=year,y=N.obs*mean(IMF_ALL_agreed_GDP)/5,color="N. programs"))+
               theme_bw()+
               #scale_y_continuous(sec.axis = sec_axis(~./2906052,name="Number of programs"))+
               scale_fill_discrete(name=NULL)+
               scale_color_manual(values="black",name=NULL)+
               labs(y="% quotas",
                    x=NULL,
                    title="Evolution of IMF programs")+
               theme(legend.position = "bottom")
    )})
  
  
  #Profile IMF programs: figure of duration by type of program and characteristic of programs by type
  
  IMFcompare_dt=IMFcompare_dt %>% mutate(year=year(Period),
                                         diff=(1-(Amount_agreed-Amount_drawn)/Amount_agreed),
                                         pure.liq=ifelse(diff<=t.liquid.crisis,1,0),
                                         non.pure.liq=ifelse(diff>t.liquid.crisis,1,0),
                                         pure.solv=ifelse(diff>=t.solv.crisis,1,0),
                                         duration=as.numeric(year(as.Date(Date_Expiration)))-as.numeric(year(as.Date(Period))),
                                         name.variable=substring(variable,4,6))
  
 
  output$Hist_durationIMF=renderPlotly({
    ggplotly(
      ggplot(IMFcompare_dt)+
        geom_histogram(aes(duration,fill=name.variable),binwidth = 0.5)+
        scale_fill_discrete(name=NULL)+
        scale_x_continuous(breaks=0:6)+
        theme_bw()+
        theme(legend.position="bottom")+
        labs(y="Number of programs",
             x="Years",
             title="Number of programs by duration")
    )
  })
  
  IMFcompare=IMFcompare_dt %>% group_by(variable) %>% summarize(Agreed=sum(Amount_agreed,na.rm=T),
                                                                Drawn=sum(Amount_drawn,na.rm=T),
                                                                Agreed_quotas=mean(Amount_agreed_quotas,na.rm=T),
                                                                Drawn_quotas=mean(Amount_drawn_quotas,na.rm=T),
                                                                diff=mean(diff,na.rm=T)*100,
                                                                duration=mean(duration,na.rm=T),
                                                                Sup.Rfacilities=sum(Sup_reserves_facilities,na.rm=T),
                                                                pure.liq=mean(pure.liq,na.rm=T)*100,
                                                                pure.solv=mean(pure.solv,na.rm=T)*100,
                                                                non.pure.liq=mean(non.pure.liq,na.rm=T)*100,
                                                                n.programs=n()) %>% mutate(variable=substr(variable,4,6))
  
  
  output$fig_IMF_diff_TS=renderPlotly({
    ggplotly(ggplot(data=IMFcompare_dt %>% filter(year>=1984)) +
    geom_rect(aes(xmin=1984,xmax=2020,ymin=0,ymax=0.1),fill="lightgreen",alpha=0.3)+
    geom_rect(aes(xmin=1984,xmax=2020,ymin=0.9,ymax=1),fill="darkred",alpha=0.3)+
    geom_rect(aes(xmin=1984,xmax=2020,ymin=0.1,ymax=0.9),fill="red",alpha=0.1)+
    geom_point(aes(x=year,y=diff,color=ISO3_Code,size=Amount_agreed_quotas))+
    annotate("text",x=1984,y=0.05,label="Expectation driven",size=2)+
    annotate("text",x=1984,y=0.95,label="Pure fundamental driven",size=2)+
    annotate("text",x=1984,y=0.5,label="Fundamental driven ",size=2)+
    theme_bw()+
    labs(x=NULL,y="% of program drawn")+
    theme(legend.position = "none",axis.title.y=element_text(size=6))+
    lims(y=c(0,1))
    )
  })
  
  observe({  
    var=input$var_Barchart_characteristics_IMFprograms
    output$Barchart_characteristics_IMFprograms=renderPlotly({
      ggplotly(
        ggplot(IMFcompare)+
          geom_bar(stat="identity",aes(x=variable,y=get(var)),fill="darkblue")+
          theme(legend.position="bottom")+
          theme_bw()+
          labs(y=var,x=NULL,
               title="Characteristics of program by type")
      )
    })
    
  })
  
  # Figures distribution of variables depending on the type of crisis (liq,solv,pure.liq....)
  load("data/Figures_IMFprogram_vars.RData")
  list_plots_IMF=list(After_programs=After_programs,
                      before_programs=before_programs)
  
  # Table with averages of variables depending on the type of crisis (liq,solv,pure.liq....)
 load("data/Table_average_IMFprogram_vars.RData")
 list_tables_IMF=list(After_programs=After_programs,
                     before_programs=before_programs)
 
 # list_tables_IMF[["After_programs"]]=data.frame(list_tables_IMF$After_programs) %>% dplyr::select(-c("FR","SD"))
 # list_tables_IMF[["before_programs"]]=data.frame(list_tables_IMF$before_programs) %>% dplyr::select(-c("FR","SD"))
 # 
 # 
 # rownames(list_tables_IMF$before_programs)=c("Pure liquidity","Pure solvency",
 #                                             "Non pure liquidity")
 #  
 # colnames(list_tables_IMF$before_programs)=c("P[B=1]","P[Large B=1]","g (%)",
 #                                            "Gross Gov. Debt (% GDP)","diff in Gross Gov. Debt (%)",
 #                                            "P[SD=1]",
 #                                            "P[CC=1]","d.Exch.Rate (CC) (%)",
 #                                            "P[BC=1]","Fisc.Cost Bank Crisis (BC) (% GDP)",
 #                                            "P[SMC=1]","d.Stock Market (SMC) (%)",
 #                                            "N.Obs")
 #allow user to switch tables and figures for before and after the program
  observe({
    
    if(input$moment_program=="2 years preceeding the program"){
      moment_program="before_programs"
    }else if(input$moment_program=="2 years following the program"){
      moment_program="After_programs"
    }
    
    var_program=input$var_program
    
    output$densities_IMF_vars=renderPlotly(list_plots_IMF[[moment_program]][[var_program]])
    #output$table_IMF_vars=renderTable(datatable(list_tables_IMF[[moment_program]],rownames = T,options = list(dom = 't')))
    output$table_IMF_vars=renderTable(list_tables_IMF[[moment_program]],rownames = T,striped = T,hover=T)
  })
  
      ## [COMMENTED] Business cycles and crisis  ####
  # observe({
  #   dt=mydata_q %>%  filter(eval(parse(text=input$idvarbarplot_cycle_crisis))==1)
  #   
  #   output$Barplot_cycle_crisis=renderPlotly({
  #     ggplotly(ggplot(dt)+
  #       geom_bar(aes(x=Phase_4))+
  #       ylim(c(0,40))+
  #       theme_bw()
  #   )})
  #   
  #   output$plotdates_cycle_crisis <- renderPlotly({
  #     ggplotly(show_defaults2(dt,input$idvarbarplot_cycle_crisis)
  #   )})
  #   
  # })
  
      ## [COMMENTED] Sovereign default events    ####
  
  # output$plotdates_default <- renderPlotly({
  #   ggplotly(show_defaults2(mydata_q,input$idCrisis)
  # )})
  # 
  # output$table_defaults <- renderTable({
  #   default_table(mydata_q,input$idCrisis)
  # })
  # 
      ## [COMMENTED] Business cycles and default ####
  ctries=unique(mydata_q$ISO3_Code)
  
  # output$select_ctry=renderUI(
  #   selectInput(inputId="ctry",label="select country",
  #               choices=ctries)
  # )
  # 
  # output$plot_Cycles <- renderPlotly({
  #   ggplotly(plot_IMF(mydata_q,input$ctry,input$idCrisis)
  # )})
  
      ## [COMMENTED] Business cycles and Twin Ds ####
  # output$plot_all_crisis <- renderPlotly({
  #   ggplotly(plot_all_crisis(mydata_q,input$idctry_TwinDs,input$idSD_crisis_TwinDs,"CC.MB_first","BC.LV")
  # )})
  
      ## Sovereign debt default                  ####
  
  observe({
    var=input$idvar_arrears
    
    dt_arrears=mydata_q %>%
      filter(ISO3_Code==input$idCtry_arrears) %>%
      group_by(year) %>%
      summarize(arrears=sum(eval(parse(text=var)),na.rm=T),
                debt=mean(Gov.Gross.debt.HPDD,na.rm=T))
    
    output$TS_debt_default=renderPlotly({
      ggplotly(ggplot(dt_arrears,aes(x=year,y=arrears))+
        geom_bar(stat='identity') +
        xlab(NULL)+
        ylab("Debt in default")+
        theme_bw()
    )})
    
    output$TS_debt=renderPlotly({
      ggplotly(ggplot(dt_arrears)+
        geom_line(aes(x=year,y=debt)) +
        theme_bw()
    )})
    
  })
  
      ## Exchange rate and crisis                ####
  
  observe({
    dt=mydata_q %>% 
      mutate(BC_PeakNPL.LV=ifelse(is.na(BC_PeakNPL.LV),0,BC_FiscalCost_percGDP.LV),
             Intensity_CC.MB=d.XR*CC.MB_first) %>%
      filter(ISO3_Code==input$idCtry_Intensity_CC)
    
    var=input$idSD_Intensity_CC
    var2="BC.LV"
    SD=(dt %>% filter(eval(parse(text=var))>0) %>% dplyr::select(Period))$Period
    BC=(dt %>% filter(eval(parse(text=var2))>0) %>% dplyr::select(Period))$Period
    
    output$Plot_Intensity_CC=renderPlotly({
      ggplotly(ggplot(dt)+
        geom_bar(stat="identity",aes(x = Period,y=d.XR),fill="darkgreen")+
        geom_bar(stat="identity",aes(x = Period,y=Intensity_CC.MB),fill="darkred")+
        geom_vline(xintercept = as.numeric(SD),linetype="dotted",color="red")+
        geom_vline(xintercept = as.numeric(BC),linetype="dashed",color="blue")+
        theme_bw()+ theme(legend.position="none")
    )})
    
    var3="CC.MB"
    varMacro=input$idMacrovar_crisis_CC
    SD=(dt %>% filter(eval(parse(text=var))>0) %>% dplyr::select(Period))$Period
    BC=(dt %>% filter(eval(parse(text=var2))>0) %>% dplyr::select(Period))$Period
    CC=(dt %>% filter(eval(parse(text=var3))>0) %>% dplyr::select(Period))$Period
    
    output$Plot_Macrovar_crisis_CC=renderPlotly({
      ggplotly(ggplot(dt)+
        geom_line(stat="identity",aes(x = Period,y=eval(parse(text=varMacro))),fill="darkred")+
        geom_vline(xintercept = as.numeric(SD),linetype="dotted",color="red")+
        geom_vline(xintercept = as.numeric(BC),linetype="dashed",color="blue")+
        geom_vline(xintercept = as.numeric(CC),linetype="dashed",color="green")+
        theme_bw()+ theme(legend.position="none")
    )})
    
  })
  
      ## What is the story                       ####
  
  observe({
    dt=mydata_q %>% 
      mutate(BC_PeakNPL.LV=ifelse(is.na(BC_PeakNPL.LV),0,BC_FiscalCost_percGDP.LV),
             Intensity_SMC.MB=d.Stock.market.index*Stock.market.index_Phase_B) %>%
      filter(ISO3_Code==input$idCtry_Intensity_SMC)

    
    output$Plot_Intensity_SMC=renderPlotly({
      ggplotly(ggplot(dt)+
        geom_bar(stat="identity",aes(x = Period,y=d.Stock.market.index),fill="darkgreen")+
         geom_bar(stat="identity",aes(x = Period,y=Intensity_SMC.MB),fill="darkred")+
        geom_vline(xintercept = as.numeric(SD),linetype="dotted",color="red")+
        geom_vline(xintercept = as.numeric(BC),linetype="dashed",color="blue")+
        theme_bw()+ theme(legend.position="none")
    )})
    
    myvar=input$Figswitch
    lim_value=2
    dt=mydata_q %>% 
      mutate(`GDP cycle`=GDPV_XDC_cycle/4,
             `S. Market index`=d.Stock.market.index,
             `S. Market crisis`=SMC.MB*d.Stock.market.index,
             `Banking crisis`=BC_FiscalCost_percGDP.LV/100,
             `F. Reserves`=d.RAFA_USD,
             `F. Reserves crisis`=d.RAFA_USD*FRC.MB,
             `Exch. Rate`=d.XR,
             `Exch. Rate crisis`=d.XR*CC.MB_first,
             `Agreed IMF program`=IMF_ALL_agreed_GDP/100,
             `Drawn IMF program`=IMF_ALL_drawn_GDP/100,
             `Gov. debt to GDP`=Gov.Gross.debt.HPDD/100,
             `Debt in default`=SD.debt.Tot.BM/100000,
             d.debt=lag(log(Gov.Gross.debt.HPDD)-lag(log(Gov.Gross.debt.HPDD),1),3)#,
             #d.debt=ifelse(quarter==4,d.debt,NA)
             ) %>%
      filter(ISO3_Code==input$idCtry_Intensity_SMC & (year>1960 & year<2018))
  
    var="SMC.MB"
    var2="SD_Tot.BM"
    SMC=(dt %>% filter(eval(parse(text=var))>0) %>% dplyr::select(Period))$Period
    SD=(dt %>% filter(eval(parse(text=var2))>0) %>% dplyr::select(Period))$Period
    
    
    if(myvar=="Economic Activity"){
      output$Fig_Crisis_Intensity=renderPlotly({
        ggplotly( ggplot(dt)+
          geom_line(stat="identity",aes(x = Period,y=`GDP cycle`),color="darkgrey")+
          #scale_y_continuous(sec.axis =dup_axis(~.*15))+
          scale_x_date(breaks = seq(as.Date("1960-01-01"), as.Date("2019-12-31"), by="1 years"),date_labels =  "%Y")+
          theme_bw()+ 
          labs(x=NULL,y=NULL)+
          lims(y=c(-lim_value,lim_value))+
          theme(legend.position="bottom",axis.text.x = element_text(angle=90, hjust=1),axis.text.y = element_blank())
      )})
    }else if(myvar=="Stock Market performance"){
      output$Fig_Crisis_Intensity=renderPlotly({
        ggplotly( ggplot(dt)+
          geom_bar(stat="identity",aes(x = Period,y=`S. Market index`),fill="lightgrey",color="darkgrey")+
         #geom_line(stat="identity",aes(x = Period,y=Intensity_SMC.MB/100*6),color="darkgrey")+
          geom_line(stat="identity",aes(x = Period,y=`GDP cycle`),color="darkgrey")+
          scale_x_date(breaks = seq(as.Date("1960-01-01"), as.Date("2019-12-31"), by="1 years"),date_labels =  "%Y")+
          theme_bw()+ 
          labs(x=NULL,y=NULL)+
          lims(y=c(-lim_value,lim_value))+
          theme(legend.position="bottom")+
          theme(legend.position="bottom",axis.text.x = element_text(angle=90, hjust=1))
      )})
    }else if(myvar=="Stock Market Crash"){
      output$Fig_Crisis_Intensity=renderPlotly({
      ggplotly(ggplot(dt)+
          geom_bar(stat="identity",aes(x = Period,y=`S. Market index`),fill="lightgrey",color="darkgrey")+
          geom_bar(stat="identity",aes(x = Period,y=`S. Market crisis`),fill="red")+
          geom_line(stat="identity",aes(x = Period,y=`GDP cycle`),color="darkgrey")+
          scale_x_date(breaks = seq(as.Date("1960-01-01"), as.Date("2019-12-31"), by="1 years"),date_labels =  "%Y")+
          theme_bw()+ 
          labs(x=NULL,y=NULL)+
          lims(y=c(-lim_value,lim_value))+
          theme(legend.position="bottom")+
          theme(legend.position="bottom",axis.text.x = element_text(angle=90, hjust=1))
      )})
    } else if(myvar=="...Turning point?"){
      output$Fig_Crisis_Intensity=renderPlotly({
        ggplotly(ggplot(dt)+
          geom_bar(stat="identity",aes(x = Period,y=`S. Market crisis`),fill="red")+
          geom_line(stat="identity",aes(x = Period,y=`GDP cycle`),color="darkgrey")+
          scale_x_date(breaks = seq(as.Date("1960-01-01"), as.Date("2019-12-31"), by="1 years"),date_labels =  "%Y")+
          geom_vline(xintercept = as.numeric(SMC),linetype="dashed",color="red")+
          theme_bw()+ 
          labs(x=NULL,y=NULL)+
          lims(y=c(-lim_value,lim_value))+
          theme(legend.position="bottom")+
          theme(legend.position="bottom",axis.text.x = element_text(angle=90, hjust=1))
      )})
    } else if(myvar=="Banking Crisis"){
      output$Fig_Crisis_Intensity=renderPlotly({
      ggplotly(ggplot(dt)+
          geom_bar(stat="identity",aes(x = Period,y=`S. Market crisis`),fill="red")+
          geom_bar(stat="identity",aes(x = Period,y=`Banking crisis`),fill="blue")+
          geom_line(stat="identity",aes(x = Period,y=`GDP cycle`),color="darkgrey")+
          scale_x_date(breaks = seq(as.Date("1960-01-01"), as.Date("2019-12-31"), by="1 years"),date_labels =  "%Y")+
          theme_bw()+ 
          labs(x=NULL,y=NULL)+
          lims(y=c(-lim_value,lim_value))+
          theme(legend.position="bottom")+
          theme(legend.position="bottom",axis.text.x = element_text(angle=90, hjust=1))
     )})
    } else if(myvar=="Foreign Reserves crisis"){
      output$Fig_Crisis_Intensity=renderPlotly({
        ggplotly( ggplot(dt)+
          geom_bar(stat="identity",aes(x = Period,y=`S. Market crisis`),fill="red")+
          geom_bar(stat="identity",aes(x = Period,y=`Banking crisis`),fill="blue")+
          geom_bar(stat="identity",aes(x = Period,y=`F. Reserves`),fill="lightgrey")+
          geom_bar(stat="identity",aes(x = Period,y=`F. Reserves crisis`),fill="black")+
          geom_line(stat="identity",aes(x = Period,y=`GDP cycle`),color="darkgrey")+
          scale_x_date(breaks = seq(as.Date("1960-01-01"), as.Date("2019-12-31"), by="1 years"),date_labels =  "%Y")+
          theme_bw()+ 
          labs(x=NULL,y=NULL)+
          lims(y=c(-lim_value,lim_value))+
          theme(legend.position="bottom")+
          theme(legend.position="bottom",axis.text.x = element_text(angle=90, hjust=1))
      )})
    }else if(myvar=="Exchange rate crisis"){
      output$Fig_Crisis_Intensity=renderPlotly({
        ggplotly(ggplot(dt)+
          geom_bar(stat="identity",aes(x = Period,y=`S. Market crisis`),fill="red")+
          geom_bar(stat="identity",aes(x = Period,y=`Banking crisis`),fill="blue")+
          geom_bar(stat="identity",aes(x = Period,y=`Exch. Rate`),fill="lightgrey",color="darkgrey")+
          geom_bar(stat="identity",aes(x = Period,y=`Exch. Rate crisis`),fill="darkred")+
          geom_line(stat="identity",aes(x = Period,y=`GDP cycle`),color="darkgrey")+
          scale_x_date(breaks = seq(as.Date("1960-01-01"), as.Date("2019-12-31"), by="1 years"),date_labels =  "%Y")+
          theme_bw()+ 
          labs(x=NULL,y=NULL)+
          lims(y=c(-lim_value,lim_value))+
          theme(legend.position="bottom")+
          theme(legend.position="bottom",axis.text.x = element_text(angle=90, hjust=1))
      )})
    }else if(myvar=="Agreed IMF programs"){
      output$Fig_Crisis_Intensity=renderPlotly({
        ggplotly(ggplot(dt)+
          geom_bar(stat="identity",aes(x = Period,y=`S. Market crisis`),fill="red")+
          geom_bar(stat="identity",aes(x = Period,y=`Banking crisis`),fill="blue")+
          geom_bar(stat="identity",aes(x = Period,y=`Exch. Rate crisis`),fill="darkred")+
          geom_line(stat="identity",aes(x = Period,y=`GDP cycle`),color="darkgrey")+
          geom_bar(stat="identity",aes(x = Period,y=`Agreed IMF program`),fill="lightgreen")+
          scale_x_date(breaks = seq(as.Date("1960-01-01"), as.Date("2019-12-31"), by="1 years"),date_labels =  "%Y")+
          theme_bw()+ 
          labs(x=NULL,y=NULL)+
          lims(y=c(-lim_value,lim_value))+
          theme(legend.position="bottom")+
          theme(legend.position="bottom",axis.text.x = element_text(angle=90, hjust=1))
      )})
    }else if(myvar=="Drawn IMF programs"){
      output$Fig_Crisis_Intensity=renderPlotly({
        ggplotly(ggplot(dt)+
          geom_bar(stat="identity",aes(x = Period,y=`S. Market crisis`),fill="red")+
          geom_bar(stat="identity",aes(x = Period,y=`Banking crisis`),fill="blue")+
          geom_bar(stat="identity",aes(x = Period,y=`Exch. Rate crisis`),fill="darkred")+
          geom_bar(stat="identity",aes(x = Period,y=`Agreed IMF program`),fill="lightgreen")+
          geom_bar(stat="identity",aes(x = Period,y=`Drawn IMF program`),fill="darkgreen")+
          geom_line(stat="identity",aes(x = Period,y=`GDP cycle`),color="darkgrey")+
          scale_x_date(breaks = seq(as.Date("1960-01-01"), as.Date("2019-12-31"), by="1 years"),date_labels =  "%Y")+
          theme_bw()+ 
          labs(x=NULL,y=NULL)+
          lims(y=c(-lim_value,lim_value))+
          theme(legend.position="bottom")+
          theme(legend.position="bottom",axis.text.x = element_text(angle=90, hjust=1))
      )})
    }else if(myvar=="...Sovereign default"){
      output$Fig_Crisis_Intensity=renderPlotly({
        ggplotly(ggplot(dt)+
          geom_bar(stat="identity",aes(x = Period,y=`S. Market crisis`),fill="red")+
          geom_bar(stat="identity",aes(x = Period,y=`Banking crisis`),fill="blue")+
          geom_bar(stat="identity",aes(x = Period,y=`Exch. Rate crisis`),fill="darkred")+
          geom_bar(stat="identity",aes(x = Period,y=`Agreed IMF program`),fill="lightgreen")+
          geom_bar(stat="identity",aes(x = Period,y=`Drawn IMF program`),fill="darkgreen")+
         # geom_line(stat="identity",aes(x = Period,y=d.debt),color="blue",linetype="dotted")+
          geom_line(stat="identity",aes(x = Period,y=`GDP cycle`),color="darkgrey")+
          geom_vline(xintercept = as.numeric(SD),linetype="dashed",color="black")+
          scale_x_date(breaks = seq(as.Date("1960-01-01"), as.Date("2019-12-31"), by="1 years"),date_labels =  "%Y")+
          theme_bw()+ 
          labs(x=NULL,y=NULL)+
          lims(y=c(-lim_value,lim_value))+
          theme(legend.position="bottom")+
          theme(legend.position="bottom",axis.text.x = element_text(angle=90, hjust=1))
      )})
    }else if(myvar=="...Government debt and debt in default"){
      output$Fig_Crisis_Intensity=renderPlotly({
       ggplotly(ggplot(dt)+
        
          #geom_bar(stat="identity",aes(x = Period,y=BC_FiscalCost_percGDP.LV/100),fill="blue")+
          #geom_bar(stat="identity",aes(x = Period,y=Intensity_CC.MB),fill="darkred")+
          #geom_bar(stat="identity",aes(x = Period,y=IMF_ALL_agreed_GDP/100),fill="lightgreen")+
          #geom_bar(stat="identity",aes(x = Period,y=IMF_ALL_drawn_GDP/100),fill="darkgreen")+
          geom_line(stat="identity",aes(x = Period,y=`Gov. debt to GDP`),color="darkblue",linetype="dashed")+
         #geom_line(stat="identity",aes(x = Period,y=GDPV_XDC_cycle/10),color="darkgrey")+
          geom_bar(stat="identity",aes(x = Period,y=`Debt in default`),fill="darkgreen")+
       #  geom_vline(xintercept = SD,linetype="dashed",color="black")+
          scale_x_date(breaks = seq(as.Date("1960-01-01"), as.Date("2019-12-31"), by="1 years"),date_labels =  "%Y")+
          theme_bw()+ 
          labs(x=NULL,y=NULL)+
          lims(y=c(-lim_value,lim_value))+
          theme(legend.position="bottom")+
          theme(legend.position="bottom",axis.text.x = element_text(angle=90, hjust=1))
      )})
    }
   
  })#end observe
   
      ## Dispo periods of variables              ####
  
  vars=c("Phase_A","XR_rate_BIS","BC_FiscalCost_percGDP.LV","IMF_ALL_agreed_dummy","Stock.market.index","Gov.Gross.debt.HPDD")
  sample_full=table_dispo_periods_generic(mydata_q,vars)
  sample_full=merge(x=sample_full,y=ctry_classif(),by=c("ISO3_Code"),all.x=T)
  sample_full = sample_full %>% arrange(Classification,ISO3_Code) %>% dplyr::select(ISO3_Code,Classification,everything())
  
  sample=sample_full %>% filter(!is.na(Stock.market.index) | !is.na(Phase_A))
  rownames(sample)=1:dim(sample)[1]
  
  output$dispo_vars=renderTable(sample)
  
  # Definition of sovereign defaults: tabid_4 (ui code in fun.body.tabid_4.r)  #####
  # Business cycle: tabid_3 (ui code in fun.body.tabid_3".r)  ####
      ## Figures and stats of cycles             ####
  observe({
    #plots=plot.Cycles(mydata_q,"GDPV_XDC_cycle",input$idCountryCycle,"X5Y_cds")
    plots= plot.Cycles(mydata_q,"GDPV_XDC_cycle","MEX","X5Y_cds")
     output[["Cycles"]]=renderPlot({plots$Cycle})
     output[["Turning_points"]]=renderPlot({plots$`Turning points`})
     output[["PhaseA1.A2.B1.B2"]]=renderPlot({plots$`Phases A1.A1.B1.B2`})
     dt=mydata_q %>% filter(ISO3_Code==input$idCountryCycle)
     
     output[["GDP_original"]]=renderPlot({
       ggplot(dt)+
         geom_line(aes(x=Period,y=GDP_original))+
         theme_bw()
     })
     
    cycles_stat_ctry=data.frame(cycles_statistics) %>%
      filter(ISO3_Code==input$idCountryCycle) %>%
      dplyr::select(-c(ISO3_Code,Period,Index_cycle,duration.C,amplitude.C,quarter.amplitude.C))
    output$cycle_stat_ctry=renderTable(cycles_stat_ctry,width="auto",rownames = T)
    
  })
  
      ## Business cycles Sample                  ####
  ctries_Cycles=mydata_q  %>%
    group_by(ISO3_Code) %>%
    filter(!is.na(Phase_A)) %>%
    summarize(Period=paste0(first(year)," - ",last(year)))
  
  output$table_Available_BC <- renderTable({
    ctries_Cycles
  },rownames = T)
  
  # Macrovariables around peak  tabid2  (ui code in fun.body.Tabid_2.r)   #####
  
  
  observe({
  output$plot_CDSs_around_Peak <- renderPlot({
    Plot_list_var_around_Peak(mydata_q,input$idCDS,window=12)
  })
  })
  
  load("../Output/Graphs/Vars around peak/vars.around.Peak.RData")
  
  vars=c("d.XR","UNR","TTRADE","X5Y_cds","d.Stock.market.index","d.RAFA_USD","Gov.Gross.debt.HPDD","Gov.effectiveness","Gov.Primary.bal")
  lapply(vars,function(x){
    output[[paste0(x,"_aroundPeak")]]=renderPlot(var_around_peak[[x]])
  })
  
  # A bit of Econometrics                        #####
  # Bibliography: tabid_6 (ui code in fun.body.Tabid_6.r)            ####                                               #####
  output$biblio=renderTable(biblio)
  # Presentation:  Tabid_7  (ui code in Slides.Workshop.04-2019)     ####                                             #####
  
  if(run_presentation==T){

  #Slide 1: introduction
  output$diagram_crisis <- renderImage({
    return(list(
      src = "Input_Presentations/Diagram_crisis.png",
      filetype = "png",
      height = "80%",
      width = "75%",
      align="center"
    ))
  }, deleteFile = FALSE)
  
  Proba_crisis_table=mydata_q %>% 
    group_by(Phase_4) %>% 
    filter(!is.na(Phase_4)) %>% 
    summarise(`Stock Market Crash`=round(mean(SMC.RR_first*100,na.rm=T),2),
              `Stock Market Crash MB`=round(mean(SMC.MB*100,na.rm=T),2),
              `Currency crisis`=round(mean(CC.MB_first*100,na.rm=T),2),
              `IMF program`=round(mean(IMF_program*100,na.rm=T),2),
              `Banking crisis`=round(mean(BC.LV*100,na.rm=T),2),
              `Start Default`=round(mean(SD_Tot.BM_first*100,na.rm=T),2),
              `End Default`=round(mean(SD_E.RR_last*100,na.rm=T),2),
              `Expectation programs`=round(mean(IMF_pure_Expect_crisis*100,na.rm=T),2),
              `Liquidity programs`=round(mean(IMF_pure_liquid_crisis*100,na.rm=T),2))
  
  mydata_q=mydata_q %>% mutate(SD.CT=ifelse(SD.CT!=1,0,1))
  
  output$Prob_crisis_table=renderTable(Proba_crisis_table)
  
  #Slide 2: Measuring Business Cycles
  
  output$ctry_BC <- renderImage({
    return(list(
      src = "Input_Presentations/USA_BC4.png",
      filetype = "image/png",
      height = "70%",
      width = "65%"
    ))
  }, deleteFile = FALSE)
  
  #Slide 4: Business cycles and defaults
  
  Pres.ctries=c("ARG","THA","IDN","GRC","MEX","BRA","USA")
  output$Pres.select_ctry=renderUI(
    selectInput(inputId="Pres.ctry",label="Select country",
                choices=Pres.ctries,width="10%")
  )
  
  Pres.idCrisis=c('SD_Tot.BM_first','SD.CT','SD_E.RR',
                  'SD.RR_first',
                  
                  'SD_FCBonds.BM_first','SD_FCBankloans.BM_first','SD_LCdebt.BM_first',
                  
                  'IMF_EFF','IMF_SBA',
                  
                  'BC.LV',"CC.RR_first","CC.MB_first","SMC.RR_first")
  output$Pres.select_crisis=renderUI({
    selectInput(inputId="Pres.idCrisis",label="Select Crisis",
                choices=Pres.idCrisis,width="10%")})
  
  
  output$Pres.plot_Cycles <- renderPlot({
    plot_IMF(mydata_q,input$Pres.ctry,input$Pres.idCrisis)
  })
  
  #slide 5: Twin Ds
  
  output$Pres.select_ctry.twinD=renderUI({
    selectInput(inputId="Pres.ctry.twinD",label="Select country",
                choices=Pres.ctries,width="10%")}
  )
  
  output$Pres.select_crisis.twinD=renderUI({
    selectInput(inputId="Pres.idCrisis.twinD",label="Select Crisis",
                choices=Pres.idCrisis,width="10%")}
  )
  
  output$Pres.plot_all_crisis.twinD <- renderPlot({
    plot_all_crisis(mydata_q,input$Pres.ctry.twinD,input$Pres.idCrisis.twinD,"CC.MB_first","BC.LV")
  })
  
  
  #slide 6 : Heterogenous business cycles
  
  Pres.cycles_stat_ctry=data.frame(cycles_statistics) %>%
    filter(ISO3_Code=="MEX") %>% 
    mutate(duration.A=as.integer(duration.A),
           duration.B=as.integer(duration.B)) %>%
    dplyr::select(-c(ISO3_Code,Period,var_Trough_1,var_Trough_2,var_Peaks,Index_cycle,duration.C,amplitude.C,quarter.amplitude.C))
  output$Pres.cycle_stat_ctry=renderTable(Pres.cycles_stat_ctry,width="auto")
  
  #slide 7 and 8: variables around Peak
  
  vars_peaks=c("Gov.Gross.debt.HPDD",
               "Gov.Primary.bal",
               "sovrate",
               "d.XR",
               "CBGDPR",
               "TTRADE",
               "5Y_cds",
               "1Y_cds",
               "d.Stock.market.index")
  
  lapply(vars_peaks,function(x){
    output[[paste0("Peak_",x)]]<- renderImage({
      return(list(
        src = paste0("Input_Presentations/",x,"_aroundPeak.png"),
        filetype = "PNG",
        height = "100%",
        width = "100%"
      ))
    }, deleteFile = FALSE)
  })
  
}

}

##************************************************************************************************#### 
# Run app
##************************************************************************************************#### 

shinyApp(ui=ui, server=server)
