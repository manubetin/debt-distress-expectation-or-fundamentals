
slide.1.Introduction=function(){
  box(title="1. Introduction",width=12,id="Slide.1",class="SlideBox",
      collapsible = T,
      collapsed = T,
      h4(strong("- Research Question:")),
     h5("1. How can we empirically distinguish solvency problems (question of fundamentals) from liquidity problems (question of Roll-over)?"),
   #  h5("2. Once we identify solvency crisis and liquidity crises is there a way of distinguishing ex-ante the type of distress? "),
  # h5("3. Does the empirical findings match the theoretical framework regarding the conditions for debt defaults?"),
     hr(),
  #   br(),
   #   h4("- Debt distress vs debt default"),
   #    tags$ol(
   #      tags$li(strong("Debt distress:"),"Economic concept when tensions on debt arise (increasing cost of funding,loss of market access...) " ),
   #      tags$li(strong("Debt default:"),"Legal and Political concept when the government miss a payment and/or an agreement is found between
   #              creditors and the debtor country.")),
   #   br(),
   #    h4("- One concept does not necessarily imply the other"),
   #    tags$ol(
   #      tags$li("Debt distress without default (Colombia)"),
   #      tags$li("Default without debt distress (Ecuador in 2009)"),
   #      tags$li("Several debt distress episodes during same episode of default (Long restructing: Argentina 2000-2005)")
   #    ),
   #   hr(),
   # #  br(),
      h4("- Quantitative models with rollover-risk (Cole and Kehoe (2000),Chamon (2007),Cohen & Villemot (2015))"),
      h5("At each period countries compare value functions of defaulting or repaying and choose strategically given their current regime"),
      tags$ol(
        tags$li(strong("Tranquil regime (1):"),"no default single equilibrium regime"),
        tags$li(strong("Rollover risk regime (2):"),"Multiple equilibria regime with good state (2_G) (No default) and bad state (2_B) (default)"),
        tags$li(strong("Vulnerable regime (3):"),"default single equilibrium regime")),
    # # br(),
    #   h4("- Condition for a default: the country has to suffer a discontinuous jump in the debt to GDP ratio (Cohen and Villemot(2018))"),
    #  tags$ol(
    #    tags$li("A discontinuity in the denominator:","Series of negative output shocks (Countries default during bad times)"),
    #    tags$li("A discontinuity in the numerator:","Exchange rate or banking crisis")
    #    ),
    # # br(),
     h4("- Typology of crises"),
     tags$ol(
       tags$li(strong("Solvency crise:"),"Tranquil regime (1) -----> (3) vulnerable regime:"),
       tags$li(strong("Solvency and/or Liquidity crise:"),"Tranquil regime (1) -----> (2_B) Rollover regime in default:"),
       tags$li(strong('Avoided liquidity crise: "...whathever it takes":'),"Tranquil regime (1) -----> (2_A): Rollover regime with no default"),
       tags$li(strong("Pure liquidity crise:"),"Rollover regime with no default(2_G) ---> (2_B) Rollover regime with default:")
       )
  )
}

slide.2.data=function(){
  box(title="2. Data and Approach",width=12,id="Slide.3",class="SlideBox",
      collapsible = T,
      collapsed = T,
      
      # h5("- Cyclical component of quarterly nominal GDP (ADB from OECD and IFS from IMF): around 70 countries since 1860"),
      # tags$ol(
      #   tags$li("Hoddrick-Prescott filter"),
      #   tags$li("BBQ algorithm to find Peaks and Troughs of the series"),
      #   tags$li("Statistics of cycles: duration, amplitude,frequency...")
      # ),
      h5("- Days and characteristics of IMF programs: 184 member countries"),
      plotlyOutput("TS_size_IMF_progs"),
      br(),
      h5("- Estimated amount of debt in default by year (Beers and Mavalwalla):"),
      # tags$ol(
      #   tags$li("By type of creditors: Official, Paris Club, Other official, Private"),
      #   tags$li("By type of instruments: Foreign bonds, bank loans")
      # ),
      fluidRow(
        column(6,selectInput(inputId="idCtry_arrears",label="Select country",
                             choices=myctries,width="100%")),
        column(6,selectInput(inputId="idvar_arrears",label="Select type of default",
                             choices=c('SD.debt.Tot.BM','SD.debt.Tot.BM_GDP','SD.debt.IMF.BM',"SD.debt.OthersOffCred.BM","SD.debt.IBRD.BM",
                                       'SD.debt.PrivateCred.BM','SD.debt.ParisClub.BM',
                                       'SD.debt.FCBonds.BM','SD.debt.FCBankloans.BM',
                                       'SD.debt.LCdebt.BM','SD.debt.Tot.BM_GDP','SD.debt.Tot.BM_GDP','SD.debt.IMF.BM_GDP',
                                       "SD.debt.OthersOffCred.BM_GDP","SD.debt.IBRD.BM_GDP",
                                       'SD.debt.PrivateCred.BM_GDP','SD.debt.ParisClub.BM_GDP',
                                       'SD.debt.FCBonds.BM_GDP','SD.debt.FCBankloans.BM_GDP'
                             ),width="100%"))),
      plotlyOutput("TS_debt_default")
      # tags$ol(
      #   tags$li("Day of agreement and day of end of the program"),
      #   tags$li("Amount agreed"),
      #   tags$li("Amount drawn"),
      #   tags$li("Amount Outstanding")
      # ),
      #h5("- Quarterly exchange rate (BIS): All countries since 1980"),
      #h5("- Quarterly stock market index (ADB from OECD and IFS from IMF) around 70 countries since 1970"),
      #h5("- Estimated fiscal costs of systemic banking crisis (Laeven and Valencia): all countries")
  )
  
}

slide.3.definitions=function(){
  box(title="3. Definitions",width=12,id="Slide.3",class="SlideBox",
      collapsible = T,
      collapsed = T,
      h5("- ",strong("Downturn (Phase B)"),"=> Peaks to Trough of the cyclical component (hp filter) of nominal GDP"),
      h5("- ",strong("Expansion (Phase A)"),"=> Trough to Peak "),
      h5("- ",strong("Debt distress"),"=> Agreement of an IMF program"),
      tags$ol(
        tags$li(strong("Pure liquidity programs:"),"Drawn < 0.1*Agreed"),
        tags$li(strong("Pure Solvency programs:"),"Drawn > 0.9*Agreed"),
        tags$li(strong("Solvency programs:"),"Drawn > 0.1*Agreed")
        ),
      h5("-",strong("Debt default:"),"when debt service is not paid on the due date"),
      # h5("- ",strong("Start of default"),"first quarter of the first year year when debt in default is stricly positive"),
      # h5("- ",strong("End of default"),"first quarter of last year when debt in default is stricly positive"),
      h5("- ",strong("Currency crisis:"), "quarterly depreciation > 0.2"),
      h5("- ",strong("Systemic Banking crisis:"),"Fiscal cost of banking crise (Laeven and Valencia)")
      #h5("- ",strong("Stock Market crash:"),"quarter following the peak of the stock market index")
  )
  
}

slide.4.Example=function(){
  box(
    collapsible = T,
    collapsed = T,
    width=12,
    title="4. Illustration ",
    id="Slide.10",class="SlideBox",
    #plotlyOutput("CDS.solv"),
    plotlyOutput("CDS.solv.IMF"),
    #plotlyOutput("CDS.liq"),
    plotlyOutput("CDS.liq.IMF")
  )
}

slide.5.Results=function(){
  box(
    collapsible = T,
    collapsed = T,
    width=12,
    title="5. Descriptive Statistics",
    id="Slide.5",class="SlideBox",
    fluidRow(
    column(12,
           h4(strong("Proportion of crises by type")),
           tableOutput("IMF_prob_type_program"),
           hr()),
           # h4("Probability of default depends on the type of IMF program"),
           # br(),
           # tableOutput("Type_IMFprogram_defaults")),
    column(8,box(title="",plotlyOutput("Plot_cumDistrib_IMFprograms"),width=12,collapsed=T,collapsible=T),align="center")
    ),
    hr(),
    h4(strong("Profile of debt distress episodes and coincidence with other crisis")),  
    selectInput(inputId="moment_program",label="",
                  choices=c("2 years preceeding the program","2 years following the program")),
    tableOutput("table_IMF_vars"),
    br(),
    tags$ol(tags$li("Dowturn refers to the proportion of programs occuring in dowturn in the 2 years before (after) the program"),
            tags$li("Large Dowturn refers to the 10% of the downturns with largest amplitude (Peak to Trough in % of trend)"),
            tags$li("Gov. Gross Debt refers to the debt at the moment when the program has been agreed"),
            tags$li("Difference in Gov. Gross Debt refers to the difference between the debt at the moment of the program and the maximum debt 2 years before (after) the program"),
            tags$li("Sov. Default refers to the proportion of programs that involve a sovereign default in the 2 years before (after) the program as estimated by Beers and Malvalwalla (2017)"),
            tags$li("Avg qrt Exch. Rate Depreciation refers to the average percentage depreciation during the 2 years following (preceeding) the program"),
            tags$li("Currency Crisis refers to the proportion of programs that coincide with depreciation of more than 20% in the 2 years before (after) the agreement"),
            tags$li("Banking Crisis refers to the average fiscal cost of banking crisis (% GDP) as estimated by Laeven and Valencia (2017)")
            #tags$li("SMC refers to the average change in the stock market indexes (%)")
            #tags$li("FR refers to the average change in the foreign reserves (%)")
           ),
    selectInput(inputId="var_program",label="",
                choices=c("gdebt","d.gdebt","gdebt_max","CC","BC","SMC","FR"),width="100%"),
    plotlyOutput("densities_IMF_vars"),
    p("Notes:")#,
    
  )
}

slide.6.Conclusions=function(){
  box(
    collapsible = T,
    collapsed = T,
    width=12,
    title="6. Take away",
    id="Slide.10",class="SlideBox",
    h5("- Expectation driven crises represents",strong("15%"),"of debt distress episodes"),
    h5("-",strong("90%"),"of solvency debt distresses end in default versus",strong("70%")," of liquidity debt distresses"),
    #h5("- Amount of debt in default in liquidity 5 times b"),
    h5("- Solvency vs Liquidity: What happen 2 years before?"),
    tags$ol(
      tags$li("Probability of large recession lower:",strong("5% vs 40%")),
      tags$li("Initial debt and debt increase is lower:","Level:",strong("50% vs 80%"),",Increase:",strong("  +10% vs +12%")),
      tags$li("Probability and intensity of Exchange rate crise is lower:","Probability",strong("  15% vs 25%"),",Intensity:",strong("3% vs 6% depreciation per quarter")),
      tags$li("Probability of Banking crise is higher but its intensity higher:","Probability",strong("  6% vs 4%"),",Intensity:",strong("  7% vs 17% of GDP in bailouts"))
    )
   # h5("- Economic conditions after the crise:"),
   #  tags$ol(
   #   tags$li("Initial debt is lower:",strong(" 50% vs 80%")),
   #    tags$li("Ex-post debt increase is lower:",strong(" +10% vs +17%")),
   #   tags$li("Ex-post probability of Exchange rate crise is lower:",strong(" +5% vs +13%")),
   #    tags$li("Ex-post probability of Banking crise is higher:",strong(" +7% vs +3%")),
   #   tags$li("Ex-post Severity of Exchange rate depreciation is lower:",strong(" +2% vs 3.5% per quarter")),
   #    tags$li("Ex-post of Banking crisis is lower:",strong(" +5% vs 14% of GDP in bailouts"))
    #)
  )
}

fun.body.Pres04.2019=function(){
  fluidPage(
    div(id="header.presention",align="center",
        h1("Debt Distress and Sovereign Default: Liquidity or Solvency?"),
        h2("Manuel Betin"),
        h3("Macro Workshop"),
        h4("04-04-2019")
    ),
    slide.1.Introduction(),
    slide.2.data(),
    slide.3.definitions(),
    slide.4.Example(),
    slide.5.Results(),
    slide.6.Conclusions()
  )
}


# slide.6.Intuition=function(){
#   box(
#     collapsible = T,
#     collapsed = T,
#     width=12,
#     title="5. Intuition of Timing",
#     id="Slide.10",class="SlideBox",
#     h4(strong("My intuition:")),
#     plotOutput("diagram_crisis"),
#     tags$hr(),
#     h4(strong("What the data suggests:"),"probability of each type of crisis"),
#     tableOutput("Prob_crisis_table")
#   )
# }

# slide.2.Compute.BC=function(){
#   box(title="1. Measuring Economic Activity",width=12,id="Slide.2",class="SlideBox",
#       collapsible = T,
#       collapsed = T,
#       hr(),
#       h4("Notations"),
#       tags$ol(
#         tags$li(strong("Phase B:"),"Peak to Trough",
#                 tags$ol(
#                   tags$li(strong("B1"),": First half of downturn"),
#                   tags$li(strong("B2"),": Second half of downturn")
#                 )),
#         tags$li(strong("Phase A:"),"Trough to Peak",
#                 tags$ol(
#                   tags$li(strong("A1"),": First half of expansion"),
#                   tags$li(strong("A2"),": Second half of expanson")
#                 ))),
#       br(),
#       br(),
#       h4("The case of the US"),
#       imageOutput("ctry_BC"),
#       hr(),
#       p("Notes:"),
#       tags$ol(tags$li("Business cycles as the cyclical component of the HP filter on quarterly Real GDP"),
#               tags$li("43 countries: Emerging and Developped since 1980")))
# }
# 
# slide.3.defaults.def=function(){
#   box(title="2. Defining Sovereign Crisis",width=12,id="Slide.3",class="SlideBox",
#       collapsible = T,
#       collapsed = T,
#       h5(strong("The definition")),
#       tags$ol(
#         tags$li("Perimeters of the debt: Public, Publicly guaranteed, Public + private"),
#         tags$li("Status of creditors: Domestic vs External debt, Private vs Official"),
#         tags$li("Currency denomination: Domestic vs foreign denominated debt"),
#         tags$li("Quality of the data: dummy vs amount of debt in default")
#       ),
#       h5(strong("... The frequency:")," yearly, monthly, daily"),
#       h5(strong("... The Authors:")," Reinhart & Rogoff (2009), Cruces & Trebesch (2013), Beers and Nadeau (2015), Cohen et Valladier(2012) ...."),
#       h5(strong("... The objective")),
#       tags$ol(
#         tags$li("Are sovereign default rare events? (Reinhart, Rogoff)"),
#         tags$li("What is the cost of defaults for the sovereign? (Panizza, ...)"),
#         tags$li("What is the cost for the creditors? (Cruces, Trebesch)"),
#         tags$li("What is the intensity of crisis associated to sovereign defaults?"),
#         tags$li("...")
#       ),
#       h5(strong("My interest:"),"Look at the",strong("rise"),"of the Sovereign crisis and observe how the start of the crisis 
#          interact with the rise of other associated crises"),
#       tags$ol(
#         tags$li("Identify the first signs of debt distress"),
#         tags$li("Characterize whether debt distress episodes have or not materalized in a crisis."),
#         tags$li("Judge the severity of the crisis and the outcomes of the resolutions")
#       )
#       )
# }

