table_dispo_periods_generic=function(mydata,vars,file=NULL){
  if(!is.null(file)){
    OUT <- openxlsx::createWorkbook()
  }
  data=list()
  first<-lapply(vars, function(x) {
    first <- mydata %>% mutate(year=as.numeric(year(Period))) %>% 
      group_by(ISO3_Code) %>% dplyr::select(ISO3_Code,x,year) %>% 
      na.omit() %>% filter(year==min(year)) %>% slice(1) %>%
      dplyr::select(ISO3_Code,year)
  })
  names(first)=vars
  last<-lapply(vars, function(x) {
    last <- mydata %>% mutate(year=as.numeric(year(Period))) %>% 
      group_by(ISO3_Code) %>% dplyr::select(ISO3_Code,x,year) %>% 
      na.omit() %>% filter(year==max(year)) %>% slice(1) %>%
      dplyr::select(ISO3_Code,year)
  })   
  names(last)=vars
  
  first<-melt(first,id.vars = "ISO3_Code") %>% dplyr::select(ISO3_Code,variable=L1,first=value)
  last<-melt(last,id.vars = "ISO3_Code")  %>% dplyr::select(ISO3_Code,variable=L1,last=value)
  
  data_range<-merge(first,last,by=c("ISO3_Code","variable")) %>% 
    mutate(range=paste(first,"-",last,sep="")) %>% 
    dplyr::select(ISO3_Code,variable,range) %>%
    reshape2::dcast(ISO3_Code~variable,value.var="range")
  if(!is.null(file)){
    openxlsx::addWorksheet(OUT,"dispo_periods")
    openxlsx::writeData(OUT, sheet = "dispo_periods", x = data_range)
    openxlsx::saveWorkbook(OUT,file,overwrite=T)
  }
  return(data_range)
}
