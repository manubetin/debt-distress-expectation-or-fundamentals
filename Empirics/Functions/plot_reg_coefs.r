plot_coefs_reg.q.P4=function(reg,level.significance=0.05){
  critical_value=qnorm(level.significance/2,lower.tail=FALSE)
  data=data.frame(summary(reg)$coef)[1:4,] %>% mutate(Phase=rownames(summary(reg)$coef)[1:4],
                                                      Phase=ifelse(Phase=="(Intercept)","Phase_B2",Phase),
                                                      Conf.Interval.sup=Estimate+Std..Error*critical_value,
                                                      Conf.Interval.inf=Estimate-Std..Error*critical_value,
                                                      Conf.Interval.sup=ifelse(Phase=="Phase_B2",0,Conf.Interval.sup),
                                                      Conf.Interval.inf=ifelse(Phase=="Phase_B2",0,Conf.Interval.inf),
                                                      Estimate=ifelse(Phase=="Phase_B2",0.1,Estimate)) %>%
    dplyr::select(Phase,Estimate,Conf.Interval.sup,Conf.Interval.inf) %>% arrange(Phase)
  
  ggplot(data)+
    geom_point(stat = "identity",aes(x=Phase,y=Estimate,fill="Estimate"),size=2)+ 
    geom_point(stat = "identity",aes(x=Phase,y=Conf.Interval.sup,fill="Upper bound"),shape=2,size=1)+ 
    geom_point(stat = "identity",aes(x=Phase,y=Conf.Interval.inf,fill="Lower bound"),shape=6,size=1)+ 
    geom_hline(yintercept = 0)+
    xlab("") +
    #scale_fill_manual(value=c("red","blue","green")) +
    ylab("Basis points") +
   # scale_y_continuous(breaks = seq(-170, 100, by = 20),limits=c(-170,100)) +
    labs(title=NULL,
         subtitle = NULL,
         caption=NULL, fill="") +
    #ylim(800,1200)+
    theme_bw() +
    theme(axis.text.x = element_text(angle = 90, hjust = 1,vjust=0.5,size = 8),legend.position = "none",
          axis.title.y = element_text(size=8))
}
plot_coefs_reg.q.P2=function(reg,level.significance=0.05){
  critical_value=qnorm(level.significance/2,lower.tail=FALSE)
  data=data.frame(summary(reg)$coef)[1:2,] %>% mutate(Phase=rownames(summary(reg)$coef)[1:2],
                                                      Phase=ifelse(Phase=="(Intercept)","Phase_B",Phase),
                                                      Conf.Interval.sup=Estimate+Std..Error*critical_value,
                                                      Conf.Interval.inf=Estimate-Std..Error*critical_value,
                                                      Conf.Interval.sup=ifelse(Phase=="Phase_B",Estimate-Conf.Interval.sup,Conf.Interval.sup),
                                                      Conf.Interval.inf=ifelse(Phase=="Phase_B",Estimate-Conf.Interval.inf,Conf.Interval.inf),
                                                      Estimate=ifelse(Phase=="Phase_B",0.,Estimate)) %>%
    dplyr::select(Phase,Estimate,Conf.Interval.sup,Conf.Interval.inf) %>% arrange(Phase)
  
  ggplot(data)+
    geom_point(stat = "identity",aes(x=Phase,y=Estimate,fill="Estimate"),size=2)+ 
    geom_point(stat = "identity",aes(x=Phase,y=Conf.Interval.sup,fill="Upper bound"),shape=2,size=1)+ 
    geom_point(stat = "identity",aes(x=Phase,y=Conf.Interval.inf,fill="Lower bound"),shape=6,size=1)+ 
    geom_hline(yintercept = 0)+
    xlab("") +
    #scale_fill_manual(value=c("red","blue","green")) +
    ylab("Basis points") +
    #scale_y_continuous(breaks = seq(-170, 30, by = 20),limits=c(-170,30)) +
    labs(title=NULL,
         subtitle = NULL,
         caption=NULL, fill="") +
    #ylim(800,1200)+
    theme_bw() +
    theme(axis.text.x = element_text(angle = 90, hjust = 1,vjust=0.5,size = 8),legend.position = "none",
          axis.title.y = element_text(size=8))
  # if(save.name!=NULL){
  #    ggsave(save.name)
  # }
  #
}
