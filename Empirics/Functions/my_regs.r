my_regs=function(list_vars,list_ctrls){
  
  specifications=c("Univariate","Two_way_Fixed_Effects","Two_way_Fixed_Effects_and_controls")
  
  reg.q.P2=list() #two phases
  reg.q.P2[[specifications[1]]]=reg_by_phase_2(mydata_q,list_vars)
  reg.q.P2[[specifications[2]]]=reg_by_phase_2(mydata_q,list_vars,ctry.FE = T)
  reg.q.P2[[specifications[3]]]=reg_by_phase_2(mydata_q,list_vars,ctrls = list_ctrls,ctry.FE = T)
  
  reg.q.P4=list()
  reg.q.P4[[specifications[1]]]=reg_by_phase_4(mydata_q,list_vars)
  reg.q.P4[[specifications[2]]]=reg_by_phase_4(mydata_q,list_vars,ctry.FE = T)
  reg.q.P4[[specifications[3]]]=reg_by_phase_4(mydata_q,list_vars,ctrls = list_ctrls,ctry.FE = T)
  
  reg.q=list("Phase 2"= reg.q.P2,"Phase 4"= reg.q.P4)
  
  reg_export=list()
  for(i in 1:length(specifications)){
    reg_export[[paste(specifications[i],"_P2",sep="")]]=reg.q.P2[[specifications[i]]]$summary_regression
    plot_coefs_reg.q.P2(reg_export[[paste(specifications[i],"_P2",sep="")]])+
    ggsave(paste("Output/Graphs/Regressions/Fig_",list_vars,"_P2_",specifications[i],".png",sep=""))
  }
  
  for(i in 1:length(specifications)){
    reg_export[[paste(specifications[i],"_P4",sep="")]]=reg.q.P4[[specifications[i]]]$summary_regression
    plot_coefs_reg.q.P4(reg_export[[paste(specifications[i],"_P4",sep="")]])+
    ggsave(paste("Output/Graphs/Regressions/Fig_",list_vars,"_P4_",specifications[i],".png",sep=""))
    
  }
  critical_value=qnorm(0.05/2,lower.tail=FALSE)
  
  reg_pval=t(reg.q.P4[[specifications[1]]]$coefs)
  pval_A1_A2=(reg_pval["Phase_A1","Estimate"]-reg_pval["Phase_A2","Estimate"])/reg_pval["Phase_A1","Std..Error"]
  pval_A2_B1=(reg_pval["Phase_A2","Estimate"]-reg_pval["Phase_B1","Estimate"])/reg_pval["Phase_A2","Std..Error"]
  pval_A1_B1=(reg_pval["Phase_A1","Estimate"]-reg_pval["Phase_B1","Estimate"])/reg_pval["Phase_A1","Std..Error"]
  ttest_spec_1=c(pval_A1_A2,pval_A2_B1,pval_A1_B1)
  
  reg_pval=t(reg.q.P4[[specifications[2]]]$coefs)
  pval_A1_A2=(reg_pval["Phase_A1","Estimate"]-reg_pval["Phase_A2","Estimate"])/reg_pval["Phase_A1","Std..Error"]
  pval_A2_B1=(reg_pval["Phase_A2","Estimate"]-reg_pval["Phase_B1","Estimate"])/reg_pval["Phase_A2","Std..Error"]
  pval_A1_B1=(reg_pval["Phase_A1","Estimate"]-reg_pval["Phase_B1","Estimate"])/reg_pval["Phase_A1","Std..Error"]
  ttest_spec_2=c(pval_A1_A2,pval_A2_B1,pval_A1_B1)
  
  reg_pval=t(reg.q.P4[[specifications[3]]]$coefs)
  pval_A1_A2=(reg_pval["Phase_A1","Estimate"]-reg_pval["Phase_A2","Estimate"])/reg_pval["Phase_A1","Std..Error"]
  pval_A2_B1=(reg_pval["Phase_A2","Estimate"]-reg_pval["Phase_B1","Estimate"])/reg_pval["Phase_A2","Std..Error"]
  pval_A1_B1=(reg_pval["Phase_A1","Estimate"]-reg_pval["Phase_B1","Estimate"])/reg_pval["Phase_A1","Std..Error"]
  ttest_spec_3=c(pval_A1_A2,pval_A2_B1,pval_A1_B1)
  
  ttest=round(t(rbind(ttest_spec_1,ttest_spec_2,ttest_spec_3)),2)
  
  table=stargazer(reg_export,title="Estimation results",
                  no.space=TRUE,align = T,summary=F,rownames = T, digits=2,
                  table.placement="H",
                  df=F,
                  order=c(2,6,7,8,9,10,11,12,3,4,5,1),
                  column.sep.width="3pt",
                  add.lines=list(c("Country FE","No","Yes","Yes","No","Yes","Yes"),
                                 c("Time FE","No","Yes","Yes","No","Yes","Yes"),
                                 c("t.test A1 vs A2","-","-","-",ttest[1,]),
                                 c("t.tes A2 vs B1","-","-","-",ttest[2,]),
                                 c("t.test A1 vs B1","-","-","-",ttest[3,])),
                  column.labels = c("A - B","A1 - A2 - B1 - B2"),
                  column.separate = c(3,3),
                  # dep.var.labels = "test",
                  omit=c("ISO3_Code","Period"),
                  font.size = "footnotesize",
                  label=paste("Reg_main_",list_vars,sep=""),
                  notes=c("Constant refers to Phase B2 t.test line compare the significance of the difference between",
                          "coefficient in each phase",
                          "A - B specification (column (1),(2),(3)) compare level of CDS between Phase A and B",
                          "A1 - A2 - B1 - B2 specification (column (1), (2), (3)) compare the level of CDS in,"
                          ,"the first and second half of A and first and second half of B"),
                  notes.align = "l",
                  notes.append=T,
                  out=paste("Output/Tables/Regressions/regressions_",list_vars,".tex",sep=""))
  
  output=list("regression table"=table,"output reg"=reg.q)
}
