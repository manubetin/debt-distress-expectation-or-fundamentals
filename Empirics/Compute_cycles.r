##### *********************************************************************************************#####

#                       Compute different Cycles                                                         ####

##### *********************************************************************************************#####


##### *********************************************************************************************#####
##### set up#####
##clean environment
rm(list = ls())

## set the working directory were the script is located
current_path = rstudioapi::getActiveDocumentContext()$path 
#setwd("~/Documents/Professionel/Manuel/2018-2021 Th??se/Chap1-Sovereign CDS and Business Cycles/Empirics")
#setwd(dirname("~/Documents/Professionel"))

#download own functions
file.sources = list.files("Functions",  pattern="*.R$", full.names=TRUE, ignore.case=TRUE)
sapply(file.sources,source,.GlobalEnv) #download the functions necessary for the simulator.

##install common packages
packages <- c("dplyr","ggplot2","reshape2","mFilter","car","countrycode","tidyquant","lubridate",'tictoc',"rio","BCDating")

#packages <- c("dplyr","ggplot2","countrycode","rio","tictoc","tidyquant")
#install.my.packages(packages)

## load common packages
load.my.packages(packages)

##data at daily frequency
load("Data/SovDebt_riskPrem_BusCycles.RData")

mydata=SovDebt_riskPrem_BusCycles$data
metadata=SovDebt_riskPrem_BusCycles$metadata

##Compute additional variables
mydata=mydata %>% mutate(`5Y_cds`=as.numeric(`5Y_cds`),
                         Gov.Gross.debt=as.numeric(Gov.Gross.debt),
                         Gov.Primary.bal=as.numeric(Gov.Primary.bal))

mydata=mydata %>% 
  group_by(ISO3_Code) %>%
  arrange(ISO3_Code,Period) %>%
  mutate(d.5Y_cds=`5Y_cds`-dplyr::lag(`5Y_cds`,1))

mean.na.omit=function(x){
  x=mean(x,na.rm=T)
  return(x)
}
mydata_q=mydata %>% group_by(ISO3_Code,year,quarter) %>% summarize_if(is.numeric,mean.na.omit)
mydata_q=mydata_q %>% mutate(Period=as.Date(as.yearqtr(paste(year,quarter,sep="-Q"),format="%Y-Q%q")))
mydata %>% group_by(ISO3_Code)
##Compute new variable at quaterly frequency
mydata_q=mydata_q %>%
  group_by(ISO3_Code,Period) %>% 
  arrange(ISO3_Code,Period) %>% mutate(X5Y_cds=`5Y_cds`,
                                       X2Y_cds=`2Y_cds`,
                                       X3Y_cds=`3Y_cds`,
                                       X4Y_cds=`4Y_cds`,
                                       X7Y_cds=`7Y_cds`,
                                       X10Y_cds=`10Y_cds`,
                                       X5Y_cds=ifelse(X5Y_cds==cds5y,NA,X5Y_cds),
                                       X5Y_cds_vol=`5Y_cds_vol90`,
                                       l1.X5Y_cds=dplyr::lag(X5Y_cds,1),
                                       d.q.5Y_cds=log(X5Y_cds/dplyr::lag(X5Y_cds,1)),
                                       US.yield.curve=US_tbill_10y-US_tbill_3m,
                                       bd_spread=ifelse(is.na(bond.spread),bond.spread.IFS,bond.spread)*100,
                                       cds_bond_basis=X5Y_cds-bd_spread,
                                       X1Y_cds=`1Y_cds`,
                                       cds_curve=X5Y_cds-X1Y_cds)

##### *********************************************************************************************#####
# Compute GDP cycles and coincidence with US cycles
##### *********************************************************************************************#####

#Look at coincidence of output cycles of the US and economic activities of countries
US_GDPcycle=data.frame(mydata_q) %>% filter(ISO3_Code=="USA") %>% dplyr::select(Period,US_GDPcycle=GDPV_XDC_cycle)
mydata_q=merge(x=mydata_q,y=US_GDPcycle,by=c("Period"),all.x=T)

CoIndex_globalGDP=CoIndex_barchart("GDPV_XDC_cycle","US_GDPcycle")
CoIndex_globalGDP_table=CoIndex_globalGDP$`Coincidence index` %>% filter(CoIndex.GDPV_XDC_cycle.US_GDPcycle!=0 & ISO3_Code!="USA")

median_CI=median(CoIndex_globalGDP_table$CoIndex.GDPV_XDC_cycle.US_GDPcycle)
CoIndex_globalGDP_table=CoIndex_globalGDP_table %>%
  mutate(Procyclical_ctries=ifelse(CoIndex.GDPV_XDC_cycle.US_GDPcycle<median_CI,"counterCyclical","Procyclical")) %>%
  na.omit()

ggplot(data=CoIndex_globalGDP_table,aes(x=reorder(ISO3_Code, CoIndex.GDPV_XDC_cycle.US_GDPcycle),y=CoIndex.GDPV_XDC_cycle.US_GDPcycle))+
  geom_bar(stat='identity',aes(fill=Classification))+
  geom_hline(yintercept=median_CI,aes(fill="Median"))+
  theme_bw(base_size = 10) + 
  xlab("")+
  ylab("Index")+
  labs(title="Coincidence with US business cycle",subtitle =NULL ,caption=NULL)+
  theme(axis.text.x=element_text(angle=90,hjust=1),legend.position = "bottom")
ggsave(paste("Output/Graphs/Business Cycles/CoIndex_with_UScycle.png",sep=""))

#plot coincidence index and average CDS spread. 
cds=mydata_q %>% group_by(ISO3_Code) %>% summarize(X5Y_cds=mean(X5Y_cds,na.rm=T),X5Y_cds_vol=mean(X5Y_cds_vol,na.rm=T))
CoIndex_globalGDP_table=merge(x=CoIndex_globalGDP_table,y=cds,by=c("ISO3_Code"))

ggplot(data=CoIndex_globalGDP_table,aes(x=CoIndex.GDPV_XDC_cycle.US_GDPcycle,y=X5Y_cds))+
  geom_text(aes(label=ISO3_Code,color=Procyclical_ctries))+
  geom_smooth(method='lm')+
  lims(y=c(0,500))+
  xlab("Coincidence Index with US cycles")+
  ylab("CDS spread in basis points")+
  lims(x=c(0.25,0.8))+
  theme_bw()

##### *********************************************************************************************#####
# Compute FXC cycles and coincidence with GDP cycle
##### *********************************************************************************************#####

FXC_cycle=panel_hp_filter(mydata_q,"FXC",lambda=1600)
FXC_cycle=FXC_cycle %>% dplyr::rename(FXC_cycle=Value_cycle_DS)

FXC_cycle=Expansions_Contractions_4(FXC_cycle,"FXC_cycle",mincycle = 6,minphase = 4)
FXC_cycle_stats=cycle_stats(FXC_cycle,"FXC_cycle")
FXC_cycle_stats=FXC_cycle_stats %>% filter(ISO3_Code=="MEX")
FXC_cycle_plots=plot.Cycles(FXC_cycle,"FXC_cycle","MEX","FXC_cycle")
FXC_cycle_plots$`Turning points`

FXC_cycle=FXC_cycle %>% dplyr::select(ISO3_Code,Period,FXC_cycle)
mydata_q=merge(x=mydata_q,y=FXC_cycle,by=c("ISO3_Code","Period"),all.x=T)

Coindex_GDP_FXC_cycles=CoIndex_barchart("GDPV_XDC_cycle","FXC_cycle")

##### *********************************************************************************************#####
# Compute X5Y_cds cycles and coincidence with GDP cycle
##### *********************************************************************************************#####

CDS_cycle=Expansions_Contractions_4(data.frame(mydata_q),"X5Y_cds",mincycle = 6,minphase = 4)
CDS_cycle_stats=cycle_stats(CDS_cycle,"X5Y_cds")

country="BRA"
CDS_cycle_stats=CDS_cycle_stats %>% filter(ISO3_Code==country)
CDS_cycle_plots=plot.Cycles(CDS_cycle,"X5Y_cds",country,"X5Y_cds")

CDS_cycle_plots$`Turning points`
CDS_cycle_plots$`Phases A1.A1.B1.B2`
CDS_cycle_plots$`X5Y_cds in Cycle`
ggsave(paste("Output/Graphs/Belief Cycles/X5Y_cds_cycles_",country,".png",sep=""))

cor.test(mydata_q$X5Y_cds,mydata_q$Gov.Gross.debt,na.rm=T)

cor.test(mydata_q$X5Y_cds,mydata_q$FXC.growth_q,na.rm=T)


Coindex_GDP_CDS_cycles=CoIndex_barchart("GDPV_XDC_cycle","X5Y_cds")
Coindex_GDP_CDS_cycles$plot

#y=CoIndex_barchart("X5Y_cds","FXC_cycle")

#z=CoIndex_barchart("X5Y_cds","bd_spread")

##### *********************************************************************************************#####
# Compute VIX_close cycles and coincidence with GDP cycle
##### *********************************************************************************************#####

VIX_cycle=Expansions_Contractions_4(data.frame(mydata_q),"VIX_close",mincycle = 6,minphase = 4)
VIX_cycle_stats=cycle_stats(VIX_cycle,"VIX_close")

country="TUR"
VIX_cycle_stats=VIX_cycle_stats %>% filter(ISO3_Code==country)
VIX_cycle_plots=plot.Cycles(VIX_cycle,"VIX_close",country,"VIX_close")

VIX_cycle_plots$`Turning points`
VIX_cycle_plots$`Phases A1.A1.B1.B2`
VIX_cycle_plots$`VIX_close in Cycle`
ggsave("Output/Graphs/Belief Cycles/VIX_cycles.png")


##### *********************************************************************************************#####
# Compute VIX_close cycles and coincidence with GDP cycle
##### *********************************************************************************************#####

oil_cycle=Expansions_Contractions_4(data.frame(mydata_q),"oil_price",mincycle = 6,minphase = 2)
oil_cycle_stats=cycle_stats(oil_cycle,"oil_price")

country="TUR"
oil_cycle_stats=oil_cycle_stats %>% filter(ISO3_Code==country)
oil_cycle_plots=plot.Cycles(oil_cycle,"oil_price",country,"oil_price")

oil_cycle_plots$`Turning points`
oil_cycle_plots$`Phases A1.A1.B1.B2`
oil_cycle_plots$`oil_price in Cycle`


CurrentAcc_cycle=Expansions_Contractions_4(data.frame(mydata_q),"CBGDPR",mincycle = 6,minphase = 2)
CurrenAcc_cycle_stats=cycle_stats(CurrentAcc_cycle,"CBGDPR")

country="MEX"
CurrenAcc_cycle_stats=CurrenAcc_cycle_stats %>% filter(ISO3_Code==country)
CurrenAcc_cycle_plots=plot.Cycles(CurrentAcc_cycle,"CBGDPR",country,"CBGDPR")
CurrenAcc_cycle_plots$`Phases A1.A1.B1.B2`
CurrenAcc_cycle_plots$`CBGDPR in Cycle`


names(mydata_q)

GDPV_cycle=Expansions_Contractions_4(data.frame(mydata_q),"GDPV_XDC_cycle",mincycle = 6,minphase = 2)

GDPV_cycle_stats=cycle_stats(GDPV_cycle,"GDPV_XDC_cycle")

country="MEX"
GDPV_cycle_stats=GDPV_cycle_stats %>% filter(ISO3_Code==country)
GDPV_cycle_plots=plot.Cycles(GDPV_cycle,"GDPV_XDC_cycle",country,"GDPV_XDC_cycle")
GDPV_cycle_plots$`Phases A1.A1.B1.B2`
GDPV_cycle_plots$`GDPV_XDC_cycle in Cycle`

Coindex_GDP_FXC_cycles=CoIndex_barchart("GDPV_XDC_cycle","CBGDPR")

Coindex_GDP_FXC_cycles$plot

##### *********************************************************************************************#####
# Look at beta of spreads with US equity
##### *********************************************************************************************#####

years=as.numeric(seq(from=2004,to=2017))
ctries=unique((mydata %>% dplyr::select(ISO3_Code,GSPC_close,`5Y_cds`) %>% na.omit())$ISO3_Code)

fun_2=function(ctry,x){ #run regression for selected country
  dt=mydata %>% dplyr::filter(year==x & ISO3_Code==ctry) %>% dplyr::select(ISO3_Code,`5Y_cds`,GSPC_close) %>% na.omit()
  if(dim(dt)[1]>10){
    reg=lm(`5Y_cds`~ GSPC_close,dt)
    res=summary(reg)$coef[2,"Estimate"]
    return(res)
  }else return(res=NA)
}
fun_1=function(year){ #run regression selected year for all countries
  sapply(ctries,function(x){
     fun_2(x,year)
   })
}

beta_USstock=sapply(years,fun_1) #table of correlation between spread and US equity performance
colnames(beta_USstock)=years
dt=data.frame(beta_USstock["ARG",])
dt$dates=rownames(dt)
colnames(dt)=c("ARG","dates")

        