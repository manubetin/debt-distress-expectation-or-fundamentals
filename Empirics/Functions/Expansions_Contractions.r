
Expansions_Contractions_2=function(mydata,var,mincycle=12,minphase=2){
  
  #compute the Peaks and troughs with BBQ algorithm using 4 subphase (A1,A2,B1,B2)
  mydata=data.frame(mydata)
  mydata=Locate_Peaks_Troughs(mydata,var,mincycle,minphase)$data %>% arrange(ISO3_Code,Period) #rearrange the rows in proper order 
  mydata=data.frame(mydata)
  list_country=unique(mydata$ISO3_Code)
  n_ctry=length(list_country)
  
  
  for (k in 1:length(list_country)){
    count_Peaks=0
    count_Troughs=0
    country=list_country[k]
    #initialize the new variables
    mydata_country=mydata %>% filter(ISO3_Code==country & !is.na(eval(parse(text=var)))) %>% mutate(Phase_A_t=NA, #time trend inside each phase of expansion
                                                                                                    Phase_B_t=NA, #time trend  in phase of recession
                                                                                                    Index_Phase_A=NA, #count the position of each phase A  
                                                                                                    Index_Phase_B=NA, #count the position of each phase B  
                                                                                                    Phase_A1_to_A2=0, #dummy for quarter in the middle of expansion cycle
                                                                                                    Phase_A2_t=0, #time trend in the second half phase of expansion
                                                                                                    Phase_B1_to_B2=0, #dummy for quarter in the middle of recession cycle
                                                                                                    Phase_B2_t=0) #time trend in the second half phase of recession
    
    #find the whether the first cycle is phase A or Phase B
    #keep in mind that there is a issue for the first cycle in phase_A1 and phase_A2 that are not perfectly computed
    z_peak=1
    while(is.na(mydata_country[1+z_peak,"Peaks"]) & z_peak<dim(mydata_country)[1]){z_peak=z_peak+1} 
    z_trough=1
    while(is.na(mydata_country[1+z_trough,"Troughs"]) & z_trough<dim(mydata_country)[1]){z_trough=z_trough+1}
    z=min(z_peak,z_trough)
    if(z==z_peak){
      count_Peaks=count_Peaks+1
      mydata_country[1:z,"Phase_A_t"]=1:z
      mydata_country[1:z,"Index_Phase_A"]=count_Peaks
    }else{
      count_Troughs=count_Troughs+1
      mydata_country[1:z,"Phase_B_t"]=1:z
      mydata_country[1:z,"Index_Phase_B"]=count_Troughs
    }
    #message(paste(country,z,dim(mydata_country)[1]) )
    if (z<dim(mydata_country)[1]) {
      for(i in z:dim(mydata_country)[1]){ #loop on all countries
        
        if(!is.na(mydata_country[i,"Troughs"])){ #build variables for Booms
          j=1
          count_Peaks=count_Peaks+1
          while(is.na(mydata_country[i+j,"Peaks"]) & j<40){ #j<40 is only used to make sure there is no infinite loop
            mydata_country[i+j,"Index_Phase_A"]=count_Peaks
            mydata_country[i+j,"Phase_A_t"]=j
            j=j+1
          }
          mydata_country[i+j,"Phase_A_t"]=j
          mydata_country[i+j,"Index_Phase_A"]=count_Peaks
          mydata_country[i+round(j/2,0),"Phase_A1_to_A2"]=1 #create dummy at half of the cycle
          v=j-round(j/2,0)
          #restart time trend for the second half of the cycle
          mydata_country[(i+round(j/2,0)+1):(i+round(j/2,0)+v),"Phase_A2_t"]=1:v
        }else if(!is.na(mydata_country[i,"Peaks"])){ #build variables for troughs
          j=1
          count_Troughs=count_Troughs+1
          while(is.na(mydata_country[i+j,"Troughs"]) & j<40){
            mydata_country[i+j,"Phase_B_t"]=j
            mydata_country[i+j,"Index_Phase_B"]=count_Troughs
            j=j+1
          }
          mydata_country[i+j,"Phase_B_t"]=j
          mydata_country[i+j,"Index_Phase_B"]=count_Troughs
          mydata_country[i+round(j/2,0),"Phase_B1_to_B2"]=1
          v=j-round(j/2,0)
          mydata_country[(i+round(j/2,0)+1):(i+round(j/2,0)+v),"Phase_B2_t"]=1:v
        }
      }
    }
    if(k==1){
      mydata_2=mydata_country
    }else mydata_2=rbind(mydata_2,mydata_country)
    
  }
  
  mydata=mydata_2
  remove(mydata_2)
  remove(mydata_country)
  mydata=mydata %>% mutate(Phase_A=ifelse(!is.na(Phase_B_t),1,0),
                           Phase_B=ifelse(!is.na(Phase_A_t),0,1),
                           Phase_A=ifelse(Phase_B==1,0,1),
                           Phase_A1_t=ifelse(Phase_A==1 & Phase_A2_t==0,Phase_A_t,0),
                           Phase_B1_t=ifelse(Phase_B==1 & Phase_B2_t==0,Phase_B_t,0),
                           Phase_A2=ifelse(Phase_A2_t!=0,1,0),
                           Phase_A1=ifelse(Phase_A1_t!=0,1,0),
                           Phase_B2=ifelse(Phase_B2_t!=0,1,0),
                           Phase_B1=ifelse(Phase_B1_t!=0,1,0),
                           Peaks=ifelse(is.na(Peaks),0,Peaks),
                           Troughs=ifelse(is.na(Troughs),0,Troughs),
                           Phase_A_t=ifelse(is.na(Phase_A_t),0,Phase_A_t),
                           Phase_B_t=ifelse(is.na(Phase_B_t),0,Phase_B_t),
                           year=year(Period),
                           quarter=quarter(Period))
  
  mydata=mydata %>% group_by(ISO3_Code,Index_Phase_A) %>% arrange(ISO3_Code,Period) %>% mutate(length.Phase_A=n(),
                                                                                               length.Phase_A=ifelse(Phase_A_t==0,0,length.Phase_A))
  mydata=mydata %>% group_by(ISO3_Code,Index_Phase_B) %>% arrange(ISO3_Code,Period) %>% mutate(length.Phase_B=n(),
                                                                                               length.Phase_B=ifelse(Phase_B_t==0,0,length.Phase_B))
  mydata=mydata %>% mutate(Phase_A_t_mirror=ifelse(Phase_A_t==0,Phase_B_t,-(length.Phase_A-Phase_A_t))) 
  
  #correct the indexes to ensure that the first cycle correspond to a complete cycle and not only due to arbitrary initial 
  
  mydata<-mydata %>% group_by(ISO3_Code) %>% 
    mutate(Index_Phase_B=Index_Phase_B-1) %>%
    mutate(Index_Phase_A=Index_Phase_A-ifelse(is.na(first(Index_Phase_A)),0,1))
  #creation of an index for the cycle (trough to trough)
  mydata<-mydata %>% mutate(Index_cycle=ifelse(is.na(Index_Phase_A),Index_Phase_B,Index_Phase_A),
                            Index_cycle=ifelse(is.na(Index_cycle),0,Index_cycle)) 
  
  
  mydata=mydata %>% filter(!is.na(ISO3_Code) & !is.na(Period)) %>%
    dplyr::select(ISO3_Code,
                  Period,
                  year,
                  quarter,
                  var,
                  Phase_A,
                  Phase_A_t,
                  Index_Phase_A,
                  length.Phase_A,
                  Phase_A1,
                  Phase_A1_t,
                  Phase_A2,
                  Phase_A2_t,
                  Phase_B,
                  Phase_B_t,
                  Index_Phase_B,
                  length.Phase_B,
                  Phase_B1,
                  Phase_B1_t,
                  Phase_B2,
                  Phase_B2_t,
                  Peaks,
                  Troughs,
                  Phase_A1_to_A2,
                  Phase_B1_to_B2,
                  Index_cycle)
  return(mydata)
}


Expansions_Contractions=function(mydata,var,mincycle=12,minphase=2){
  
  #compute the Peaks and troughs with BBQ algorithm using 4 subphase (A1,A2,B1,B2)
  mydata=data.frame(mydata)
  mydata=Locate_Peaks_Troughs(mydata,var,mincycle,minphase)$data %>% arrange(ISO3_Code,Period) #rearrange the rows in proper order 
  mydata=data.frame(mydata)
  list_country=unique(mydata$ISO3_Code)
  n_ctry=length(list_country)
  
  
  for (k in 1:length(list_country)){
    count_Peaks=0
    count_Troughs=0
    country=list_country[k]
    #initialize the new variables
    mydata_country=mydata %>% filter(ISO3_Code==country & !is.na(eval(parse(text=var)))) %>% mutate(Phase_A_t=NA, #time trend inside each phase of expansion
                                                                                                    Phase_B_t=NA, #time trend  in phase of recession
                                                                                                    Index_Phase_A=NA, #count the position of each phase A  
                                                                                                    Index_Phase_B=NA, #count the position of each phase B  
                                                                                                    Phase_A1_to_A2=0, #dummy for quarter in the middle of expansion cycle
                                                                                                    Phase_A2_t=0, #time trend in the second half phase of expansion
                                                                                                    Phase_B1_to_B2=0, #dummy for quarter in the middle of recession cycle
                                                                                                    Phase_B2_t=0) #time trend in the second half phase of recession
    
    #find the whether the first cycle is phase A or Phase B
    #keep in mind that there is a issue for the first cycle in phase_A1 and phase_A2 that are not perfectly computed
    z_peak=1
    while(is.na(mydata_country[1+z_peak,"Peaks"]) & z_peak<dim(mydata_country)[1]){z_peak=z_peak+1} 
    z_trough=1
    while(is.na(mydata_country[1+z_trough,"Troughs"]) & z_trough<dim(mydata_country)[1]){z_trough=z_trough+1}
    z=min(z_peak,z_trough)
    if(z==z_peak){
      count_Peaks=count_Peaks+1
      mydata_country[1:z,"Phase_A_t"]=1:z
      mydata_country[1:z,"Index_Phase_A"]=count_Peaks
    }else{
      count_Troughs=count_Troughs+1
      mydata_country[1:z,"Phase_B_t"]=1:z
      mydata_country[1:z,"Index_Phase_B"]=count_Troughs
    }
    #message(paste(country,z,dim(mydata_country)[1]) )
    if (z<dim(mydata_country)[1]) {
      for(i in z:dim(mydata_country)[1]){ #loop on all countries
        
        if(!is.na(mydata_country[i,"Troughs"])){ #build variables for Booms
          j=1
          count_Peaks=count_Peaks+1
          while(is.na(mydata_country[i+j,"Peaks"]) & j<40){ #j<40 is only used to make sure there is no infinite loop
            mydata_country[i+j,"Index_Phase_A"]=count_Peaks
            mydata_country[i+j,"Phase_A_t"]=j
            j=j+1
          }
          mydata_country[i+j,"Phase_A_t"]=j
          mydata_country[i+j,"Index_Phase_A"]=count_Peaks
          mydata_country[i+round(j/2,0),"Phase_A1_to_A2"]=1 #create dummy at half of the cycle
          v=j-round(j/2,0)
          #restart time trend for the second half of the cycle
          mydata_country[(i+round(j/2,0)+1):(i+round(j/2,0)+v),"Phase_A2_t"]=1:v
        }else if(!is.na(mydata_country[i,"Peaks"])){ #build variables for troughs
          j=1
          count_Troughs=count_Troughs+1
          while(is.na(mydata_country[i+j,"Troughs"]) & j<40){
            mydata_country[i+j,"Phase_B_t"]=j
            mydata_country[i+j,"Index_Phase_B"]=count_Troughs
            j=j+1
          }
          mydata_country[i+j,"Phase_B_t"]=j
          mydata_country[i+j,"Index_Phase_B"]=count_Troughs
          mydata_country[i+round(j/2,0),"Phase_B1_to_B2"]=1
          v=j-round(j/2,0)
          mydata_country[(i+round(j/2,0)+1):(i+round(j/2,0)+v),"Phase_B2_t"]=1:v
        }
      }
    }
    if(k==1){
      mydata_2=mydata_country
    }else mydata_2=rbind(mydata_2,mydata_country)
    
  }
  
  mydata=mydata_2
  remove(mydata_2)
  remove(mydata_country)
  mydata=mydata %>% mutate(Phase_A=ifelse(!is.na(Phase_B_t),1,0),
                           Phase_B=ifelse(!is.na(Phase_A_t),0,1),
                           Phase_A=ifelse(Phase_B==1,0,1),
                           Phase_A1_t=ifelse(Phase_A==1 & Phase_A2_t==0,Phase_A_t,0),
                           Phase_B1_t=ifelse(Phase_B==1 & Phase_B2_t==0,Phase_B_t,0),
                           Phase_A2=ifelse(Phase_A2_t!=0,1,0),
                           Phase_A1=ifelse(Phase_A1_t!=0,1,0),
                           Phase_B2=ifelse(Phase_B2_t!=0,1,0),
                           Phase_B1=ifelse(Phase_B1_t!=0,1,0),
                           Peaks=ifelse(is.na(Peaks),0,Peaks),
                           Troughs=ifelse(is.na(Troughs),0,Troughs),
                           Phase_A_t=ifelse(is.na(Phase_A_t),0,Phase_A_t),
                           Phase_B_t=ifelse(is.na(Phase_B_t),0,Phase_B_t),
                           year=year(Period),
                           quarter=quarter(Period))
  
  mydata=mydata %>% group_by(ISO3_Code,Index_Phase_A) %>% arrange(ISO3_Code,Period) %>% mutate(length.Phase_A=n(),
                                                                                               length.Phase_A=ifelse(Phase_A_t==0,0,length.Phase_A))
  mydata=mydata %>% group_by(ISO3_Code,Index_Phase_B) %>% arrange(ISO3_Code,Period) %>% mutate(length.Phase_B=n(),
                                                                                               length.Phase_B=ifelse(Phase_B_t==0,0,length.Phase_B))
  mydata=mydata %>% mutate(Phase_A_t_mirror=ifelse(Phase_A_t==0,Phase_B_t,-(length.Phase_A-Phase_A_t))) 
  
  #correct the indexes to ensure that the first cycle correspond to a complete cycle and not only due to arbitrary initial 
  
  mydata<-mydata %>% group_by(ISO3_Code) %>% 
    mutate(Index_Phase_B=Index_Phase_B-1) %>%
    mutate(Index_Phase_A=Index_Phase_A-ifelse(is.na(first(Index_Phase_A)),0,1))
  #creation of an index for the cycle (trough to trough)
  mydata<-mydata %>% mutate(Index_cycle=ifelse(is.na(Index_Phase_A),Index_Phase_B,Index_Phase_A),
                            Index_cycle=ifelse(is.na(Index_cycle),0,Index_cycle)) 

  mydata=mydata %>% filter(!is.na(ISO3_Code) & !is.na(Period)) %>%
    dplyr::select(ISO3_Code,
                  Period,
                  year,
                  quarter,
                  var,
                  Phase_A,
                  Phase_A_t,
                  Index_Phase_A,
                  length.Phase_A,
                  Phase_B,
                  Phase_B_t,
                  Index_Phase_B,
                  length.Phase_B,
                  Peaks,
                  Troughs,
                  Index_cycle)
  
  return(mydata)
}