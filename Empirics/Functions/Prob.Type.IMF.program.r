Prob.Type.IMF.program=function(mydata_q,t.liquid.crisis=0.1,t.solv.crisis=0.9){
  
  mydata_q=mydata_q %>% mutate(IMF.pure.liq.crisis=ifelse(IMF_All_diff<=t.liquid.crisis,1,0),
                               IMF.Non.pure.liq.crisis=ifelse(IMF_All_diff>t.liquid.crisis,1,0),
                               IMF.pure.solv.crisis=ifelse(IMF_All_diff>=t.solv.crisis,1,0),
                               IMF.Non.pure.solv.crisis=ifelse(IMF_All_diff<t.solv.crisis,1,0),
                               IMF.liq.crisis=ifelse(IMF_All_diff<=0.5,1,0),
                               IMF.solv.crisis=ifelse(IMF_All_diff>0.5,1,0))
  
  Prob_type_IMF_program=data.frame(mydata_q) %>% filter(!is.na(IMF_All_diff)) %>% 
    summarize(IMF.pure.liq.crisis=mean(IMF.pure.liq.crisis,na.rm=T)*100,
              IMF.pure.solv.crisis=mean(IMF.pure.solv.crisis,na.rm=T)*100,
              IMF.Non.pure.liq.crisis=mean(IMF.Non.pure.liq.crisis,na.rm=T)*100) %>% round(.,2)
              #IMF.liq.crisis=mean(IMF.liq.crisis,na.rm=T)*100,
              #IMF.solv.crisis=mean(IMF.solv.crisis,na.rm=T)*100 
  
  
  ctries.IMF.pure.liq.crisis=data.frame(mydata_q) %>% 
    filter(IMF.pure.liq.crisis==1) %>% mutate(year_pure.liq.crisis=paste0("Q",quarter,"-",year)) %>%
    dplyr::select(ctries_pure.liq.crisis=ISO3_Code,year_pure.liq.crisis)
  
  ctries.IMF.pure.solv.crisis=data.frame(mydata_q) %>% 
    filter(IMF.pure.solv.crisis==1) %>% mutate(year_pure.solv.crisis=paste0("Q",quarter,"-",year)) %>%
    dplyr::select(ctries_pure.solv.crisis=ISO3_Code,year_pure.solv.crisis)
  
  return(list(Proba=Prob_type_IMF_program,
              ctries.IMF.pure.liq.crisis=ctries.IMF.pure.liq.crisis,
              ctries.IMF.pure.solv.crisis=ctries.IMF.pure.solv.crisis))
}
