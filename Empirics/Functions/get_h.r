get_h=function(mydata,var=NULL) {
  if (is.null(var)==F) {
    mydata=data.frame(mydata) %>% dplyr::select(ISO3_Code,Period,var)
    mydata=Expansions_Contractions_2(mydata,var)
  }
  list_country=unique(mydata$ISO3_Code) #find list of countries available in the sample
  fun=function(ctry){
    mydata_country=mydata %>% filter(ISO3_Code==ctry)
    n_peaks=sum(mydata_country$Peaks)
    if (n_peaks>0) {
      N=NROW(na.omit(mydata_country$Peaks))
      h<-matrix(nrow = N, ncol = n_peaks)
      h<-data.frame(h)
      colnames(h) <- paste('h',1:n_peaks,sep="")
      t_peak=which(mydata_country$Peaks==1)
      for(j in 1:n_peaks){
        h[,j]<-1:NROW(na.omit(mydata_country$Peaks))-t_peak[j]
      }
      mydata_country<-transform(merge(mydata_country, h, by=0), row.names=Row.names, Row.names=NULL)
      mydata_country<-mydata_country[order(mydata_country$Period),]
      mydata_country_h<-melt(mydata_country,id=c("ISO3_Code", "Period", "Peaks"),measure.vars = colnames(h))
      mydata_country_=data.frame(mydata_country_h)
      return(mydata_country_h)
      }
}
  mydata_h=lapply(list_country,fun)  
  mydata_h=do.call(rbind,mydata_h)
  return(mydata_h)
}


get_h2=function(mydata) {
  # if (is.null(var)==F) {
  #   mydata=data.frame(mydata) %>% dplyr::select(ISO3_Code,Period,var)
  #  # mydata=Expansions_Contractions_2(mydata,var)
  # }
  list_country=unique(mydata$ISO3_Code) #find list of countries available in the sample
  fun=function(ctry){
    mydata_country=mydata %>% filter(ISO3_Code==ctry) %>% dplyr::select(ISO3_Code,Period,Peaks)
    n_peaks=sum(mydata_country$Peaks,na.rm = T)
    if (n_peaks>0) {
      N=NROW(na.omit(mydata_country$Peaks))
      h<-matrix(nrow = N, ncol = n_peaks)
      h<-data.frame(h)
      colnames(h) <- paste('h',1:n_peaks,sep="")
      t_peak=which(mydata_country$Peaks==1)
      for(j in 1:n_peaks){
        h[,j]<-1:NROW(na.omit(mydata_country$Peaks))-t_peak[j]
      }
      mydata_country<-transform(merge(mydata_country, h, by=0), row.names=Row.names, Row.names=NULL)
      mydata_country<-mydata_country[order(mydata_country$Period),]
      mydata_country_h<-melt(mydata_country,id=c("ISO3_Code", "Period", "Peaks"),measure.vars = colnames(h))
      mydata_country_h=data.frame(mydata_country_h)
      #return(mydata_country_h)
    }
    return(mydata_country_h)
  }
  mydata_h=lapply(list_country,fun)  
  mydata_h=do.call(rbind,mydata_h)
  return(mydata_h)
}