mydata_init=function(countries="FRA",start=1980,end=2018,frequency="quarter"){
  mydata=data.frame()
  for(i in 1:length(countries)){
    country=countries[i]
    dt.frame=data.frame(seq(from=as.Date(paste(start,"/01/01",sep="")),to=as.Date(paste(end,"/12/01",sep="")),by=frequency),country)
    colnames(dt.frame)=c("Period","ISO3_Code")
    mydata=rbind(mydata,dt.frame)
  }
  return(mydata)
}