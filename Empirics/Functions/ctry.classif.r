ctry_classif=function(){
  classif_name=c('ARG','AUS','AUT','BEL','BGR','BRA','CHE','CHL','CHN','COL','DEU','DNK','ESP','EST','FRA','GBR','GRC','HKG','HRV','HUN','IDN','IRL','ISL','ISR','ITA','JPN','KOR','MEX','MYS','NLD','NZL','PER','PHL','POL','PRT','ROU','RUS','SVK','THA','TUN','TUR','USA','ZAF')
  
  classif=c('Emerging','Developped','Developped','Developped','Emerging','Emerging','Developped','Emerging','Emerging','Emerging','Developped','Developped','Developped','Emerging','Developped','Developped','Developped','Developped','Developped','Developped','Emerging','Developped','Developped','Developped','Developped','Developped','Developped','Emerging','Emerging','Developped','Developped','Emerging','Emerging','Emerging','Developped','Emerging','Emerging','Developped','Emerging','Emerging','Emerging','Developped','Emerging')
  
  classif=cbind(classif_name,classif)
  classif=data.frame(classif)
  colnames(classif)=c("ISO3_Code","Classification")
  return(classif)
}
